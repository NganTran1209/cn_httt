package com.crazyteam.edayfashion.models

import com.google.gson.annotations.SerializedName

data class DataUserResponse(
    val birthDay: String?,
    val id: Int,
    val username : String,
    val email : String,
    val password : String,
    val sex: Boolean = false,
    @SerializedName("facebook_url")
    val imageUrl: String?,
    val role : String,
    val addr : String?,
    val first_name : String?,
    val last_name : String?,
    val phone : String?
)