<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="context" value="${pageContext.request.contextPath}" />

<header id="header" class="header-light-bg header_mobile">
    <nav id="sticky_head" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="row">
                <div class="navbar-header">
                    <div class="col-midle-recod">
                           <a class="navbar-brand" href="/" id="logo-img">                
                               <div class="bt-logo-light" id="bt-log"></div>
                           </a>
                           <a class="navbar-note" href="#">
                               Test Automation
                            	<br>Without Coding 
                           </a>
                    </div>
                    <button type="button" class="menu-bar-header navbar-toggle">
                            <i class="fa fa-bars" aria-hidden="true" onclick="clickMenuIconHeader()" id="iconMenuSideMobile"></i>
                        </button>
                </div>
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav navbar-right navigation">   
                        <li class="nav-item">
                            <a href="#features">Features</a>
                        </li>
                        <li class="nav-item">
                            <a href="#platform">Platforms</a>
                        </li>                   
                        <li class="nav-item">
                            <a href="#pricing">Pricing</a>
                        </li>
                        <li class="nav-item">
                            <a href="#contactus">Contact</a>
                        </li>
                        <li>
                            <a href="${context}/login">Log In</a>
                        </li>
                        <li class="nav-item">
                        	<a href="${context}/signup" class="btn btn-primary" style="margin:auto 0; font-family: 'Oswald', sans-serif;padding: 6px 12px;">Sign up</a>
                        </li>
                        <li class="nav-item">
                                <a href="/ja"><img width="35px" height="35px" src="${context }/assets/img/jp.png" /></a>
                        </li>

                    </ul>
                </div>
            </div>    
            <div id="mySidenav" class="sidenav">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">Ã</a>
                <ul>
	                <li class="dropdown">
	                    <span class="dropbtn" id="mobile_automated" onclick="show_dropdown('mobile_automated',240,245)">Automate</span>
	                    <i class="fa fa-angle-down"></i>
	                    <ul class="dropdown-content">
	                        <li>
	                            <a href="#">Web Application Testing</a>
	                        </li>
	                        <li>
	                            <a href="#">Mobile App Testing</a>
	                        </li>
	                        <li>
	                            <a href="#">Regression Testing</a>
	                        </li>
	                        <li>
	                            <a href="#">Cross Browser Testing</a>
	                        </li>
	                        <li>
	                            <a href="#">Data Driven Testing</a>
	                        </li>
	                    </ul>
	                </li>
	                <li class="dropdown">
	                    <span
	                        class="dropbtn"
	                        id="mobile_features"
	                        onclick="show_dropdown('mobile_features',320,335)">Features</span>
	                    <i class="fa fa-angle-down"></i>
	                    <ul class="dropdown-content">
	                        <li>
	                            <a href="#">AI-Driven Test Automation</a>
	                        </li>
	                        <li>
	                            <a href="#">Test Development</a>
	                        </li>
	                        <li>
	                            <a href="#">Test Plan</a>
	                        </li>
	                        <li>
	                            <a href="#">Test Lab</a>
	                        </li>
	                        <li>
	                            <a href="#">Reports</a>
	                        </li>
	                        <li>
	                            <a href="#">Analytics</a>
	                        </li>
	                        <li>
	                            <a href="#">Configuration</a>
	                        </li>
	                        <li class="divider"></li>
	                        <li>
	                            <a href="#" class="btn-all-features">All Features â</a>
	                        </li>
	                    </ul>
	                </li>
	                <li>
	                    <a href="#">Pricing</a>
	                </li>
	                <li>
	                    <a href="#">Blog</a>
	                </li>
	                <li>
	                    <a href="#">Resources</a>
	                </li>
	                <li>
	                    <a href="#" target="_blank">Log In</a>
	                </li>
	                <li>
	                    <button
	                        class="btn btn-primary"
	                        style="margin: 25px 0 0 32px;"
	                        onclick="window.location.href='signup'">Sign up for free</button>
	                </li>
                </ul>
            </div>
            <!-- <span onclick="openNav()" class="fa fa-bars fa-2x" id="bars"></span> -->
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
        
    </nav>
    <div class="side_wrap">
        <div class="panel_top">
            <nav class="menu_main_nav_area">
                <ul id="menu_main_mobile" class="menu_main_nav">
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-143"><a href="#features">Features</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-143"><a href="#platform">Platforms</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-518"><a href="#pricing">Pricing</a></li>  
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-518"><a href="#contactus">Contact</a></li> 
                </ul>
            </nav>
            <div class="signup-menu-res">
                <a href="${context}/signup" class="btn btn-primary btn-oval signup_oval_btn">Sign up</a>
                <a href="/login" class="btn btn-primary btn-oval signup_oval_btn">Login</a>
            </div>
        </div>
    </div>
</header>

