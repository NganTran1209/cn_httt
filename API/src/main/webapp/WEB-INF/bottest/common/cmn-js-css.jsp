<script>
var CONTEXT = "${context }";
var currentLocale='${sessionScope.lang}';
if (currentLocale == '') {
	currentLocale = navigator.language || navigator.userLanguage;
	if (currentLocale == 'jp' || currentLocale == 'ja' || currentLocale.indexOf('jp-') != -1 || currentLocale.indexOf('ja-') != -1) {
		currentLocale = 'ja';
	} else {
		currentLocale = 'en';
	}
}
</script>

<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,900&display=swap&subset=latin-ext" rel="stylesheet">
<link href="${context}/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="${context}/assets/plugins/themify-icons/themify-icons.css" rel="stylesheet" type="text/css" />
<link href="${context}/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="${context}/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${context}/assets/plugins/summernote/summernote.css" rel="stylesheet">
<link href="${context}/assets/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="${context}/assets/plugins/material/material.min.css">
<link rel="stylesheet" href="${context}/assets/css/material_style.css">
<link href="${context}/assets/css/pages/animate_page.css" rel="stylesheet">
<link href="${context}/assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
<link href="${context}/assets/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${context}/assets/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="${context}/assets/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css" />
<link rel="stylesheet" href="${context}/assets/plugins/sweet-alert/sweetalert.min.css">
<link href="${context}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
<link href="${context}/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="${context}/assets/css/jquery-cron.css" rel="stylesheet" type="text/css" /> 
<link href="${context}/assets/css/jquery-gentleSelect.css" rel="stylesheet" type="text/css" /> 
<link href="${context}/assets/css/style.css" rel="stylesheet" type="text/css" /> 
<link href="${context}/assets/css/pages/formlayout.css" rel="stylesheet" type="text/css" />
<link href="${context}/assets/css/plugins.min.css" rel="stylesheet" type="text/css" />
<link href="${context}/assets/css/responsive.css" rel="stylesheet" type="text/css" />
<link href="${context}/assets/css/theme-color.css" rel="stylesheet" type="text/css" /> 
<link href="${context}/assets/css/custom.css" rel="stylesheet" type="text/css" />
<link href="${context}/assets/plugins/jquery-ui/jquery-ui.css" rel="stylesheet" />
<%-- <link href="${context}/assets/css/editor.css" type="text/css" />  --%>
<!-- start js include path -->
<script src="${context}/assets/plugins/jquery/jquery.min.js" ></script>
<script src="${context}/assets/plugins/popper/popper.min.js" ></script>
<script src="${context}/assets/plugins/jquery-ui/jquery-ui.min.js" ></script>

<script src="${context}/assets/plugins/sparkline/jquery.sparkline.min.js" ></script>
<script src="${context}/assets/js/pages/sparkline/sparkline-data.js" ></script>
<script src="${context}/assets/js/app.js" ></script>
<script src="${context}/assets/js/templateLayout.js" ></script>
<script src="${context}/assets/js/theme-color.js" ></script>
<script src="${context}/assets/js/pages/ui/animations.js" ></script>
<script src="${context}/assets/plugins/morris/raphael-min.js" ></script>
<script src="${context}/assets/plugins/counterup/jquery.waypoints.min.js" ></script>
<script src="${context}/assets/js/management/jquery-gentleSelect-min.js" ></script>
<script src="${context}/assets/js/management/jquery-cron-min.js" ></script>
<script src="${context}/assets/plugins/counterup/jquery.counterup.js" ></script>
<script src="${context}/assets/plugins/select2/js/select2.full.min.js"></script>
<script src="${context}/assets/plugins/jquery-validation/js/jquery.validate.min.js" ></script>
<script src="${context}/assets/plugins/morris/morris.min.js" ></script>
<script src="${context}/assets/plugins/morris/raphael-min.js" ></script>
<script src="${context}/assets/js/ckeditor.js" ></script>
<script src="${context}/assets/plugins/jquery-ui/jquery-ui.js"></script>
<script src="${context}/assets/plugins/bootstrap/js/bootstrap.min.js" ></script>
<!--Chart JS-->
<script src="${context}/assets/plugins/chart-js/Chart.bundle.js" ></script>
<script src="${context}/assets/plugins/chart-js/utils.js" ></script>
<!-- Sweet Alert -->
<script src="${context}/assets/plugins/sweet-alert/sweetalert.min.js" ></script>
<script src="${context}/assets/js/pages/sweet-alert/sweet-alert-data.js" ></script>
<script src="${context}/assets/plugins/sockjs-0.3.4.min.js" ></script>
<script src="${context}/assets/plugins/stomp.min.js" ></script>
<script src="${context}/assets/plugins/datatables/jquery.dataTables.min.js" ></script>
<script src="${context}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
<script  src="${context}/assets/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
<script src="${context}/assets/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
<script src="${context}/assets/js/management/translate.js" charset="UTF-8" ></script>
<script src="${context}/assets/js/management/user-main.js" ></script>
<!-- Bootstable -->
<script src="${context}/assets/plugins/bootstable/bootstable.js" ></script>



          