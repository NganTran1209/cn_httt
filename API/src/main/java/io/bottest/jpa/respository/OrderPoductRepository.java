package io.bottest.jpa.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import io.bottest.jpa.entity.Order;

@Repository
@Transactional
public interface OrderPoductRepository extends JpaRepository<Order, Long>{

	@Query(value= "SELECT * FROM  orders where id =?1", nativeQuery = true)
	Order findByOrderId(int orderId);

	@Query(value= "SELECT * FROM  orders where user_id =?1 and state=0", nativeQuery = true)
	List<Order> findCartByUserId(Long id);

	@Query(value= "SELECT * FROM  orders where pro_id =?1 ", nativeQuery = true)
	List<Order> findByProductId(Long proId);

}
