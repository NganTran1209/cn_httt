package io.bottest.jpa.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ColumnDefault;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "product")
@NamedQuery(name = "Product.findAll", query = "SELECT p FROM Product p")
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String name;
	
	private Long amount;
	
	private String size;
	
	private Long price_buy;
	
	private Long price_sell;
	
	private int catId;
	
	@Column(name = "input_day")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyyMMdd")
	private Date inputDay;
	
	@Lob
	@Column( length = 100000 )
	private String image;
	
	private String media;
	
	@ColumnDefault("0")
	private Long amount_sell;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public Long getPrice_buy() {
		return price_buy;
	}

	public void setPrice_buy(Long price_buy) {
		this.price_buy = price_buy;
	}

	public Long getPrice_sell() {
		return price_sell;
	}

	public void setPrice_sell(Long price_sell) {
		this.price_sell = price_sell;
	}

	public int getCatId() {
		return catId;
	}

	public void setCatId(int catId) {
		this.catId = catId;
	}

	public Date getInputDay() {
		return inputDay;
	}

	public void setInputDay(Date inputDay) {
		this.inputDay = inputDay;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getMedia() {
		return media;
	}

	public void setMedia(String media) {
		this.media = media;
	}

	public Long getAmount_sell() {
		return amount_sell;
	}

	public void setAmount_sell(Long amount_sell) {
		this.amount_sell = amount_sell;
	}
	
	
}
