package io.bottest.jpa.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Null;

import org.springframework.format.annotation.DateTimeFormat;

@Embeddable
public class PayRollIdentity implements Serializable{

	private static final long serialVersionUID = 6891779134365313756L;

	/**
	 *luong theo thang
	 */
	@Lob
	@Null
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "month")
	private String month;
	
	/**
	 * Id cong nhan
	 */
	@Lob
	@Null
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long workerId;
	
	


	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public Long getWorkerId() {
		return workerId;
	}

	public void setWorkerId(Long workerId) {
		this.workerId = workerId;
	}

	
	
	
}
