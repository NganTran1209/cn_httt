package io.bottest.jpa.entity;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity(name = "product_warehouse")
@Table(name = "product_warehouse")
@NamedQuery(name = "product_warehouse.findAll", query = "SELECT p_w FROM product_warehouse p_w")
public class ProductWareHouse {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "pro_Id")
	private Long proId;
	
	private Long amount;
	
	@Column(name = "update_day")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyyMMdd")
	private Date updateDay;
	
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyyMMdd")
	private Date created_date;
	
	@Column(name = "stock_quantity")
	private Long stockQuantity;
	
	@Column(name = "salerable_quantity")
	private Long salerableQuantity;
	
	@Column(name = "total_money_buy")
	private Long totalMoneyBuy;
	
	@Column(name = "total_money_sell")
	private Long totalMoneySell;
	
	private Long owner_id;

	private Long profit;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProId() {
		return proId;
	}

	public void setProId(Long long1) {
		this.proId = long1;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Date getUpdateDay() {
		return updateDay;
	}

	public void setUpdateDay(Date updateDay) {
		this.updateDay = updateDay;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	public Long getStockQuantity() {
		return stockQuantity;
	}

	public void setStockQuantity(Long stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public Long getSalerableQuantity() {
		return salerableQuantity;
	}

	public void setSalerableQuantity(Long salerableQuantity) {
		this.salerableQuantity = salerableQuantity;
	}

	public Long getTotalMoneyBuy() {
		return totalMoneyBuy;
	}

	public void setTotalMoneyBuy(Long totalMoneyBuy) {
		this.totalMoneyBuy = totalMoneyBuy;
	}

	public Long getTotalMoneySell() {
		return totalMoneySell;
	}

	public void setTotalMoneySell(Long totalMoneySell) {
		this.totalMoneySell = totalMoneySell;
	}

	public Long getOwner_id() {
		return owner_id;
	}

	public void setOwner_id(Long long1) {
		this.owner_id = long1;
	}

	public Long getProfit() {
		return profit;
	}

	public void setProfit(Long profit) {
		this.profit = profit;
	}
	
}
