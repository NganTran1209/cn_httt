package io.bottest.controller;

import java.lang.reflect.Type;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.bottest.dto.APIResult;
import io.bottest.dto.FilterUserDTO;
import io.bottest.dto.FilterWareHouseDTO;
import io.bottest.dto.OrderDTO;
import io.bottest.dto.ProductWareHouseDTO;
import io.bottest.jpa.entity.Order;
import io.bottest.jpa.entity.Payment;
import io.bottest.jpa.entity.Product;
import io.bottest.jpa.entity.ProductWareHouse;
import io.bottest.jpa.entity.User;
import io.bottest.jpa.respository.ProductRepository;
import io.bottest.jpa.respository.ProductWareHouseResponsitory;
import io.bottest.jpa.respository.UserRepository;
import io.bottest.jpa.respository.WareHouseRepositoryCustom;

@Controller
@RestController
@RequestMapping("/warehouse")
public class ProductWareHouseController {

	private DateFormat dateFormat;

	private DateFormat dateFormat2;

	private static final String LOG_STRING = null;

	private final Logger LOGGER = LogManager.getLogger(ProductWareHouseController.class);
	
	@Autowired
	ProductWareHouseResponsitory proWareRepo;
	
	@Autowired
	ProductRepository productRepo;
	
	@Autowired
	UserRepository userRepo;
	
	@Autowired
	WareHouseRepositoryCustom wareCustome;
	
	@RequestMapping(value = { "/getAll" }, method = RequestMethod.GET)
	public APIResult listWareHouse( ) {

		final String LOG_STRING = "GET /warehouse/getAll";
		LOGGER.info("Start " + LOG_STRING);
		
		APIResult result = new APIResult();
		List<ProductWareHouse> listPrWare = proWareRepo.findAll();
		List<ProductWareHouseDTO> listPrWareDTO = new ArrayList<ProductWareHouseDTO>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		long totalProfit = 0;
		if(listPrWare != null) {
			for (ProductWareHouse prWare : listPrWare) {
				ProductWareHouseDTO prWareDTO = new ProductWareHouseDTO();
				prWareDTO.setId(prWare.getId());
				Product product = productRepo.findByProductId(prWare.getProId());
				prWareDTO.setProduct(product);
				User imple_staff = userRepo.findById(prWare.getOwner_id());
				prWareDTO.setImple_staff(imple_staff);
				prWareDTO.setAmount(prWare.getAmount());
				prWareDTO.setCreated_date(df.format(prWare.getCreated_date()));
				prWareDTO.setSalerableQuantity(prWare.getSalerableQuantity());
				prWareDTO.setStockQuantity(prWare.getStockQuantity());
				prWareDTO.setTotalMoneyBuy(prWare.getTotalMoneyBuy());
				prWareDTO.setTotalMoneySell(prWare.getTotalMoneySell());
				prWareDTO.setProfit(prWare.getProfit());
				listPrWareDTO.add(prWareDTO);
			}
			result.setData(listPrWareDTO);
		}else {
			result.setData(listPrWare);
		}
		
		LOGGER.info("End GET /warehouse/getAll");
		return result;

	}
	
	@RequestMapping(value = { "/total" }, method = RequestMethod.GET)
	public APIResult totalWareHouse( ) {

		final String LOG_STRING = "GET /warehouse/total";
		LOGGER.info("Start " + LOG_STRING);
		
		APIResult result = new APIResult();
		List<ProductWareHouse> listPrWare = proWareRepo.findAll();
		long sum = 0;
		if(listPrWare != null) {
			for (ProductWareHouse productWareHouse : listPrWare) {
				if(productWareHouse.getProfit() != null) {
					sum += productWareHouse.getProfit();
				}
			}
			result.setData(sum);
		}else {
			result.setData(0);
		}
		
		LOGGER.info("End GET /warehouse/total");
		return result;

	}
	
	@RequestMapping(value = { "/filter" }, method = RequestMethod.POST)
	public APIResult filterWarehouse(Principal principal, @RequestBody FilterWareHouseDTO filterWareHouse,HttpSession session,
			@AuthenticationPrincipal UserDetails userDetails) {

		APIResult result = new APIResult();
		Gson gson = new Gson();
		Type listType = new TypeToken<FilterWareHouseDTO>() {
		}.getType();
		String filStr = gson.toJson(filterWareHouse, listType);
		session.setAttribute("filterWareHouse", filStr);
		List<Map<String, Object>> wareHouseList = wareCustome.filterWarehouse(filterWareHouse);
		if(wareHouseList != null && wareHouseList.size() > 0) {
			result.setData(wareHouseList);
		}else {
			result.setData("No data available");
		}
		
		return result;
	}
}
