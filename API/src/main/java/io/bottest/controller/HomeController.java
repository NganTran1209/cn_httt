package io.bottest.controller;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import io.bottest.dto.APIResult;
import io.bottest.dto.ChangePassDTO;
import io.bottest.dto.FilterProductDTO;
import io.bottest.dto.ProductDTO;
import io.bottest.dto.SignupDTO;
import io.bottest.dto.UpdateUserDTO;
import io.bottest.jpa.entity.Category;
import io.bottest.jpa.entity.Product;
import io.bottest.jpa.entity.User;
import io.bottest.jpa.respository.ProductRepository;
import io.bottest.jpa.respository.UserRepository;
import io.bottest.service.CategoryServise;
import io.bottest.service.ProductService;
import io.bottest.service.UserService;
import io.bottest.utils.FunctionUtils;
import io.bottest.utils.TokenUtil;

@CrossOrigin
@Controller
@RestController
@RequestMapping("/")
public class HomeController {
 
	private final Logger LOGGER = LogManager.getLogger(HomeController.class);
	
	public static final String STATUS_REJECT = "REJECT";
	public static final String STATUS_ACCEPT = "ACCEPT";
	public static final String STATUS_WATING = "WAITING";

	@Value("${security.jwt.token.secret-key:Bottest@Paracel}")
	private String secretKey = "Bottest@Paracel";
	
	//@Value("${spring.project.username}")
	private String projectUserName;
	
	//@Value("${spring.project.password}")
	private String projectPassword;
	
	//@Value("${spring.project.url}")
	private String projectUrl;
	
	//@Value("${spring.project.type}")
	private String projectType;
	
	//@Value("${sample.path}")
	private String samplePath;
	
//	@Value("${resource.path}")
//	private String resourcePath;
	
	@Value("${bottest.url}")
	private String bottestUrl;
	

	@Autowired
	private UserService userService;
	
	//EDAY
	@Autowired
	UserRepository userRepos;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	ServletContext context;
	
	@Autowired
	CategoryServise categoryService;
	
	@Autowired
	ProductRepository productRepo;
	
	/**
	 * @return
	 */
	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public ModelAndView getProject() {
		LOGGER.info("Start GET / ");
		
		ModelAndView mav = new ModelAndView("login");
		
		LOGGER.info("End GET / ");
		return mav;
	}
	@RequestMapping(value = { "/en" }, method = RequestMethod.GET)
	public ModelAndView homeEn() {
		LOGGER.info("Start GET /en ");
		
		ModelAndView mav = new ModelAndView("login");
		
		LOGGER.info("End GET /en ");
		return mav;
	}

	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	public ModelAndView login() {

		LOGGER.info("Start GET /login ");
		ModelAndView mav = new ModelAndView("login");
		
		LOGGER.info("End GET /login ");
		return mav;
	}

	/**
	 * @param token
	 * @param model
	 * @return
	 */
	@RequestMapping(value = { "/signup" }, method = RequestMethod.GET)
	public ModelAndView singup() {
		
		LOGGER.info("Start GET /signup ");
		
		SignupDTO dto = new SignupDTO();
		ModelAndView mav = new ModelAndView("signup");
		mav.addObject("dto", dto);
		
		LOGGER.info("End GET /signup ");
		return mav;
	}

	

	@MessageMapping("/listen")
	//@SendToUser("/bot_notify/job")
	public APIResult listen2(@RequestBody String message, Principal principal) throws Exception {
//		System.out.println("/listen:Client message:" + message);
		// Thread.sleep(1000); // simulated delay
		LOGGER.info("Start Message /listen ");
		
		LOGGER.info("GET /auth/me PARAMS message:" + message);
		APIResult result = new APIResult();
		result.setData("");
		
		LOGGER.info("End Message /listen ");
		return result;
	}

	

	
	
	
	

	
	//EDAY
	@RequestMapping(value = { "/editProfile" }, method = RequestMethod.POST)
	public APIResult editProfile(@RequestBody UpdateUserDTO user, @AuthenticationPrincipal UserDetails userDetails, HttpServletRequest request) {
		
		LOGGER.info("Start POST /editProfile ");
		LOGGER.info("POST /editProfile PARAMS user:" + new Gson().toJson(user, new TypeToken<UpdateUserDTO>() {}.getType()));
		LOGGER.info("POST /editProfile PARAMS userDetails:" + new Gson().toJson(userDetails, new TypeToken<UserDetails>() {}.getType()));
		APIResult result = new APIResult();
		User theUser = userRepos.findByUsernameIgnoreCase(user.getUsername());
		theUser.setAddr(user.getAddr());
		theUser.setBirthDay(user.getBirthDay());
		theUser.setPhone(theUser.getPhone());
		theUser.setSex(user.isSex());
		userRepos.save(theUser);
		result.setData(theUser);
		
		LOGGER.info("End POST /editProfile ");
		return result;
	}
	
	
	
	
	
	//API EDAY
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public APIResult signupSubmit(@RequestBody SignupDTO dto) {
		final String LOG_STRING = "POST /api/v1/user/signup";
		LOGGER.info("START" + LOG_STRING);
		LOGGER.info(LOG_STRING + " PARAM dto: " + dto);

		APIResult result = new APIResult();
		if (dto != null) {
			if (dto.getEmail() != null) {
				boolean checkUser = userService.checkEmailExist(String.valueOf(dto.getEmail()));
				if (checkUser == false) {
					result.setErrormessage("Email is exist! Please enter the new username");
					LOGGER.info("POST /signup MESSAGE: Username is exist! Please enter the new username");
				} else {
					User user = new User();
					PasswordEncoder encoder = new BCryptPasswordEncoder();
					user.setEmail(dto.getEmail());
					user.setUsername(dto.getUsername());
					user.setPassword(encoder.encode(dto.getPassword()));
					user.setRole("ROLE_CUSTOMER");
					user.setEnable(true);
					//user.setStartDay(Date );
					userRepos.save(user);
					result.setData(user);
				}
			}
		}

		LOGGER.info("END" + LOG_STRING);
		return result;
	}
	
	@RequestMapping(value = { "/product/list" }, method = RequestMethod.GET)
	public APIResult listProduct() {
		LOGGER.info("GET /product/list");
		APIResult result = new APIResult();
		List<Product> listProduct = productService.getAllProduct();
		result.setData(listProduct);
		return result;
	}
	
	
	
	@RequestMapping(value = { "/product/{productId}/detail" }, method = RequestMethod.GET)
	public APIResult detailProduct(@PathVariable(name = "productId") String productId) {
		APIResult result = new APIResult();
		Product product = productService.getProductById(Long.valueOf(productId));
		result.setData(product);
		return result;
	}
	
	
	@RequestMapping(value = { "/product/filter" }, method = RequestMethod.POST)
	public APIResult filterProduct(Principal principal, @RequestBody FilterProductDTO filterProduct,HttpSession session) {

		APIResult result = new APIResult();
		Gson gson = new Gson();
		Type listType = new TypeToken<FilterProductDTO>() {
		}.getType();
		String filStr = gson.toJson(filterProduct, listType);
		session.setAttribute("filterProduct", filStr);
		List<Map<String, Object>> productList = productService.filterProduct(filterProduct);
		if(productList != null && productList.size() > 0) {
			result.setData(productList);
		}else {
			result.setData("No data available");
		}
		
		return result;
	}
	
	@RequestMapping(value = { "/listStaff" }, method = RequestMethod.GET)
	public APIResult listStaff( ) {

		final String LOG_STRING = "GET /listStaff";
		LOGGER.info("Start " + LOG_STRING);
		
		APIResult result = new APIResult();
		List<User> listStaff = userRepos.getListStaff();
		result.setData(listStaff);

		LOGGER.info("End GET /listStaff");
		return result;

	}
	
	@RequestMapping(value = { "/cat/list" }, method = RequestMethod.GET)
	public APIResult listCat(@AuthenticationPrincipal UserDetails userDetails) {

		final String LOG_STRING = "GET /admin/cat/list";
		LOGGER.info("Start " + LOG_STRING);
		
		APIResult result = new APIResult();
		List<Category> cats = categoryService.findAll();
		result.setData(cats);

		LOGGER.info("GET /admin/cat/list");
		return result;

	}
	
	@RequestMapping(value = { "/productFilterCat" }, method = RequestMethod.GET)
	public APIResult productFilterCat(@RequestBody String catId) {

		final String LOG_STRING = "GET /productFilterCat";
		LOGGER.info("Start " + LOG_STRING);
		
		APIResult result = new APIResult();
		Long id = Long.valueOf(catId);
		if(id == null || id <= 0 ) {
			result.setErrormessage("Check category again!!");
		}else {
			List<Product> listProduct = productRepo.getListProductByCatId(id);
		}
		List<Category> cats = categoryService.findAll();
		result.setData(cats);

		LOGGER.info("GET /productFilterCat");
		return result;

	}
	
}
