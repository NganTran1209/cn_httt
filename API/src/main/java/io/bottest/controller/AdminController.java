package io.bottest.controller;

import java.lang.reflect.Type;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.bottest.dto.APIResult;
import io.bottest.dto.CatDTO;
import io.bottest.dto.FilterProductDTO;
import io.bottest.dto.FilterUserDTO;
import io.bottest.dto.OrderDTO;
import io.bottest.dto.PayRollDTO;
import io.bottest.dto.ProductDTO;
import io.bottest.dto.StaffDTO;
import io.bottest.dto.UpdateUserDTO;
import io.bottest.dto.UserDTO;
import io.bottest.dto.WorkerDTO;
import io.bottest.jpa.entity.Category;
import io.bottest.jpa.entity.Order;
import io.bottest.jpa.entity.PayRoll;
import io.bottest.jpa.entity.PayRollIdentity;
import io.bottest.jpa.entity.Payment;
import io.bottest.jpa.entity.Product;
import io.bottest.jpa.entity.ProductWareHouse;
import io.bottest.jpa.entity.User;
import io.bottest.jpa.entity.Worker;
import io.bottest.jpa.respository.CategoryRepository;
import io.bottest.jpa.respository.OrderPoductRepository;
import io.bottest.jpa.respository.PayRollRepository;
import io.bottest.jpa.respository.PaymentRepository;
import io.bottest.jpa.respository.ProductRepository;
import io.bottest.jpa.respository.ProductWareHouseResponsitory;
import io.bottest.jpa.respository.UserRepository;
import io.bottest.jpa.respository.UserRepositoryCustom;
import io.bottest.jpa.respository.WorkerRepository;
import io.bottest.service.CategoryServise;
import io.bottest.service.PayRollService;
import io.bottest.service.ProductService;
import io.bottest.service.ProductWareHouseService;
import io.bottest.service.WorkerService;
import io.bottest.utils.DateUtil;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@Controller
@RestController
@RequestMapping("/admin")
public class AdminController { 

	private static final String LOG_STRING = null;

	private final Logger LOGGER = LogManager.getLogger(AdminController.class);

	@Autowired
	ProductService productService;
	
	@Autowired
	CategoryServise categoryService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	ProductRepository productRepo;
	
	@Autowired
	CategoryRepository catRepo;
	
	@Autowired
	OrderPoductRepository orderRepo;
	
	@Autowired
	WorkerRepository workerRepo;
	
	@Autowired
	private WorkerService workerService;
	
	@Autowired
	private PayRollRepository payRollRepository;
	
	@Autowired
	private ProductWareHouseService productWareService;
	
	@Autowired
	ProductWareHouseResponsitory productWareRepo;
	
	@Autowired
	PaymentRepository paymentRepo;
	
	@Autowired
	UserRepositoryCustom userCustome;
	
	@Autowired
	PayRollService payRollService;
	/**
	 * @return
	 */
	@RequestMapping(value = { "" }, method = RequestMethod.GET)
	public ModelAndView getManagement() {

		LOGGER.info("Start GET /admin ");

		ModelAndView mav = new ModelAndView("homeAdmin");

		LOGGER.info("End GET /admin");
		return mav;
	}

	

	//API EDAY
	/**
	 * @return
	 */
	@RequestMapping(value = { "/user/getAll" }, method = RequestMethod.GET)
	public APIResult getAllUser() {

		LOGGER.info("Start GET /admin/user/getAll");

		APIResult result = new APIResult();
		List<User> users = userRepository.findAll();
		result.setData(users);

		LOGGER.info("End GET /admin/user/getAll");
		return result;

	}
	
	@RequestMapping(value = { "/user/getAllCustomer" }, method = RequestMethod.GET)
	public APIResult getAllCustomer(@AuthenticationPrincipal UserDetails userDetails) {

		LOGGER.info("Start GET /admin/user/getAll");

		APIResult result = new APIResult();
		if(userDetails != null ) {
			User auth = userRepository.findByUserNameAndPassword(userDetails.getUsername(),userDetails.getPassword() );
			if(auth.getRole().equals("ROLE_ADMIN")) {
				List<User> users = userRepository.findAllCustomer();
				result.setData(users);
		
			}else {
				result.setErrormessage("User don't permission!!");
			}
			
		}else {
	    	result.setErrormessage("403 Authencation!!");
	    }
		LOGGER.info("End GET /admin/user/getAll");
		return result;

	}
	
	@RequestMapping(value = { "/user/update" }, method = RequestMethod.POST)
	public APIResult updateRoleUser(@RequestBody UpdateUserDTO user) {

		LOGGER.info("Start POST /admin/user/update");

		APIResult result = new APIResult();
		User theUser = userRepository.findById(user.getId());
		theUser.setRole(user.getRole());
		userRepository.save(theUser);
		Worker worker = new Worker();
		Worker check = workerRepo.findByUserId(theUser.getId());
		if(check== null) {
			LOGGER.info("End GET -------" + theUser.getRole());
			if(theUser.getRole().equals("ROLE_STAFF") || theUser.getRole().equals("ROLE_MANAGER")) {
				worker.setUserId(theUser.getId());
				worker.setCreateDay(new Date());
				workerRepo.save(worker);
				workerRepo.save(worker);
				worker = workerRepo.findByUserId(theUser.getId());
				
			}
		}
		
		result.setData(theUser);

		LOGGER.info("End POST /admin/user/update");
		return result;

	}

	
	@RequestMapping(value = { "/user/delete" }, method = RequestMethod.POST)
	public APIResult deleteUser(@RequestBody UpdateUserDTO user) {

		LOGGER.info("Start GET /admin/user/delete");

		APIResult result = new APIResult();
		User theUser = userRepository.findById(user.getId());
		theUser.setEnable(false);
		userRepository.save(theUser);
		Worker worker = workerRepo.findByUserId(user.getId());
		if(worker != null ) {
			List<PayRoll> listPayroll = payRollRepository.findAllPayRollByWorkerId(worker.getId());
			payRollRepository.deleteAll(listPayroll);
		}
		result.setData("");

		LOGGER.info("End GET /admin/user/delete");
		return result;

	}
	
	@RequestMapping(value = { "/detailUser/{userId}" }, method = RequestMethod.GET)
	public APIResult detailUser(@PathVariable("userId") Long userId ) {

		final String LOG_STRING = "GET /admin/detailUser";
		LOGGER.info("Start " + LOG_STRING);
		
		APIResult result = new APIResult();
		User theUser = userRepository.getUser(userId);
		result.setData(theUser);
		LOGGER.info("End GET /admin/detailUser");
		return result;

	}

	/**
	 * @return
	 */
	@RequestMapping(value = { "/user" }, method = RequestMethod.GET)
	public ModelAndView getListUserProject() {

		LOGGER.info("Start GET /admin/user");

		ModelAndView mav = new ModelAndView("userManagement");

		LOGGER.info("End GET /admin/user");
		return mav;
	}

	//API EDAY PRODUCT
	/**
	 * @param product
	 * @return
	 */
	@RequestMapping(value = { "/product/create" }, method = RequestMethod.POST)
	public APIResult createProduct(@RequestBody Product product, @AuthenticationPrincipal UserDetails userDetails) {

		final String LOG_STRING = "POST /admin/product/create";
		LOGGER.info("Start " + LOG_STRING);
		
		APIResult result = new APIResult();
		
		product.setAmount_sell((long) 0);
		Product productNew = productRepo.save(product);
		if(productNew != null) {
			productWareService.addProductToWareHouse(productNew, userDetails);
			result.setData(productNew);
		}

		LOGGER.info("End POST /admin/product/create");
		return result;

	}
	
	
	@RequestMapping(value = { "/product/update" }, method = RequestMethod.POST)
	public APIResult updateProduct(@RequestBody ProductDTO product, HttpServletRequest request) {
		
		LOGGER.info("Start POST /cat/update ");
		APIResult result = new APIResult();
		Product theProduct = productRepo.findByProductId(product.getId());
		theProduct.setName(product.getName());
		theProduct.setAmount(product.getAmount());
		theProduct.setCatId(product.getCatId());
		theProduct.setInputDay(product.getInputDay());
		theProduct.setPrice_buy(product.getPrice_buy());
		theProduct.setPrice_sell(product.getPrice_sell());
		theProduct.setSize(product.getSize());
		theProduct.setImage(product.getImage());
		Product productUpdate = productRepo.save(theProduct);
		
		ProductWareHouse productWareHouse = productWareRepo.findByProductId(productUpdate.getId());
		productWareHouse.setAmount(productUpdate.getAmount());
		productWareHouse.setTotalMoneyBuy(productUpdate.getAmount() * productUpdate.getPrice_buy());
		productWareRepo.save(productWareHouse);
		
		result.setData(theProduct);
		
		LOGGER.info("End POST /product/update ");
		return result;
	}
	@RequestMapping(value = { "/{productId}/deleted" }, method = RequestMethod.DELETE)
	public APIResult deleteProduct(Principal principal,@PathVariable(name = "productId") String productId) {
		
		final String LOG_STRING = "POST /admin/"+ productId + "/deleted";
		LOGGER.info("Start " + LOG_STRING);
		APIResult result = new APIResult();
		if(productId != null && Long.valueOf(productId) > 0) {
			Product product = productRepo.findByProductId(Long.valueOf(productId));
			if(product != null) {
				List<Order> listOrder = orderRepo.findByProductId(Long.valueOf(productId));
				if(listOrder != null) {
					for (Order order : listOrder) {
						if(order.getState() != 2) {
							orderRepo.delete(order);
						}
					}
				}
				ProductWareHouse productWareHouse  = productWareRepo.findByProductId(Long.valueOf(productId));
				if(productWareHouse != null) {
					productWareRepo.delete(productWareHouse);
				}
				productRepo.delete(product);
			}else {
				result.setErrormessage("Product don't exist!!");
			}
			
		}else {
			result.setErrormessage("Invalid productId!!");
		}
		
		
		LOGGER.info("End " + LOG_STRING);
		return result;

	}
	//API EDAY CATEGORY
		/**
		 * @param product
		 * @return
		 */
		@RequestMapping(value = { "/cat/create" }, method = RequestMethod.POST)
		public APIResult createCat(@RequestBody Category cat) {

			final String LOG_STRING = "POST /admin/cat/create";
			LOGGER.info("Start " + LOG_STRING);
			
			APIResult result = new APIResult();
			
			catRepo.save(cat);
			
			result.setData(cat);

			LOGGER.info("End POST /admin/cat/create");
			return result;

		}
		@RequestMapping(value = { "/cat/list" }, method = RequestMethod.GET)
		public APIResult listCat(@AuthenticationPrincipal UserDetails userDetails) {

			final String LOG_STRING = "GET /admin/cat/list";
			LOGGER.info("Start " + LOG_STRING);
			
			APIResult result = new APIResult();
			List<Category> cats = categoryService.findAll();
			result.setData(cats);

			LOGGER.info("GET /admin/cat/list");
			return result;

		}
		@RequestMapping(value = { "/cat/{catId}/detail" }, method = RequestMethod.GET)
		public APIResult detailCategory(@PathVariable("catId") Long catId, HttpServletRequest request) {
			
			LOGGER.info("Start POST /cat/{catIdId}/detail ");
			APIResult result = new APIResult();
			Category theCat = catRepo.findByCatId(catId);
			
			result.setData(theCat);
			
			LOGGER.info("End POST /cat/{catIdId}/detail ");
			return result;
		}
		
		@RequestMapping(value = { "/cat/update" }, method = RequestMethod.POST)
		public APIResult updateCategory(@RequestBody CatDTO cat, HttpServletRequest request) {
			
			LOGGER.info("Start POST /cat/update ");
			APIResult result = new APIResult();
			Category theCat = catRepo.findByCatId((long) cat.getId());
			theCat.setName(cat.getName());
			catRepo.save(theCat);
			result.setData(theCat);
			
			LOGGER.info("End POST /cat/{catIdId}/detail ");
			return result;
		}
		
		@RequestMapping(value = { "/cat/{catId}/deleted" }, method = RequestMethod.DELETE)
		public APIResult deleteCat(Principal principal,@PathVariable(name = "catId") String catId) {
			
			final String LOG_STRING = "POST /admin/"+ catId + "/deleted";
			LOGGER.info("Start " + LOG_STRING);
			APIResult result = new APIResult();
			if(catId!= null && Integer.parseInt(catId) > 0) {
				
				
					Category theCat = catRepo.findByCatId(Long.valueOf(catId));
					if(theCat != null) {
						List<Product> listProduct = productRepo.getListProductByCatId(Long.valueOf(catId));
						if(listProduct.size() == 0) {
							catRepo.delete(theCat);
							List<Category> cats = categoryService.findAll();
							result.setData(cats);
						}else {
							result.setErrormessage("Catalog contains products . Cannot perform delete !!");
						}
					}else {
						result.setErrormessage("Don't find category!!");
					}
					
				
				
			}else {
				result.setErrormessage("Please check catId!!");
			}
			
			
			LOGGER.info("End " + LOG_STRING);
			return result;

		}
		
		@RequestMapping(value = { "/order/getAll" }, method = RequestMethod.GET)
		public APIResult listOrder( ) {

			final String LOG_STRING = "POST /order/getAll";
			LOGGER.info("Start " + LOG_STRING);
			
			APIResult result = new APIResult();
			List<Order> orders = orderRepo.findAll();
			List<OrderDTO> listOrderDTO = new ArrayList<OrderDTO>();
			if(orders != null) {
				for (Order order : orders) {
					OrderDTO orderDTO = new OrderDTO();
					orderDTO.setId(order.getId());
					User user = userRepository.findById(order.getUser_id());
					orderDTO.setUser(user);
					Product product = productRepo.findByProductId(order.getProId());
					orderDTO.setProduct(product);
					Payment payment = paymentRepo.getPaymentByOrderId(order.getId());
					orderDTO.setPayment(payment);
					orderDTO.setAmount(order.getAmount());
					orderDTO.setPrice(order.getPrice());
					if(order.getState() == 0) {
						orderDTO.setStatus("Add cart");
					}else {
						if(order.getState() == 1) {
							orderDTO.setStatus("Done payment");
						}else {
							if(order.getState() == 2) {
								orderDTO.setStatus("Done buy");
							}
						}
					}
					listOrderDTO.add(orderDTO);
				}
				result.setData(listOrderDTO);
			}else {
				result.setData(orders);
			}
			LOGGER.info("End POST /order/getAll");
			return result;

		}
		
		@RequestMapping(value = { "/order/{orderId}/delete" }, method = RequestMethod.DELETE)
		public APIResult deleteOrder(@PathVariable("orderId") int orderId ) {

			final String LOG_STRING = "POST /admin/order/" + orderId + "/delete";
			LOGGER.info("Start " + LOG_STRING);
			
			APIResult result = new APIResult();
			if(orderId > 0) {
				Order order = orderRepo.findByOrderId(orderId);
				if(order != null ) {
					if(order.getState() == 2) {
						orderRepo.deleteById((long) orderId);
						result.setData(order);
					}else {
						result.setErrormessage("The application isn't completed. You can't delete it");
					}
					
				}else {
					result.setData(order);
					result.setErrormessage("No orders exist!");
				}
			}else {
				result.setErrormessage("Order id must be greater than 0 !");
			}
			LOGGER.info("End GET" + LOG_STRING );
			return result;

		}
		//api staff
		
		
		@RequestMapping(value = { "/detailStaff/{userId}" }, method = RequestMethod.GET)
		public APIResult detailStaff(@PathVariable("userId") Long userId ) {

			final String LOG_STRING = "GET /admin/listStaff";
			LOGGER.info("Start " + LOG_STRING);
			
			APIResult result = new APIResult();
			Worker staff = workerRepo.getStaff(userId);
			result.setData(staff);
			LOGGER.info("End GET /admin/listStaff");
			return result;

		}
		
		@RequestMapping(value = { "/staff/update" }, method = RequestMethod.POST)
		public APIResult updateStaff(@RequestBody StaffDTO staffDTO ) {

			final String LOG_STRING = "GET /admin/staff/update";
			LOGGER.info("Start " + LOG_STRING);
			
			APIResult result = new APIResult();
			Worker staff = workerRepo.getStaffById(staffDTO.getUserId());
			staff.setBranch_card(staffDTO.getBranch_card());
			staff.setCard_number(staffDTO.getCard_number());
			staff.setExperience(staffDTO.getExperience());
			staff.setUnit_salary(staffDTO.getUnit_salary());
			workerRepo.save(staff);
			
			PayRollIdentity payRollIdentity = new PayRollIdentity();
			PayRoll payRoll = new PayRoll();
			payRollIdentity.setMonth(this.getMonthToPayRoll());
			payRollIdentity.setWorkerId(staff.getId());
			payRoll.setPayRollIdentity(payRollIdentity);
			payRoll.setContractSalary(staffDTO.getUnit_salary());
			payRoll.setStatus(false);
			payRoll.setTotalSalary(staffDTO.getUnit_salary());
//			payRollRepository.save(payRoll);
			payRollRepository.insertPayroll(this.getMonthToPayRoll(), staff.getId(),staffDTO.getUnit_salary(),false,staffDTO.getUnit_salary());
//			payRollRepository.updatePayRollBasedOnMonthAndWorkerId(staffDTO.getUnit_salary(), staffDTO.getUnit_salary(), this.getMonthToPayRoll(), staff.getId());
			result.setData(staff);
			LOGGER.info("End GET /admin/staff/update");
			return result;

		}
		
		@RequestMapping(value = "/{workerId}/dayOffOrdayWork/update", method = RequestMethod.POST)
		public APIResult updateDayOffOrDayWorkOfWorker(@AuthenticationPrincipal UserDetails userDetails,
				@RequestBody PayRollDTO payRollDTO, @PathVariable("workerId") String workerId) {
			final String LOG_STRING = "POST /api/v1/worker/" + workerId + "/dayOffOrdayWork/update";
			LOGGER.info("START " + LOG_STRING);
			LOGGER.info(LOG_STRING + "PARAMS userDetails:" + userDetails);
			LOGGER.info(LOG_STRING + "PARAMS payRollDTO:" + payRollDTO);
			LOGGER.info(LOG_STRING + "PARAMS workerId:" + workerId);

			APIResult result = new APIResult();
			try {
				
				WorkerDTO workerDTO = workerService.findWorkerById(Long.valueOf(workerId));
				if (workerDTO == null) {
					result.setErrormessage("Infor worker doesn't exist !");
					return result;
				}
				workerService.updateDayOffOrDayWorkWorker(workerId, payRollDTO);
				PayRoll payRoll = payRollRepository.findPayRollWorkerUnpaidByMonthAndWorkerId(payRollDTO.getMonth(), Long.valueOf(workerId));
				result.setData(payRoll);
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			LOGGER.info("END" + LOG_STRING);
			return result;
		}
		
		@RequestMapping(value = "/callPayroll", method = RequestMethod.POST)
		public APIResult callPayroll(@AuthenticationPrincipal UserDetails userDetails) {
			final String LOG_STRING = "POST /callPayroll" ;
			LOGGER.info("START " + LOG_STRING);
			APIResult result = new APIResult();
			List<Worker> listWorkerEntities = workerRepo.findAll();
			for(Worker worker : listWorkerEntities) {
				String month = this.getMonthToPayRoll();
				PayRoll payRoll = payRollRepository.findPayRollWorkerByMonthAndWorkerId(month, worker.getId());
				if(payRoll != null) {
					Long totalSalary = workerService.setSalaryWorker(payRoll, worker);
					payRoll.setTotalSalary(totalSalary);
					payRollRepository.save(payRoll);
					
				}
			}
			List<PayRoll> listPayRoll = payRollRepository.findAll();
			result.setData(listPayRoll);
			LOGGER.info("END" + LOG_STRING);
			return result;
		}
		
		@RequestMapping(value = "/callDelete", method = RequestMethod.POST)
		public APIResult callDelete(@AuthenticationPrincipal UserDetails userDetails) {
			final String LOG_STRING = "POST /callPayroll" ;
			LOGGER.info("START " + LOG_STRING);
			APIResult result = new APIResult();
			List<PayRoll> listPayRoll = payRollRepository.findAll();
			payRollRepository.deleteAll();
			List<Worker> listWorkerEntities = workerRepo.findAll();
			workerRepo.deleteAll();
			
			result.setData(listPayRoll);
			LOGGER.info("END" + LOG_STRING);
			return result;
		}
		
		public String getMonthToPayRoll() {
			LOGGER.info("Begin Service WorkerService Function getFirstDayOfMonthToPayRoll");
			
	        Date date = new Date();
	        try {
	            String month = DateUtil.convertDateToFormaterString(date);
	            if(!StringUtils.isEmpty(month)) {
	            	return month;
	            }
	        } catch (Exception ex) {
				ex.printStackTrace();
				System.out.println(ex.getMessage());
			}
	        LOGGER.info("End Service WorkerService Function getFirstDayOfMonthToPayRoll");
	        return "";
		}
		
		@RequestMapping(value = { "/staff/filter" }, method = RequestMethod.POST)
		public APIResult filterStaff(Principal principal, @RequestBody FilterUserDTO filterStaff,HttpSession session,
				@AuthenticationPrincipal UserDetails userDetails) {

			APIResult result = new APIResult();
			Gson gson = new Gson();
			Type listType = new TypeToken<FilterUserDTO>() {
			}.getType();
			String filStr = gson.toJson(filterStaff, listType);
			session.setAttribute("filterStaff", filStr);
			List<Map<String, Object>> staffList = userCustome.filterStaff(filterStaff, 1);
			if(staffList != null && staffList.size() > 0) {
				result.setData(staffList);
			}else {
				result.setData("No data available");
			}
			
			return result;
		}
		
		@RequestMapping(value = { "/user/filter" }, method = RequestMethod.POST)
		public APIResult filterCustomer(Principal principal, @RequestBody FilterUserDTO filterCustomer,HttpSession session,
				@AuthenticationPrincipal UserDetails userDetails) {

			APIResult result = new APIResult();
			Gson gson = new Gson();
			Type listType = new TypeToken<FilterUserDTO>() {
			}.getType();
			String filStr = gson.toJson(filterCustomer, listType);
			session.setAttribute("filterStaff", filStr);
			List<Map<String, Object>> staffList = userCustome.filterStaff(filterCustomer, 2);
			if(staffList != null && staffList.size() > 0) {
				result.setData(staffList);
			}else {
				result.setData("No data available");
			}
			
			return result;
		}
}
