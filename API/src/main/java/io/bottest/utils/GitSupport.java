//package io.bottest.utils;
//
//import java.io.File;
//import java.io.IOException;
//
//import org.apache.commons.lang.exception.ExceptionUtils;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.eclipse.jgit.api.Git;
//import org.eclipse.jgit.api.errors.GitAPIException;
//import org.eclipse.jgit.api.errors.InvalidRemoteException;
//import org.eclipse.jgit.api.errors.TransportException;
//import org.eclipse.jgit.lib.RepositoryCache;
//import org.eclipse.jgit.transport.CredentialsProvider;
//import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
//import org.eclipse.jgit.util.FS;
//import org.springframework.beans.factory.annotation.Value;
//
//
//public class GitSupport {
//	
//	private final Logger LOGGER = LogManager.getLogger(GitSupport.class);
//	
//	public CredentialsProvider credentialsProvider;
//	
//	public Git git;
//	
//	
//	public GitSupport(String user, String pass) {
//		credentialsProvider = new UsernamePasswordCredentialsProvider(user, pass);
//	}
//	
//	public void gitClone(String url, String projectId, String resourcePath, String currentUser) {
//		LOGGER.info("Git Support: Clone " + projectId);
//		String path;
//		try {
//			path = new File(resourcePath).getCanonicalPath();
//			git = Git.cloneRepository()
//					  .setURI(url)
//					  .setDirectory(new File(path + "//" + currentUser +"//" + projectId))
//					  .setCredentialsProvider(credentialsProvider)
//					  .call();
//		} catch (IOException e) {
//			LOGGER.info(ExceptionUtils.getStackTrace(e));
//		} catch (InvalidRemoteException e) {
//			LOGGER.info(ExceptionUtils.getStackTrace(e));
//		} catch (TransportException e) {
//			LOGGER.info(ExceptionUtils.getStackTrace(e));
//		} catch (GitAPIException e) {
//			LOGGER.info(ExceptionUtils.getStackTrace(e));
//		}
//		
//		
//	}
//	
//	public void gitAdd() {
//		try {
//			git.add().addFilepattern(".").call();
//		} catch (GitAPIException e) {
//			LOGGER.info(ExceptionUtils.getStackTrace(e));
//		}
//	}
//	
//	public void gitCommit(String message) {
//		try {
//			git.commit().setMessage(message).call();
//		} catch (GitAPIException e) {
//			LOGGER.info(ExceptionUtils.getStackTrace(e));
//		}
//	}
//	
//	public void gitPush() {
//		try {
//			git.push().setCredentialsProvider(credentialsProvider).call();
//		} catch (GitAPIException e) {
//			LOGGER.info(ExceptionUtils.getStackTrace(e));
//		}
//	}
//	
//	public void gitPull() {
//		try {
//			git.pull().setCredentialsProvider(credentialsProvider).call();
//		} catch (GitAPIException e) {
//			LOGGER.info(ExceptionUtils.getStackTrace(e));
//		}
//	}
//	
//	public void gitRemoveFromIndexOrTree(String filePath) {
//		try {
//			git.rm().addFilepattern(filePath).call();
//		} catch (GitAPIException e) {
//			LOGGER.info(ExceptionUtils.getStackTrace(e));
//		}
//	}
//	
//	public boolean checkExistResponsive(String projectId, String resourcePath, String currentUser) {
//		try {
//			String path = new File(resourcePath).getCanonicalPath();
//			if (RepositoryCache.FileKey.isGitRepository(new File(path + "//" + currentUser +"//" + projectId), FS.DETECTED)) {
//			     return false;
//			} 
//			if ((new File(path + "//" + currentUser +"//" + projectId)).exists()) {
//				return false;
//			}
//			return true;
//		} catch (IOException e) {
//			LOGGER.info(ExceptionUtils.getStackTrace(e));
//		}
//		return false;
//		
//	}
//	
//	
//	public void openGitDirectory(String projectId, String resourcePath, String currentUser) {
//		try {
//			String path = new File(resourcePath).getCanonicalPath();
//			git = git.open(new File(path + "//" + currentUser +"//" + projectId));
//		} catch (IOException e) {
//			LOGGER.info(ExceptionUtils.getStackTrace(e));
//		}
//	}
//}
