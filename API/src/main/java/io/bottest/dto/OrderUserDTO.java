package io.bottest.dto;

import java.util.Date;

import io.bottest.jpa.entity.Payment;
import io.bottest.jpa.entity.Product;
import io.bottest.jpa.entity.User;

public class OrderUserDTO {
	private Long id;
	private Product product;
	private Long amount;
	private Long price;
	private String status;
	private String addDay;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public Long getPrice() {
		return price;
	}
	public void setPrice(Long price) {
		this.price = price;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAddDay() {
		return addDay;
	}
	public void setAddDay(String addDay) {
		this.addDay = addDay;
	}
	
}
