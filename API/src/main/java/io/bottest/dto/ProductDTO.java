package io.bottest.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

public class ProductDTO {
private Long id;
	
	private String name;
	
	private Long amount;
	
	private String size;
	
	private Long price_buy;
	
	private Long price_sell;
	
	private int catId;
	
	private Date inputDay;
	
	private String image;
	
	private String media;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public Long getPrice_buy() {
		return price_buy;
	}

	public void setPrice_buy(Long price_buy) {
		this.price_buy = price_buy;
	}

	public Long getPrice_sell() {
		return price_sell;
	}

	public void setPrice_sell(Long price_sell) {
		this.price_sell = price_sell;
	}

	public int getCatId() {
		return catId;
	}

	public void setCatId(int catId) {
		this.catId = catId;
	}

	public Date getInputDay() {
		return inputDay;
	}

	public void setInputDay(Date inputDay) {
		this.inputDay = inputDay;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getMedia() {
		return media;
	}

	public void setMedia(String media) {
		this.media = media;
	}
	
}
