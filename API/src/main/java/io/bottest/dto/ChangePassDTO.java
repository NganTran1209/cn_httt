package io.bottest.dto;

public class ChangePassDTO {

	private String token;
	private String newPassword;
	private String newConfirmPass;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getNewConfirmPass() {
		return newConfirmPass;
	}

	public void setNewConfirmPass(String newConfirmPass) {
		this.newConfirmPass = newConfirmPass;
	}

}
