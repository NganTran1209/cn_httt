package io.bottest.dto;

import java.util.Date;
import io.bottest.jpa.entity.User;

public class StaffDTO {

	private Long id;
	
	private int userId;
	
	private String addr;
	
	private Long unit_salary;
	
	private String card_number;
	
	private String  branch_card;
	
	private Long experience;
	
	private Date createDay;
	
	private Date endDay;
	
	private User user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public Long getUnit_salary() {
		return unit_salary;
	}

	public void setUnit_salary(Long unit_salary) {
		this.unit_salary = unit_salary;
	}

	public String getCard_number() {
		return card_number;
	}

	public void setCard_number(String card_number) {
		this.card_number = card_number;
	}

	public String getBranch_card() {
		return branch_card;
	}

	public void setBranch_card(String branch_card) {
		this.branch_card = branch_card;
	}

	public Long getExperience() {
		return experience;
	}

	public void setExperience(Long experience) {
		this.experience = experience;
	}

	public Date getCreateDay() {
		return createDay;
	}

	public void setCreateDay(Date createDay) {
		this.createDay = createDay;
	}

	public Date getEndDay() {
		return endDay;
	}

	public void setEndDay(Date endDay) {
		this.endDay = endDay;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
}
