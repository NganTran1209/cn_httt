package io.bottest.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import io.bottest.jpa.entity.User;
import io.bottest.jpa.respository.UserRepository;

 
@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

	private final Logger LOGGER = LogManager.getLogger(CustomUserDetailsService.class);
	
	@Autowired
	UserRepository users;
	
	
	@Override
	public User loadUserByUsername(String loginUserName) {	
		return users.getUserByUsername(loginUserName);

	}

	

public boolean checkAuthen(Authentication auth) {
		
		LOGGER.info("Begin Service customUserDetailsService function checkAuthen");
		if (auth == null) {
			LOGGER.info("Service: customUserDetailsService function checkAuthen: Authentication is null");
			LOGGER.info("End Service: customUserDetailsService function checkAuthen: false");
			return false;
		}else {
			if (!(auth.getPrincipal() instanceof UserDetails) ) {
				LOGGER.info("Service: customUserDetailsService function checkAuthen: Principal is not UserDetails class");
				LOGGER.info("End Service: customUserDetailsService function checkAuthen: false");
				return false;
			}else {
				return true;
		}
		
	}
}

	

}
