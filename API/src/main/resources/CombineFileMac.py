#coding: utf-8
from os import walk
import xml.dom.minidom
import os,sys
import sys, os, shutil
from selenium.webdriver.chrome.options import Options
import imutils
import ftplib
import string
import xml.dom.minidom as DOM 
import datetime, xlrd
from skimage.metrics import structural_similarity
import os, sys, importlib, socket, getpass, datetime, shutil,logging
import json
import ast
import pyautogui
import os, sys, importlib, time
import imagehash
import csv
import re
from time import sleep
import operator
from selenium.webdriver import ActionChains
from email.mime.base import MIMEBase
from selenium import webdriver
from shutil import copyfile
import os
import jaconv
import cv2
from email.mime.multipart import MIMEMultipart
import codecs
from selenium.webdriver.support.ui import WebDriverWait
import os,sys, inspect, shutil, getpass, socket, time, datetime
from PIL import Image, ImageDraw
from xml.dom.minidom import parse
import math
from xlwt import Workbook
import threading
import sys, os, importlib
from email.mime.text import MIMEText
import random
from PIL import Image, ImageDraw, ImageFont,ImageGrab
from requests_aws4auth import AWS4Auth
from tempfile import mkstemp
import sys
import datetime
import numpy as np
import openpyxl
from PIL import Image
import sys,os, time
from xml.dom.minidom import Document
import xlrd
import xml.dom.minidom as DOM
from xlrd import open_workbook,cellname
from time import sleep, gmtime, strftime
from random import *
from xml.dom.minidom import parse 
from operator import contains
import argparse
from openpyxl import load_workbook
from email import encoders
from xlutils.copy import copy
from io import BytesIO
from PIL import ImageGrab
import os, sys, inspect, shutil, getpass, socket, time, datetime, importlib
from itertools import repeat
import sys, os
from selenium.webdriver.support.ui import Select
import os, sys, importlib
from selenium.webdriver.common.keys import Keys
import PIL.ImageGrab
import ast 
import smtplib
import openpyxl as px
import requests 
import time


class AC_API(object):
    def getPathFile(xmlPath, executeItem, resourceFolder, fileName):
        indexSplit = xmlPath.split(Constants.SLASH)
        jsonPath = indexSplit[-1]
        tsName = executeItem.testSuiteName
        indexUserName = jsonPath.split(Constants.UNDERSCORE)
        userName = indexUserName[1]
        testDataPath = resourceFolder + Constants.SLASH + tsName + Constants.SLASH_TESTDATA_SLASH
        
        imagePath = os.path.join(testDataPath, fileName)
        return imagePath
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        try:
            method, url, params, authorization, header, body, output = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            
            #params
            discParams = {}
            if params != None and len(params) > 0:
                for pr in params:
                    if pr != None:
                        key = LocatorHelper.get_value_from_variable(pr["Key"], listVariable)
                        value = LocatorHelper.get_value_from_variable(pr["Value"], listVariable)
                        discParams[key] = value
            
            #headers
            dictHeaders = {}
            if header != None and len(header) > 0:
                for hd in header:
                    if hd != None:
                        key = LocatorHelper.get_value_from_variable(hd["Key"], listVariable)
                        value = LocatorHelper.get_value_from_variable(hd["Value"], listVariable)
                        dictHeaders[key] = value
            #authorization
            dictAuths = {}
            tupleBasic = ()
            dictJson = {}
            dataJson = {}
            fileJson = {}
            typeAuths = Constants.BLANK
            if authorization != None and len(authorization) > 0:
                typeAuths = authorization["Type"]
                valueAuts = authorization["Value"]
                if valueAuts != None and len(valueAuts) > 0:
                    if typeAuths == "none":
                            checkT = ""
                    elif typeAuths == "bearerToken":
                        valueToken = LocatorHelper.get_value_from_variable(valueAuts["token"], listVariable)
                        dictHeaders["Authorization"] = 'Bearer ' + valueToken
                    elif typeAuths == "apiKey":
                        key = LocatorHelper.get_value_from_variable(valueAuts["key"], listVariable)
                        value = LocatorHelper.get_value_from_variable(valueAuts["value"], listVariable)
                        
                        if valueAuts["addTo"] == "header":
                            dictHeaders[key] = value
                        else:
                            dataJson[key] = value
                    elif typeAuths == "basicAuth":
                        basicUser = LocatorHelper.get_value_from_variable(valueAuts["username"], listVariable)
                        basicPass = LocatorHelper.get_value_from_variable(valueAuts["password"], listVariable)
                        
                        tupleBasic += (basicUser, basicPass)
                    elif typeAuths == "oauth2":
                        # urlRedirect = ""
                        # if valueAuts[1]["Value"] == "client_credentials":
                        #     for vl in valueAuts:
                        #         key = vl["Key"]
                        #         value = vl["Value"]
                        #         if key != "redirect_uri":
                        #             dataJson[key] = value
                        #         else:
                        #             urlRedirect = value
                        #     accessToken = requests.post(url = urlRedirect, data = dataJson)
                        #     accessTokenJson = json.loads(accessToken.content)
                        #     dictHeaders["Authorization"] = 'Bearer ' + accessTokenJson["access_token"]
                        accessToken = valueAuts["accessToken"]
                        dictHeaders["Authorization"] = 'Bearer ' + accessToken
                    if typeAuths == "awsSignature":
                        accessKey = LocatorHelper.get_value_from_variable(valueAuts["AccessKey"], listVariable)
                        secretKey = LocatorHelper.get_value_from_variable(valueAuts["SecretKey"], listVariable)
                        awsRegion = LocatorHelper.get_value_from_variable(valueAuts["AWSRegion"], listVariable)
                        serviceName = LocatorHelper.get_value_from_variable(valueAuts["ServiceName"], listVariable)
                        sessionToken = LocatorHelper.get_value_from_variable(valueAuts["SessionToken"], listVariable)
                        tupleBasic = AWS4Auth(accessKey, secretKey, awsRegion, serviceName, sessionToken)
            #body
            if body != None and len(body) > 0:
                typeBody = body["Type"]
                valueBody = body["Value"]
                if typeBody == "raw":
                    dictJson = valueBody
                elif typeBody == "binary":
                    pathFile = AC_API.getPathFile(xmlPath, executeItem, resourceFolder, valueBody)
                    valuePath = open(pathFile, 'rb').read()
                    dataJson = valuePath
                    dictHeaders["Content-Type"] = "application/octet-stream"
                if valueBody != None and len(valueBody) > 0:
                    for vB in valueBody:
                        if typeBody == "none":
                            dictJson = {}
                        elif typeBody == "form-data":
                            key = LocatorHelper.get_value_from_variable(vB["Key"], listVariable)
                            typeS = vB["Type"]
                            value = LocatorHelper.get_value_from_variable(vB["Value"], listVariable)
                            
                            if typeS == "text":
                                dataJson[key] = value
                            elif typeS == "file":
                                pathFile = AC_API.getPathFile(xmlPath, executeItem, resourceFolder, value)
                                valueMain = open(pathFile, 'rb')
                                fileJson[key] = valueMain
                        elif typeBody == "x-www-form-urlencoded":
                            key = vB["Key"]
                            value = vB["Value"]
                            dataJson[key] = value
                        # elif typeBody == "grapQL":
                        #     key = vB["Key"]
                        #     value = vB["Value"]
                        #     if key == "variables":
                        #         if value != "":
                        #             value = ast.literal_eval(value)
                        #     dictJson[key] = value
            
            if method == "post":
                response = requests.post(url = url, headers = dictHeaders, params = discParams, json = dictJson, data = dataJson, files = fileJson, auth = tupleBasic)
            elif method == "get":
                response = requests.get(url = url, headers = dictHeaders, params = discParams, json = dictJson, data = dataJson, files = fileJson, auth = tupleBasic) 
            dataText = response.text
            statusCode = response.status_code
            
            try:
                dataResponse = json.loads(dataText)
            except Exception as e:
                dataResponse = dataText
            itemMethod= str(tAction.params[0].name + Constants.COLON + tAction.params[0].value)
            listParamsShow1.append(itemMethod)
            itemUrl = str(tAction.params[1].name + Constants.COLON + tAction.params[1].value)
            listParamsShow1.append(itemUrl)
            itemParams = str(tAction.params[2].name + Constants.COLON + tAction.params[1].value)
            listParamsShow1.append(itemParams)
            itemAuthorization = str(tAction.params[3].name + Constants.COLON + str(tAction.params[3].value))
            listParamsShow1.append(itemAuthorization)
            itemHeader = str(tAction.params[4].name + Constants.COLON + str(tAction.params[4].value))
            listParamsShow1.append(itemHeader)
            itemBody = str(tAction.params[5].name + Constants.COLON + str(tAction.params[5].value))
            listParamsShow1.append(itemBody)
            itemOutput= str(tAction.params[6].name + Constants.COLON + str(dataResponse))
            listParamsShow1.append(itemOutput)
            
            if typeAuths != "awsSignature":
                dataResponse = LocatorHelper.set_value_from_variable(output, dataResponse, listVariable)
            if int(statusCode) == 200:
                res = RESULT(
                    Constants.RESULT_OK,
                    statusCode,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    dataText,
                    Constants.EMPTY_STRING
                )
            else:
                res = RESULT(
                    Constants.RESULT_NOK,
                    str(statusCode) +" - "+ response.raise_for_status(),
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
        except Exception as ex:
            print("Error Action API")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_CaptureControl():
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        
        try:
            screen, control, index, fileName  = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            if index == None:
                index = 0
            resl = fileName.split(Constants.DOT)[1]
            
            getValue = LocatorHelper.get_value_from_variable(fileName, listVariable)
            
            if "testData" not in fileName and "testResult" not in fileName:
                listData = getValue.split("/")
                nameFolder = listData[-2]
                if nameFolder == "TestDatas":
                    fileName = "$testData/" + listData[-1]
            
            imagePath = LocatorHelper.get_value_from_variable(getValue, listVariable)
            
            elements = LocatorHelper.get_element_by_locator2(driver, xmlPath, screen, executeItem, control, resourceFolder, listVariable, index)
            if elements != None:
                k = 0
                if type(elements) is list:
                    for element in elements:
                        if k == 0:
                            count = ""
                        else:
                            k += 1
                            count = "_" + str(k)
                        device_pixel_ratio = driver.execute_script('return window.devicePixelRatio')
                        location = element.location
                        size = element.size
                        driver.save_screenshot(imagePath + count)
                        x = location['x']*device_pixel_ratio
                        y = location['y']*device_pixel_ratio
                        width = location['x']*device_pixel_ratio + size['width']*device_pixel_ratio
                        height = location['y']*device_pixel_ratio + size['height']*device_pixel_ratio
                        im = Image.open(imagePath + count)
                        im = im.crop((int(x), int(y), int(width), int(height)))
                        im.save(imagePath + count)
                else:
                    count = ""
                    device_pixel_ratio = driver.execute_script('return window.devicePixelRatio')
                    location = elements.location
                    size = elements.size
                    driver.save_screenshot(imagePath + count)
                    x = location['x']*device_pixel_ratio
                    y = location['y']*device_pixel_ratio
                    width = location['x']*device_pixel_ratio + size['width']*device_pixel_ratio
                    height = location['y']*device_pixel_ratio + size['height']*device_pixel_ratio
                    im = Image.open(imagePath + count)
                    im = im.crop((int(x), int(y), int(width), int(height)))
                    im.save(imagePath + count)
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.EMPTY_STRING,
                    fileName,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                res = RESULT(
                    Constants.RESULT_NOK,
                    "element is none",
                    fileName,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            #write log file
            itemScreen = str("Screen" + Constants.COLON + screen)
            listParamsShow1.append(itemScreen)
            itemControl = str("Item" + Constants.COLON + control)
            listParamsShow1.append(itemControl)
            itemIndex = str("Index" + Constants.COLON + str(index))
            listParamsShow1.append(itemIndex)
            itemFileName = str("File name" + Constants.COLON + fileName)
            listParamsShow1.append(itemFileName)
                
        except Exception as ex:
            print (ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_CaptureImage():
    def execute(self, element, imagePath):
        x0 = 0.0
        y0 = 0.0
        x1 = 0.0
        y1 = 0.0
        
        try:
            snapshot = PIL.ImageGrab.grab()            
            if element != None:
                # get x
                if element.location['x'] - 7.0 < 0:
                    x0 = 0.0
                else:
                    x0 = element.location['x'] + 50
                # get y
                if element.location['y'] - 7 < 0:
                    y0 = 0.0
                else:
                    y0 = element.location['y'] + 7
                # get width
                if element.size['width'] + 14.0 < 0:
                    x1 = snapshot.width
                else:
                    x1 = x0 + element.size['width'] + 14
                # get height
                if element.size['height'] + 14.0 < 0:
                    y1 = snapshot.height
                else:
                    y1 = y0 + element.size['height'] + 14
                dr = ImageDraw.Draw(snapshot)
                dr.rectangle((int(x0),int(y0),int(x1),int(y1)), outline = "red")
            snapshot.save(imagePath)
            res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        except Exception as ex:
            res = RESULT(
                Constants.RESULT_CRASH,
                ex.message,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_CaptureScreen:
    def save_fullpage_screenshot(self, driver, output_path, tmp_prefix, tmp_suffix):
        window_height = driver.execute_script('return window.innerHeight')
        scroll_height = driver.execute_script('return document.body.parentNode.scrollHeight')
        num = int( math.ceil( float(scroll_height) / float(window_height) ) )
        tempfiles = []
        for i in xrange( num ):
            fd,path = mkstemp(prefix='{0}-{1:02}-'.format(tmp_prefix, i+1), suffix=tmp_suffix)
            os.close(fd)
            tempfiles.append(path)
            pass
        try:
            # take screenshots
            for i,path in enumerate(tempfiles):
                if i > 0:
                    driver.execute_script( 'window.scrollBy(%d,%d)' % (0, window_height) )
                
                driver.save_screenshot(path)
                pass
            # stitch images together
            stiched = None
            for i,path in enumerate(tempfiles):
                img = Image.open(path)
                w, h = img.size
                y = i * window_height
                
                if i == ( len(tempfiles) - 1 ):
                    img = img.crop((0, h-(scroll_height % h), w, h))
                    w, h = img.size
                    pass
                
                if stiched is None:
                    stiched = Image.new('RGB', (w, scroll_height))
                
                stiched.paste(img, (
                    0, # x0
                    y, # y0
                    w, # x1
                    y + h # y1
                ))
                pass
            stiched.save(output_path)
        finally:
            # cleanup
            for path in tempfiles:
                if os.path.isfile(path):
                    os.remove(path)
            pass
    
        return output_path
    def execute(self, driver, imagePath):
        try:
            if driver is None:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )  
            else:
                image = self.save_fullpage_screenshot(driver, imagePath, 'selenium_screenshot', Constants.EXT_PNG)
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )  
        except:
            res = RESULT(
                Constants.RESULT_CRASH,
                sys.exc_info()[0],
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_CheckAlertMessage():
    """description of class"""
    def execute(self, xmlPath, driver, executeItem):
        try:
            tAction = executeItem.testAction
            valueCheck = tAction.params[0].value
            tcId = executeItem.testCaseId
 
            # get value in variable
            if valueCheck[:1] == Constants.SHARP:
                DOMTree = DOM.parse(xmlPath)
                lstVariables = DOMTree.getElementsByTagName(Constants.TAG_VARIABLE)
                for i in range(len(lstVariables)):
                    if valueCheck[1:] == lstVariables[i].getElementsByTagName(Constants.TAG_NAME)[0].firstChild.nodeValue:
                        valueCheck = lstVariables[i].getElementsByTagName(Constants.TAG_VALUE)[0].firstChild.nodeValue
                        break
            valueAlert = self.isCheckAlert(driver)
            if valueAlert != False:
                if valueCheck == valueAlert.text:
                    res = RESULT(
                        Constants.RESULT_PASSED,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_OBJECT,
                        valueCheck,
                        valueAlert.text,
                        Constants.EMPTY_STRING
                    )
                else:
                    res = RESULT(
                        Constants.RESULT_FAILED,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_OBJECT,
                        valueCheck,
                        valueAlert,
                        Constants.EMPTY_STRING
                    )
            else:
                res = RESULT(
                        Constants.RESULT_CRASH,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_OBJECT,
                        valueCheck,
                        'Alert is not exist!',
                        Constants.EMPTY_STRING
                    )
            acCaptureImage = AC_TakeScreenshot()
            path, file_name = os.path.split(xmlPath)
            timeNow = datetime.datetime.now().strftime (Constants.FORMAT_TIME)
            imageName = tcId + Constants.SEPARATOR + timeNow + Constants.SEPARATOR +  Constants.AC_CHECK_ALERT_MESSAGE
            imagePath = os.path.join(path,Constants.EVIDENCE,imageName + Constants.EXT_PNG)
            acCaptureImage.execute(None, imagePath)
        except Exception as ex:
            res = RESULT(
                Constants.RESULT_CRASH,
                ex.message,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res
    def isCheckAlert(self, driver):
        try:
            alert = driver.switch_to.alert
            alert.text
            return alert
        except NoAlertPresentException:
            return False


class AC_CheckCellColor:
    def get_cell_range(self,start_col, start_row, sheet, book,sheetPage):
        sheet = book.sheet_by_name(sheetPage)
        xfx = sheet.cell_xf_index(start_row, start_col)
        xf = book.xf_list[xfx]
        bgx = xf.background.pattern_colour_index
        return bgx
    def execute(self,executeFilePath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        
        try:
            tAction = executeItem.testAction
            # file_name = tAction.params[0].value
            # sheetPage = tAction.params[1].value
            # row = tAction.params[2].value
            # column = tAction.params[3].value
            # result = tAction.params[4].value
            file_name, sheetPage, row, column, result = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            
            lstVariable = []
            color = Constants.BLANK
            booleanCheckColor = Constants.FALSE
            
            fileXlsx = LocatorHelper.get_value_from_variable(file_name , listVariable)
            row = LocatorHelper.get_value_from_variable(row , listVariable)
            column = LocatorHelper.get_value_from_variable(column , listVariable)
            nameFileStr = os.path.basename(fileXlsx).split(Constants.DOT)[1]
            if nameFileStr == Constants.XLS:
                book = open_workbook(fileXlsx, formatting_info=True)
                sheet = book.sheet_by_name(sheetPage)
                data = self.get_cell_range(int(row), int(column), sheet, book,sheetPage) 
                colorDict = book.colour_map
                bgxColor = colorDict[data]
                hexColor = Constants.SHARP+Constants.EMPTY_STRING.join(map(chr, bgxColor)).encode(Constants.HEX)
                
                color = hexColor.lower()
            else:
                book = px.load_workbook(fileXlsx, data_only=True)
                sheet = book[sheetPage]
                colorStr = sheet.cell(int(row),int(column)).fill.start_color.index
                colorHex = Constants.SHARP+colorStr[2:]
                color = colorHex.lower()
            
            result = LocatorHelper.get_value_from_variable(result , listVariable)
            if result == color:
                booleanCheckColor = Constants.TRUE
            else:
                booleanCheckColor = Constants.FALSE
            
            #write log file
            itemFileName = str(tAction.params[0].name + Constants.COLON + tAction.params[0].value)
            listParamsShow1.append(itemFileName)
            itemSheet = str(tAction.params[1].name + Constants.COLON + tAction.params[1].value)
            listParamsShow1.append(itemSheet)
            itemRange = str(tAction.params[2].name + Constants.COLON + tAction.params[2].value)
            listParamsShow1.append(itemRange)
            itemResult = str(tAction.params[3].name + Constants.COLON + result)
            listParamsShow1.append(itemResult)
            
            if booleanCheckColor == Constants.TRUE:
                res = RESULT(
                    Constants.RESULT_OK,
                    booleanCheckColor,
                    Constants.EMPTY_STRING,
                    color,
                    result,
                    Constants.EMPTY_STRING
                )
            else:
                res = RESULT(
                    Constants.RESULT_NOK,
                    booleanCheckColor,
                    Constants.EMPTY_STRING,
                    color,
                    result,
                    Constants.EMPTY_STRING
                )
        except Exception as ex:
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_CheckCellValue:
    def get_cell_range(self,start_col, start_row, sheet, book,sheetPage):
        sheet = book.sheet_by_name(sheetPage)
        result = Constants.BLANK
        
        try:
            result = sheet.cell(start_row, start_col).value
            code = Constants.RESULT_OK
        except Exception as e:
            result = e
            code = Constants.RESULT_CRASH
        return (result,code)
    def execute(self,executeFilePath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        
        try:
            tAction = executeItem.testAction
            # fileXl = tAction.params[0].value
            # sheetPage = tAction.params[1].value
            # row = tAction.params[2].value
            # column = tAction.params[3].value
            # result = tAction.params[4].value
            fileXl, sheetPage, row, column, result = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            fileXlsx = LocatorHelper.get_value_from_variable(fileXl , listVariable)
            sheetPage = LocatorHelper.get_value_from_variable(sheetPage , listVariable)
            row = LocatorHelper.get_value_from_variable(row , listVariable)
            column = LocatorHelper.get_value_from_variable(column , listVariable)
            nameFileStr = os.path.basename(fileXlsx).split(Constants.DOT)[1]
            if nameFileStr.lower() == "xls":
                book = open_workbook(fileXlsx)
                sheet = book.sheet_by_name(sheetPage)
                data,code = self.get_cell_range(int(row), int(column), sheet, book,sheetPage)
            else:
                import openpyxl
                book = openpyxl.load_workbook(fileXlsx)
                sheet = book[sheetPage]
                data = sheet.cell(row=int(row), column=int(column))
                # data = valueCell.value
                code = Constants.RESULT_OK
            strData = Constants.BLANK 
            boolValue = Constants.FALSE
            if code == Constants.RESULT_OK:
                if type(data.value) is float:
                    index = str(data.value).split(Constants.DOT)[1]
                    if int(index) == 0:
                        strData = str(data.value).split(Constants.DOT)[0]
                    else:
                        strData = str(data.value)
                else:
                    strData = data.value
                
                result = LocatorHelper.get_value_from_variable(result , listVariable)
                
                if result == strData:
                    boolValue = Constants.TRUE
                else:
                    boolValue = Constants.FALSE
                
                #write log file
                itemFileName = str(tAction.params[0].name + Constants.COLON + tAction.params[0].value)
                listParamsShow1.append(itemFileName)
                itemSheet = str(tAction.params[1].name + Constants.COLON + tAction.params[1].value)
                listParamsShow1.append(itemSheet)
                itemRow = str(tAction.params[2].name + Constants.COLON + str(row))
                listParamsShow1.append(itemRow)
                itemColumn = str(tAction.params[3].name + Constants.COLON + str(column))
                listParamsShow1.append(itemColumn)
                itemResult = str(tAction.params[4].name + Constants.COLON + result)
                listParamsShow1.append(itemResult)
                if boolValue == Constants.TRUE:
                    res = RESULT(
                        Constants.RESULT_OK,
                        boolValue,
                        Constants.EMPTY_STRING,
                        str(strData),
                        result,
                        Constants.EMPTY_STRING
                    )
                else:
                    res = RESULT(
                        Constants.RESULT_NOK,
                        boolValue,
                        Constants.EMPTY_STRING,
                        str(strData),
                        result,
                        Constants.EMPTY_STRING
                    )
            else:
                res = RESULT(
                    Constants.RESULT_CRASH,
                    str(data),
                    Constants.EMPTY_STRING,
                    boolValue,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
        except Exception as ex:
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_CheckControlAttribute():
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        try:
            
            tAction = executeItem.testAction
            screen, control, index, attribute, valueCheck = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            if index == None:
                index = 0
            booleanStr = Constants.EMPTY_STRING
            value = Constants.BLANK
            global element
            element = None
            if driver is None:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                elements = LocatorHelper.get_element_by_locator2(driver, xmlPath, screen, executeItem, control, resourceFolder, listVariable, index)
                
                attribute = LocatorHelper.get_value_from_variable(attribute, listVariable)
                
                if type(elements) is list:
                    for element in elements:
                        value = element.get_attribute(attribute)
                else:
                    value = elements.get_attribute(attribute)
                if value != None:
                    valueCheck = LocatorHelper.get_value_from_variable(valueCheck, listVariable)
                    if value == valueCheck:
                        booleanStr = Constants.TRUE
                    else:
                        booleanStr = Constants.FALSE
                else:
                    value = "Attribute not exist"
                    booleanStr = Constants.FALSE
                
                if tAction.id in listVariable.keys():
                    imageIdex = listVariable[tAction.id] + 1
                else:
                    imageIdex = 1
                imageNameF = str(imageIdex)+"_" + tAction.id+ Constants.EXT_PNG
                imagePath = os.path.join(listVariable["testResult"], imageNameF)
                listVariable[tAction.id] = imageIdex
                if type(elements) is list:
                    k = 0
                    for element in elements:
                        if k == 0:
                            count = ""
                        else:
                            k += 1
                            count = "_" + str(k)
                        LocatorHelper.take_screenshot_of_specific_element(driver, element, imagePath + count, browserName)
                else:
                    LocatorHelper.take_screenshot_of_specific_element(driver, elements, imagePath, browserName)
                #write log file
                itemScreen = str("Screen" + Constants.COLON + screen)
                listParamsShow1.append(itemScreen)
                itemControl = str("Item" + Constants.COLON + control)
                listParamsShow1.append(itemControl)
                itemIndex = str("Idex" + Constants.COLON + str(index))
                listParamsShow1.append(itemIndex)
                itemAttribute = str("Attribute" + Constants.COLON + attribute)
                listParamsShow1.append(itemAttribute)
                itemValueCheck = str("Value" + Constants.COLON + valueCheck)
                listParamsShow1.append(itemValueCheck)
                if booleanStr == Constants.TRUE:
                    res = RESULT(
                        Constants.RESULT_OK,
                        Constants.EMPTY_STRING,
                        imageNameF,
                        valueCheck,
                        value,
                        Constants.EMPTY_STRING
                    )
                else:
                    res = RESULT(
                        Constants.RESULT_NOK,
                        "false",
                        imageNameF,
                        valueCheck,
                        value,
                        Constants.EMPTY_STRING
                    )
                
        except Exception as ex:
            print (ex)
            res = RESULT(               
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_CheckControlExist:
    def execute(self, projectPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        try:
            tAction = executeItem.testAction
            # screen = LocatorHelper.getValueFromParam(tAction.params, "Screen")
            # control = LocatorHelper.getValueFromParam(tAction.params, "Item")
            index = LocatorHelper.getValueFromParam(tAction.params, "Index")
            screen, control = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            if index == None:
                index = 0
            element = None
            if driver is None:
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                elements = LocatorHelper.get_element_by_locator2(driver, projectPath, screen, executeItem, control, resourceFolder, listVariable, index)
                
                if elements != None:
                    booleanCheck = Constants.TRUE
                else:
                    booleanCheck = Constants.FALSE
            
            #write log file
            itemLayout = str("Screen" + Constants.COLON + screen)
            listParamsShow1.append(itemLayout)
            itemControl = str("Item" + Constants.COLON + control)
            listParamsShow1.append(itemControl) 
            itemIndex = str("Index" + Constants.COLON + str(index))
            listParamsShow1.append(itemIndex) 
            res = RESULT(
                Constants.RESULT_OK,
                booleanCheck,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        except Exception as ex:
            print (ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                ex,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_CheckControlFocus():
    """description of class"""
    def import_class_from_string(self, moduleName, className):
        from importlib import import_module
        mod = import_module(moduleName+ Constants.DOT +className)
        klass = getattr(mod, className)
        return klass
    def execute(self, projectPath, driver ,executeItem):
        try:
            tAction = executeItem.testAction
            tsName = executeItem.testSuiteName
            tcId = executeItem.testCaseId
            acRow = tAction.row
            element = None
            acDoesControlExist = eval( Constants.AC_DOES_CONTROL_EXIST)
            resDoesControlExist = acDoesControlExist().execute(projectPath, driver, executeItem)
            if resDoesControlExist.resultCode == Constants.RESULT_NOK:
                res = RESULT(
                    Constants.RESULT_CRASH,
                    Constants.MSG_CONTROL_NOT_EXIST,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
                return res
            elif resDoesControlExist.resultCode == Constants.RESULT_CRASH and resDoesControlExist.resultMsg == Constants.MSG_LAYOUT_FILE_NOT_EXIST:
                res = RESULT(
                    Constants.RESULT_CRASH,
                    Constants.MSG_LAYOUT_FILE_NOT_EXIST,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
                return res
            else:
                element = resDoesControlExist.resultObj
                currentElement = driver.switch_to.active_element
                if element == currentElement:
                    res = RESULT(
                        Constants.RESULT_PASSED,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_OBJECT,
                        'Control is focus',
                        'Control is focus',
                        Constants.EMPTY_STRING
                    )
                else:
                    res = RESULT(
                        Constants.RESULT_FAILED,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_OBJECT,
                        'Control is focus',
                        'Control is not focus',
                        Constants.EMPTY_STRING
                    )
                acCaptureImage = AC_TakeScreenshot()
                folders = projectPath.split(Constants.DOUBLE_BACKSLASH)
                projectName = folders[len(folders)-1]
                reportPath = os.path.join(projectPath, Constants.REPORTS, projectName)
                imageName = tsName + Constants.SEPARATOR + tcId + Constants.SEPARATOR + acRow + Constants.SEPARATOR + Constants.AC_CHECK_CONTROL_EXIST
                imagePath = os.path.join(reportPath,Constants.EVIDENCE,imageName + Constants.EXT_PNG)
                acCaptureImage.execute(element, imagePath)
              
        except Exception as ex:
            res = RESULT(
                Constants.RESULT_CRASH,
                ex.message,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_CheckControlNotExist:
    def import_class_from_string(self, moduleName, className):
        from importlib import import_module
        mod = import_module(moduleName+ Constants.DOT +className)
        klass = getattr(mod, className)
        return klass
    def execute(self, projectPath, driver ,executeItem):
        try:
            tAction = executeItem.testAction
            tsName = executeItem.testSuiteName
            tcId = executeItem.testCaseId
            acRow = tAction.row
            element = None
            acDoesControlExist = eval( Constants.AC_DOES_CONTROL_EXIST)
            resDoesControlExist = acDoesControlExist().execute(projectPath, driver, executeItem)
            if resDoesControlExist.resultCode == Constants.RESULT_NOK:
                res = RESULT(
                    Constants.RESULT_PASSED,
                    Constants.MSG_CONTROL_NOT_EXIST,
                    Constants.EMPTY_OBJECT,
                    Constants.MSG_CONTROL_NOT_EXIST,
                    Constants.MSG_CONTROL_NOT_EXIST,
                    Constants.EMPTY_STRING
                )
            elif resDoesControlExist.resultCode == Constants.RESULT_CRASH and resDoesControlExist.resultMsg == Constants.MSG_LAYOUT_FILE_NOT_EXIST:
                res = RESULT(
                    Constants.RESULT_CRASH,
                    Constants.MSG_LAYOUT_FILE_NOT_EXIST,
                    Constants.EMPTY_OBJECT,
                    Constants.MSG_CONTROL_IS_EXIST,
                    Constants.MSG_LAYOUT_FILE_NOT_EXIST,
                    Constants.EMPTY_STRING
                )
            else:
                element = resDoesControlExist.resultObj
                res = RESULT(
                    Constants.RESULT_FAILED,
                    Constants.MSG_CONTROL_IS_EXIST,
                    Constants.EMPTY_OBJECT,
                    Constants.MSG_CONTROL_NOT_EXIST,
                    Constants.MSG_CONTROL_IS_EXIST,
                    Constants.EMPTY_STRING
                )
            acCaptureImage = AC_TakeScreenshot()
            folders = projectPath.split(Constants.DOUBLE_BACKSLASH)
            projectName = folders[len(folders)-1]
            reportPath = os.path.join(projectPath, Constants.REPORTS, projectName)
            imageName = tsName + Constants.SEPARATOR + tcId + Constants.SEPARATOR + acRow + Constants.SEPARATOR + Constants.AC_CHECK_CONTROL_EXIST
            imagePath = os.path.join(reportPath,Constants.EVIDENCE,imageName + Constants.EXT_PNG)
            acCaptureImage.execute(element, imagePath)
        except Exception as ex:
            res = RESULT(
                Constants.RESULT_CRASH,
                ex.message,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_CheckControlValue():
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder,  listParamsShow1, browserName):
        tAction = executeItem.testAction
        try:
            screen, control, index, operator, expected = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            if index == None:
                index = 0
            recorded = Constants.EMPTY_STRING
            boolValue = Constants.EMPTY_STRING
            
            if tAction.id in listVariable.keys():
                imageIdex = listVariable[tAction.id] + 1
            else:
                imageIdex = 1
            imageNameF = str(imageIdex)+"_" + tAction.id+ Constants.EXT_PNG
            imagePath = os.path.join(listVariable["testResult"], imageNameF)
            listVariable[tAction.id] = imageIdex
            elements = LocatorHelper.get_element_by_locator2(driver, xmlPath, screen, executeItem, control, resourceFolder, listVariable, index)
            
            variables = CheckHelper.get_control_value(driver, xmlPath, screen, executeItem, control, resourceFolder, listVariable, index)
            
            if variables == "Action not support!":
                res = RESULT(
                    Constants.RESULT_NOK,   
                    "Action not support!",
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                expected = LocatorHelper.get_value_from_variable(expected, listVariable)
                recorded = LocatorHelper.get_value_from_variable(variables, listVariable)
                
                boolValue = CheckHelper.check_value_helper(driver, recorded, expected, listVariable,operator)
                
                for element in elements:
                    #take screenshot of a specific element
                    LocatorHelper.take_screenshot_of_specific_element(driver, element, imagePath, browserName)
                
                #write log file
                itemScreen = str("Screen" + Constants.COLON + screen)
                listParamsShow1.append(itemScreen)
                itemControl = str("Control" + Constants.COLON + control)
                listParamsShow1.append(itemControl)
                itemIndex = str("Index" + Constants.COLON + str(index))
                listParamsShow1.append(itemIndex)
                itemOperator = str("Operator" + Constants.COLON + operator)
                listParamsShow1.append(itemOperator)
                itemExpected = str("Expected" + Constants.COLON + variables)
                listParamsShow1.append(itemExpected)
                
                if boolValue == Constants.TRUE:
                    res = RESULT(
                        Constants.RESULT_OK,
                        Constants.BLANK,
                        imageNameF,
                        expected,
                        recorded,
                        Constants.EMPTY_STRING 
                    )
                else:
                    res = RESULT(
                        Constants.RESULT_NOK,
                        "false",
                        imageNameF,
                        expected,
                        recorded,
                        Constants.EMPTY_STRING
                    )
        except Exception as ex:
            print (ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_CheckFileExist:
    def execute(self, projectPath, driver, executeItem):       
        tAction = executeItem.testAction
        # filePath = tAction.params[0].value
              
        try:                  
            filePath = LocatorHelper.getValueFromParam(tAction.params, tAction.id) 
            if os.path.isfile(filePath):   
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.MSG_FILE_IS_EXIST,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else :
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_FILE_IS_NOT_EXIST,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )                
        except Exception as ex :            
            res = RESULT(
                Constants.RESULT_NOK,
                ex,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_CheckFileExistFTP():
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        try:
            connectionName, remotePath, typeFile, result = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            if driver is None:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                connectionValue = listVariable[connectionName]
                hostFtp = connectionValue["Host"]
                portFtp = connectionValue["Port"]
                usernameFtp = connectionValue["Username"]
                passwordFtp = connectionValue["Password"]
                ftp = ftplib.FTP()
                ftp.connect(hostFtp, int(portFtp))
                
                ftp.login(usernameFtp, passwordFtp)
                data = []
                ftp.dir(data.append)
                remotePath = LocatorHelper.get_value_from_variable(remotePath, listVariable)
                typeFile = LocatorHelper.get_value_from_variable(typeFile, listVariable)
                ftp.cwd(remotePath)
                file_list = []
                ftp.retrlines("NLST", file_list.append)
                boolean = Constants.BLANK
                for i in file_list:
                    check = i.split(Constants.DOT).pop(-1)
                    if check == typeFile:
                        boolean = Constants.TRUE
                    else:
                        boolean = Constants.FALSE
                
                ftp.quit()
                boolean = LocatorHelper.set_value_from_variable(result, boolean, listVariable)
                #Write log file
                itemConnectionName = str(tAction.params[0].name + Constants.COLON + tAction.params[0].value)
                listParamsShow1.append(itemConnectionName)
                itemRemotePath = str(tAction.params[1].name + Constants.COLON + remotePath)
                listParamsShow1.append(itemRemotePath)
                itemTypefile = str(tAction.params[2].name + Constants.COLON + typeFile)
                listParamsShow1.append(itemTypefile)
                itemResult = str(tAction.params[3].name + Constants.COLON + boolean)
                listParamsShow1.append(itemResult)
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
        except Exception as ex:
            print("Error action check file exist ftp")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_CheckFileNotExit:
    def execute(self, projectPath, driver, executeItem):
        tAction = executeItem.testAction
        try:
            filePath = LocatorHelper.getValueFromParam(tAction.params, tAction.id)                 
            if os.path.isfile(filePath):   
                res = RESULT(
                Constants.RESULT_OK,
                Constants.MSG_SELECT_NFILE,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
                )
            else :
                res = RESULT(
                Constants.RESULT_NOK,
                Constants.MSG_SELECT_FILE,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
                )                
        except Exception as ex :            
            res = RESULT(
                Constants.RESULT_NOK,
                ex,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_CheckFolderExist:
    def execute(self, projectPath, driver, executeItem):
        tAction = executeItem.testAction
        # folderPath = tAction.params[0].value
        folderPath = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
        try:
            if os.path.exists(folderPath):
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.MSG_FOLDER_IS_EXIST,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                )
            else:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_FOLDER_IS_NOT_EXIST,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                ) 
        except Exception as ex :            
            res = RESULT(
                Constants.RESULT_NOK,
                ex,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_CheckItemCssValue:
    def import_class_from_string(self, moduleName, className):
        from importlib import import_module
        mod = import_module(moduleName + Constants.DOT + className)
        klass = getattr(mod, className)
        return klass
    def execute(self,projectPath, driver, executeItem):
        tAction = executeItem.testAction
        tsName = executeItem.testSuiteName
        tsFilePath = os.path.join(projectPath,Constants.TAG_TESTSUITES,tsName + Constants.EXT_XML)
        attribute = tAction.params[2].value
        valueCheck = tAction.params[3].value
        acDoesControlExist = eval( Constants.AC_DOES_CONTROL_EXIST)
        global element
        element = None
        try:
            if valueCheck[:1] == Constants.SHARP:
                DOMTree = DOM.parse(tsFilePath)
                lstVariables = DOMTree.getElementsByTagName(Constants.TAG_VARIABLE)
                for i in range(len(lstVariables)):
                    if valueCheck[1:] == lstVariables[i].getElementsByTagName(Constants.TAG_NAME)[0].firstChild.nodeValue:
                        valueCheck = lstVariables[i].getElementsByTagName(Constants.TAG_VALUE)[0].firstChild.nodeValue
                        break
            result = acDoesControlExist().execute(projectPath, driver, executeItem)
            element = result.resultObj
            variable = element.value_of_css_property(attribute)
            if attribute == Constants.ATTR_COLOR or attribute == Constants.ATTR_BACKGROUNDCOLOR:
                variable = variable.replace(Constants.RGBA + Constants.OPEN_PARENTHESIS, Constants.EMPTY_STRING).replace(Constants.CLOSE_PARENTHESIS, Constants.EMPTY_STRING).replace(Constants.SPACE, Constants.EMPTY_STRING)
            if valueCheck == variable:
                res = RESULT(
                    Constants.RESULT_PASSED,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_OBJECT,
                    valueCheck,
                    variable,
                    Constants.EMPTY_STRING
                )
            else:
                res = RESULT(
                    Constants.RESULT_FAILED,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_OBJECT,
                    valueCheck,
                    variable,
                    Constants.EMPTY_STRING
                )
        except Exception as ex :            
            res = RESULT(
                Constants.RESULT_NOK,
                ex,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_CheckValue():
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        try:
            recorded, operator, expected = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            
            expected = LocatorHelper.get_value_from_variable(expected, listVariable)
            recorded = LocatorHelper.get_value_from_variable(recorded, listVariable)
            boolValue = CheckHelper.check_value_helper(driver, recorded, expected, listVariable, operator)
            
            #write log file
            itemRecorded = str(tAction.params[0].name + Constants.COLON + str(recorded))
            listParamsShow1.append(itemRecorded)
            itemOperator = str(tAction.params[1].name + Constants.COLON + tAction.params[1].value)
            listParamsShow1.append(itemOperator)
            itemExpected = str(tAction.params[2].name + Constants.COLON + str(expected))
            listParamsShow1.append(itemExpected)
            if boolValue == Constants.TRUE:
                res = RESULT(
                    Constants.RESULT_OK,
                    boolValue,
                    Constants.EMPTY_STRING,
                    str(expected),
                    str(recorded),
                    Constants.EMPTY_STRING
                )
            else:
                res = RESULT(
                    Constants.RESULT_NOK,
                    boolValue,
                    Constants.EMPTY_STRING,
                    str(expected),
                    str(recorded),
                    Constants.EMPTY_STRING
                )
        except Exception as ex:
            print (ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_CheckValueFormat(object):
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        
        try:
            formatStr, valueStr = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            booleanStr = Constants.BLANK
            count = 0
            #convert fullsize katakana to halfsize katakana
            halfsize_katakana_aci = jaconv.z2h(Constants.ALPHABETICAL_KATAKANA)
            if driver is None:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                formatStr = LocatorHelper.get_value_from_variable(formatStr, listVariable)
                strValue = LocatorHelper.get_value_from_variable(valueStr, listVariable)
                # check format ###,###,###.##
                if Constants.SHARP in formatStr and Constants.COMMAS in formatStr:
                    sharpSplit = formatStr.split(Constants.COMMAS)
                    listFormat = []
                    for i in sharpSplit:
                        if Constants.DOT in i:
                            dotSplit = i.split(Constants.DOT)
                            listFormat.extend(dotSplit)
                        else:
                            listFormat.append(i)
                    strValueSplit = strValue.split(Constants.COMMAS)
                    listStrValue = []
                    for k in strValueSplit:
                        if Constants.DOT in k:
                            dotSplit = k.split(Constants.DOT)
                            listStrValue.extend(dotSplit)
                        else:
                            listStrValue.append(k)
                    #get position of number
                    for index,value in enumerate(listFormat):
                        for ind,val in enumerate(listStrValue):
                            if len(listFormat) != len(listStrValue):
                                booleanStr = Constants.FALSE
                            else:
                                try:
                                    int(val)
                                except Exception as e:
                                    booleanStr = Constants.FALSE
                                    break
                                if index == ind and len(value) == len(val):
                                    if booleanStr == Constants.FALSE:
                                        booleanStr = Constants.FALSE
                                    else:
                                        booleanStr = Constants.TRUE
                                        
                                elif index == ind and len(value) != len(val):
                                    if val == listStrValue[-1:][0]:
                                        for k in (listStrValue[-1:]):
                                            count += int(k)
                                        if count == 0:
                                            if booleanStr == Constants.FALSE:
                                                booleanStr = Constants.FALSE
                                            else:
                                                booleanStr = Constants.TRUE
                                        else:
                                            booleanStr = Constants.FALSE
                                    else:
                                        booleanStr = Constants.FALSE
                #check format date yyyy-dd-mmm(mmm: Dec,Jan,May,...)
                elif "yyyy" in formatStr:
                    patternD = (((formatStr.replace("yyyy", '%Y')).replace("dd", "%d")).replace("mmm", "%b")).replace("mm", "%m")
                    try:
                        date_obj = datetime.datetime.strptime(strValue, patternD)
                        booleanStr = Constants.TRUE
                    except ValueError as e:
                        booleanStr = Constants.FALSE
                
                #check format hiragana
                elif Constants.HIRAGANA == formatStr:
                    for i in strValue:
                        if i in Constants.ALPHABETICAL_HIRAGANA:
                            booleanStr = Constants.TRUE
                        else:
                            booleanStr = Constants.FALSE
                            break
                #check format katakana
                elif Constants.KATAKANA == formatStr:
                    for i in strValue:
                        if i in Constants.ALPHABETICAL_KATAKANA:
                            booleanStr = Constants.TRUE
                        else:
                            booleanStr = Constants.FALSE
                            break
                #check format halfsizekatakana
                elif Constants.HALFSIZEKATAKANA == formatStr:
                    for i in strValue:
                        if i in halfsize_katakana_aci:
                            booleanStr = Constants.TRUE
                        else:
                            booleanStr = Constants.FALSE
                            break
                else:
                    booleanStr = Constants.FALSE
                
                #write log file
                itemFormat = str(tAction.params[0].name + Constants.COLON + formatStr)
                listParamsShow1.append(itemFormat)
                itemValue = str(tAction.params[1].name + Constants.COLON + strValue)
                listParamsShow1.append(itemValue)
                if booleanStr == Constants.TRUE:
                    res = RESULT(
                        Constants.RESULT_OK,
                        booleanStr,
                        Constants.EMPTY_STRING,
                        formatStr,
                        strValue,
                        Constants.EMPTY_STRING
                    )
                else:
                    res = RESULT(
                        Constants.RESULT_NOK,
                        booleanStr,
                        Constants.EMPTY_STRING,
                        formatStr,
                        strValue,
                        Constants.EMPTY_STRING
                    )
        except Exception as ex:
            print("Error action check value format")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_Click():
    def execute(self, projectPath, driver, executeItem, listVariable, resourceFolder,  listParamsShow1, browserName):
        global element
        element = None
        try:
            tAction = executeItem.testAction
            layout, control, index = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            if index == None:
                index = 0
            if driver is None:
                    res = RESULT(
                        Constants.RESULT_OK,
                        Constants.MSG_SELECT_BROWSER,
                        Constants.EMPTY_OBJECT,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
            else:
                elements = LocatorHelper.get_element_by_locator2(driver, projectPath, layout, executeItem, control, resourceFolder, listVariable, index)
                
                if elements != None:
                    if type(elements) is list:
                        for element in elements:
                            #thực hiện click ở vị trí được chỉ định
                            # element.click()
                            if browserName == "safari":
                                # element.send_keys(Keys.RETURN)
                                driver.execute_script("arguments[0].click();", element)
                            else:
                                element.click()
                            time.sleep(3)
                    else:
                        if browserName == "safari":
                            # element.send_keys(Keys.RETURN)
                            driver.execute_script("arguments[0].click();", elements)
                        else:
                            elements.click()
                        time.sleep(3)
                    #write log file
                    itemLayout = str("Screen" + Constants.COLON + layout)
                    listParamsShow1.append(itemLayout)
                    itemControl = str("Item" + Constants.COLON + control)
                    listParamsShow1.append(itemControl)
                    itemIndex = str("Index" + Constants.COLON + str(index))
                    listParamsShow1.append(itemIndex) 
                    
                    res = RESULT(
                        Constants.RESULT_OK,
                        Constants.EMPTY_STRING,
                        element,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
                else:
                    res = RESULT(
                        Constants.RESULT_NOK,
                        Constants.MSG_CONTROL_NOT_EXIST,
                        Constants.EMPTY_OBJECT,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
        except Exception as ex:
            print("Error action click")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_CloseBrowser:
    def execute(self, projectPath, driver, lsParams,listVariable, resourceFolder, listParamsShow1, browserName):
        try:
            if driver is None:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                # driver.quit()
                driver.close()
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
        except Exception as ex:
            print("Error action close browser")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_CloseTabPage:
    def execute(self, projectPath, driver, executeItem):
        try:
            tAction = executeItem.testAction
            if driver is None:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                index = tAction.params[0].value
                tab = driver.window_handles
                index = int(index)
                if math.isnan(index):
                    res = RESULT(
                        Constants.RESULT_NOK,
                        Constants.MSG_INDEX_IS_NOT_A_NUMBER,
                        Constants.EMPTY_OBJECT,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
                else:
                    if len(tab) > index:
                        if index == 0:
                            driver.switch_to.window(tab[0])
                            driver.close()
                            res = RESULT(
                                Constants.RESULT_OK,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_OBJECT,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING
                            )
                        else:
                            newIndex = len(tab) - index
                            a = driver.current_window_handle
                            driver.switch_to.window(tab[newIndex])
                            driver.close()
                            driver.switch_to.window(a)
                            res = RESULT(
                                Constants.RESULT_OK,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_OBJECT,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING
                            )
                    else:
                        res = RESULT(
                            Constants.RESULT_NOK,
                            Constants.MSG_TAB_LENGTH_IS_SMALLER,
                            Constants.EMPTY_OBJECT,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING
                        )
        except Exception as ex:
            print (ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                sys.exc_info()[0],
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_CompareImage(object):
    
    def execute(self, projectPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        try:
            tAction = executeItem.testAction
            # recorded_image = tAction.params[0].value
            # expected_image = tAction.params[1].value
            recorded_image, expected_image = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            pathRecordImage = LocatorHelper.get_value_from_variable(recorded_image, listVariable)
            pathExpectedImage = LocatorHelper.get_value_from_variable(expected_image, listVariable)
            
            image1 = cv2.imread(pathRecordImage)
            image2 = cv2.imread(pathExpectedImage)
            strNotifi = Constants.EMPTY_STRING
            if tAction.id in listVariable.keys():
                imageIdex = listVariable[tAction.id] + 1
            else:
                imageIdex = 1
            imageName = str(imageIdex)+"_" + tAction.id+ Constants.EXT_PNG
            imPath = os.path.join(listVariable["testResult"], imageName)
            listVariable[tAction.id] = imageIdex
            if image1.shape != image2.shape:
                strNotifi = "Images have the difference dimensions."
            else:
                i1 = Image.open(pathRecordImage)
                i2 = Image.open(pathExpectedImage)
                assert i1.mode == i2.mode, "Difference kinds of images."
                assert i1.size == i2.size, "Difference sizes."
                pairs = zip(i1.getdata(), i2.getdata())
                if len(i1.getbands()) == 1:
                    dif = sum(abs(p1-p2) for p1,p2 in pairs)
                else:
                    dif = sum(abs(c1-c2) for p1,p2 in pairs for c1,c2 in zip(p1,p2))
                ncomponents = i1.size[0]*i1.size[1]*3
                strNotifi = str((dif/255.0*100)/ncomponents)
                imageA = image1
                imageB = image2
                grayA = cv2.cvtColor(imageA, cv2.COLOR_BGR2GRAY)
                grayB = cv2.cvtColor(imageB, cv2.COLOR_BGR2GRAY)
                (score, diff) = structural_similarity(grayA, grayB, full=True)
                diff = (diff * 255).astype("uint8")
                thresh = cv2.threshold(diff, 0, 255,
                    cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
                cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
                    cv2.CHAIN_APPROX_SIMPLE)
                cnts = imutils.grab_contours(cnts)
                for c in cnts:
                    (x, y, w, h) = cv2.boundingRect(c)
                    cv2.rectangle(imageA, (x, y), (x + w, y + h), (0, 0, 255), 2)
                    cv2.rectangle(imageB, (x, y), (x + w, y + h), (0, 0, 255), 2)
                heightA, widthA = imageA.shape[:2]
                imsA = cv2.resize(imageA,(widthA//2, heightA//2), interpolation=cv2.INTER_AREA)
                
                heightB, widthB = imageB.shape[:2]
                imsB = cv2.resize(imageB,(widthB//2,heightB//2), interpolation=cv2.INTER_AREA)
                imageCombiding = np.concatenate((imsA,imsB), axis = 1)
                cv2.imwrite(imPath, imageCombiding)
            
            #Write log file
            itemRecorded = str(tAction.params[0].name + Constants.COLON + pathRecordImage)
            listParamsShow1.append(itemRecorded)
            itemExpected = str(tAction.params[1].name + Constants.COLON + pathExpectedImage)
            listParamsShow1.append(itemExpected)
            res = RESULT(
                Constants.RESULT_OK,
                strNotifi,
                imageName,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        except Exception as ex:
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res
    


class AC_ConnectDatabase:
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        
        try:
            connectionName, typeStr, serverStr, dbName, userName, passWord = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            
            typeStr = LocatorHelper.get_value_from_variable(typeStr, listVariable)
            serverStr = LocatorHelper.get_value_from_variable(serverStr, listVariable)
            dbName = LocatorHelper.get_value_from_variable(dbName, listVariable)
            userName = LocatorHelper.get_value_from_variable(userName, listVariable)
            passWord = LocatorHelper.get_value_from_variable(passWord, listVariable)
            
            connectionStr = {'Type': typeStr, 'Server': serverStr, 'DBName': dbName, 'Username': userName, 'Password': passWord}
            
            connectionName = LocatorHelper.set_value_from_variable(connectionName, connectionStr, listVariable)
            
            #Write log file
            itemConnectionName = str(tAction.params[0].name + Constants.COLON + tAction.params[0].value)
            listParamsShow1.append(itemConnectionName)
            itemType = str(tAction.params[1].name + Constants.COLON + typeStr)
            listParamsShow1.append(itemType)
            itemServer = str(tAction.params[2].name + Constants.COLON + serverStr)
            listParamsShow1.append(itemServer)
            itemDBName = str(tAction.params[3].name + Constants.COLON + dbName)
            listParamsShow1.append(itemDBName)
            itemUsername = str(tAction.params[4].name + Constants.COLON + userName)
            listParamsShow1.append(itemUsername)
            itemPassword = str(tAction.params[5].name + Constants.COLON + passWord)
            listParamsShow1.append(itemPassword)
            res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
            
        except Exception as e:
            print("Error action connect database")
            print (e)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(e),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_ConnectFTP():
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        try:
            connectionName, hostFtp, portFtp, usernameFtp, passwordFtp = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            
            if driver is None:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                hostFtp = LocatorHelper.get_value_from_variable(hostFtp, listVariable)
                portFtp = LocatorHelper.get_value_from_variable(portFtp, listVariable)
                usernameFtp = LocatorHelper.get_value_from_variable(usernameFtp, listVariable)
                passwordFtp = LocatorHelper.get_value_from_variable(passwordFtp, listVariable)
                connectionStr = {'Host': hostFtp, 'Port': portFtp, 'Username': usernameFtp, 'Password': passwordFtp}
                listVariable[connectionName] = connectionStr
                #Write log file
                itemConnectionName = str(tAction.params[0].name + Constants.COLON + tAction.params[0].value)
                listParamsShow1.append(itemConnectionName)
                itemHort = str(tAction.params[1].name + Constants.COLON + hostFtp)
                listParamsShow1.append(itemHort)
                itemPort = str(tAction.params[2].name + Constants.COLON + str(portFtp))
                listParamsShow1.append(itemPort)
                itemUsername = str(tAction.params[3].name + Constants.COLON + usernameFtp)
                listParamsShow1.append(itemUsername)
                itemPassword = str(tAction.params[4].name + Constants.COLON + passwordFtp)
                listParamsShow1.append(itemPassword)
                res = RESULT(
                    Constants.RESULT_OK,
                    "Connection successful",
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
        except Exception as ex:
            print("Error action connect ftp")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_CopyFile:
    def execute(self, projectPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        
        try:
            filePath, newdirectory = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            
            filePath = LocatorHelper.get_value_from_variable(filePath , listVariable)
            newdirectory = LocatorHelper.get_value_from_variable(newdirectory , listVariable)
            if os.path.exists(filePath):
                shutil.copyfile(filePath, newdirectory)
                res = RESULT(
                    Constants.RESULT_OK,
                    "Copy file success",
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            #write log file
            itemFilePath = str(tAction.params[0].name + Constants.COLON + filePath)
            listParamsShow1.append(itemFilePath)
            itemNewPath = str(tAction.params[1].name + Constants.COLON + newdirectory)
            listParamsShow1.append(itemNewPath)
        except Exception as ex :
            print("Error action copy file")   
            print (ex)         
            res = RESULT(   
                Constants.RESULT_NOK,
                ex,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_CreateFolder:
    def execute(self, projectPath, driver, executeItem):
        tAction = executeItem.testAction
        try:
            folderPath = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            
            if os.path.exists(folderPath):
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_FOLDER_IS_EXIST,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                os.makedirs(folderPath)
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.MSG_CREATER_FOLDER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
        except Exception as ex :            
            res = RESULT(
                Constants.RESULT_NOK,
                ex,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_CreateFolderFTP():
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        
        try:
            connectionName, dirName = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            
            if driver is None:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                connectionValue = listVariable[connectionName]
                
                hostFtp = connectionValue["Host"]
                portFtp = connectionValue["Port"]
                usernameFtp = connectionValue["Username"]
                passwordFtp = connectionValue["Password"]
                ftp = ftplib.FTP()
                ftp.connect(hostFtp, int(portFtp))
                
                ftp.login(usernameFtp, passwordFtp)
                data = []
                ftp.dir(data.append)
                dirName = LocatorHelper.get_value_from_variable(dirName, listVariable)
                
                ftp.mkd(dirName)
                ftp.quit()
                #Write log file
                itemConnectionName = str(tAction.params[0].name + Constants.COLON + tAction.params[0].value)
                listParamsShow1.append(itemConnectionName)
                itemDirname = str(tAction.params[1].name + Constants.COLON + dirName)
                listParamsShow1.append(itemDirname)
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
        except Exception as ex:
            print("Error action create folder ftp")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_DeleteFile:
    def execute(self, projectPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        try:
            filePath = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            
            filePath = LocatorHelper.get_value_from_variable(filePath , listVariable)
            
            if os.path.isdir(filePath):
                listPath = os.listdir(filePath)
                if len(listPath) > 0:
                    for i in listPath:
                        k = filePath + "/" + i
                        os.remove(k)
                    os.rmdir(filePath)
                else:
                    os.rmdir(filePath)
            else:
                os.remove(filePath)
            #write log file
            itemFilePath = str(tAction.params[0].name + Constants.COLON + filePath)
            listParamsShow1.append(itemFilePath)
        
            res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,    
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING                              
            )
        except Exception as ex :  
            print("Error action delete file")
            print(ex)        
            res = RESULT(
                Constants.RESULT_NOK,
                ex,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_DeleteFolder:
    def execute(self,projectPath, driver, executeItem):
        tAction = executeItem.testAction
        try:
            folderPath = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            
            if shutil.rmtree(folderPath):
                res = RESULT(
                Constants.RESULT_OK,
                Constants.MSG_DELETE_FOLDER,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
            else:
                es = RESULT(
                Constants.RESULT_NOK,
                Constants.MSG_DELETE_FOLDER,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        except Exception as ex :            
            res = RESULT(
                Constants.RESULT_NOK,
                ex,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_DeleteInFTP():
    def getDirListing(self, dirName, ftp):
        listing = ftp.nlst(dirName)
        # If listed a file, return.
        if len(listing) == 1 and listing[0] == dirName:
            return []
        subListing = []
        for entry in listing:
            subListing += self.getDirListing(entry, ftp)
        listing += subListing
        return listing
        
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        
        try:
            connectionName, dirName = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            
            if driver is None:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                connectionValue = listVariable[connectionName]
                hostFtp = connectionValue["Host"]
                portFtp = connectionValue["Port"]
                usernameFtp = connectionValue["Username"]
                passwordFtp = connectionValue["Password"]
                ftp = ftplib.FTP()
                ftp.connect(hostFtp, int(portFtp))
                
                ftp.login(usernameFtp, passwordFtp)
                data = []
                ftp.dir(data.append)
                dirName = LocatorHelper.get_value_from_variable(dirName, listVariable)
                
                check = Constants.BLANK
                message = Constants.BLANK
                try:
                    listing = self.getDirListing(dirName, ftp)
                        # Longest path first for deletion of sub directories first.
                    listing.sort(key=lambda k: len(k), reverse=True)
                    # Delete files first.
                    for entry in listing:
                        try:
                            ftp.delete(entry)
                        except Exception as e:
                            print(e)
                            pass
                    # Delete empty directories.
                    for entry in listing:
                        try:
                            ftp.rmd(entry)
                        except Exception as e:
                            print(e)
                            pass
                    ftp.rmd(dirName)
                    check = Constants.TRUE
                    message = "Delete file or folder in ftp Success"
                except Exception as e:
                    print(e)
                    check = Constants.FALSE
                    message = "Failed"
                ftp.quit()
                #Write log file
                itemConnectionName = str(tAction.params[0].name + Constants.COLON + tAction.params[0].value)
                listParamsShow1.append(itemConnectionName)
                itemDirname = str(tAction.params[1].name + Constants.COLON + dirName)
                listParamsShow1.append(itemDirname)
                if check == Constants.TRUE:
                    res = RESULT(
                        Constants.RESULT_OK,
                        message,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
                else:
                    res = RESULT(
                        Constants.RESULT_NOK,
                        message,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
        except Exception as ex:
            print("Error action delete in ftp")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_DismissAlert():
    """description of class"""
    def execute(self, xmlPath, driver, executeItem):
        try:
            tAction = executeItem.testAction
            valueAlert = self.isCheckAlert(driver)
            if valueAlert != False:
                driver.switch_to.alert.dismiss()
                res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
            else:
                res = RESULT(
                    Constants.RESULT_NOK,
                    'No Alert',
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
        except Exception as ex:
            print (ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                ex,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res
    def isCheckAlert(self, driver):
        try:
            alert = driver.switch_to.alert
            alert.text
            return alert
        except NoAlertPresentException:
            return False 


class AC_DoesControlExist():
    #hàm ni dư
    def load_layout_file(self,loPath):
        DOMTree = xml.dom.minidom.parse(loPath)
        loEntity = DOMTree.getElementsByTagName(Constants.TAG_LAYOUT_MODEL)[0]
        return loEntity
    def execute(self, driver,listCondition):
        global element
        element = None
        try:
            if driver is None:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else: 
                element = LocatorHelper.get_element_by_locator2(driver, projectPath, layout, executeItem, control, resourceFolder, listVariable, index)
                
                if element == None:
                    res = RESULT(
                            Constants.RESULT_NOK,
                            Constants.MSG_CONTROL_NOT_EXIST,
                            Constants.EMPTY_OBJECT,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING
                        )
                else:
                    res = RESULT(
                            Constants.RESULT_OK,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING
                        )
        except Exception as ex:
            print (ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                ex,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_DoesTextContain:
    def execute(self, projectPath, driver ,executeItem):
        tAction = executeItem.testAction
        tsName = executeItem.testSuiteName
        txt = tAction.params[0].value
        variable = tAction.params[2].value
        tsFilePath = os.path.join(projectPath,Constants.TAG_TESTSUITES,tsName + Constants.EXT_XML)
        acSetVariable = AC_SetVariable()
        fragment = tAction.params[1].value
        try:
            try:
                a = txt.index(fragment)
                acSetVariable.execute(tsFilePath, variable, Constants.TRUE)
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.MSG_TEXT_IS_FOUND,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            except:
                acSetVariable.execute(tsFilePath, variable, Constants.FALSE)
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_TEXT_IS_NOT_FOUND,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
        except Exception as ex:
            res = RESULT(
                Constants.RESULT_NOK,
                ex,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_DowloadFTPFile():
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        
        try:
            connectionName, remotePath, localPathStr, nameFile = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            
            if driver is None:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                connectionValue = listVariable[connectionName]
                hostFtp = connectionValue["Host"]
                portFtp = connectionValue["Port"]
                usernameFtp = connectionValue["Username"]
                passwordFtp = connectionValue["Password"]
                ftp = ftplib.FTP()
                ftp.connect(hostFtp, int(portFtp))
                
                ftp.login(usernameFtp, passwordFtp)
                data = []
                ftp.dir(data.append)
                remotePath = LocatorHelper.get_value_from_variable(remotePath, listVariable)
                localPath = LocatorHelper.get_value_from_variable(localPathStr, listVariable)
                nameFile = LocatorHelper.get_value_from_variable(nameFile, listVariable)
                
                pathProject = os.getcwd()
                message = Constants.BLANK
                # Change the path to the path you want
                os.chdir(localPath)
                # file location
                
                ftp.cwd(remotePath)
                try:
                    ftp.retrbinary("RETR " + nameFile ,open(nameFile, 'wb').write)
                    message = "Dowloaded"
                except Exception as e:
                    print(e)
                    message = "Dowloaded Falied"
                
                #return path project system
                os.chdir(pathProject)
                ftp.quit()
                #Write log file
                itemConnectionName = str(tAction.params[0].name + Constants.COLON + tAction.params[0].value)
                listParamsShow1.append(itemConnectionName)
                itemRemotePath = str(tAction.params[1].name + Constants.COLON + remotePath)
                listParamsShow1.append(itemRemotePath)
                itemLocalPath = str(tAction.params[2].name + Constants.COLON + localPath)
                listParamsShow1.append(itemLocalPath)
                itemNameFile = str(tAction.params[3].name + Constants.COLON + nameFile)
                listParamsShow1.append(itemNameFile)
                res = RESULT(
                    Constants.RESULT_OK,
                    message,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
        except Exception as ex:
            print("Error action download ftp file")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_DowloadFTPFiles():
    def walk_dir(self, f, remotePath, localPath, projectPath):
        try:
            f.cwd(remotePath)
        except Exception as e:
            localPathNew = Constants.EMPTY_STRING
            localSplit = localPath.split(Constants.SLASH)
            localSplit.pop(-1)
            for i in localSplit:
                localPathNew += i + Constants.SLASH
            os.chdir(localPathNew)
            remotePathNew = Constants.EMPTY_STRING
            remotePathSplit = remotePath.split(Constants.SLASH)
            remotePathSplit.pop(-1)
            for i in remotePathSplit:
                remotePathNew += i + Constants.SLASH
            f.cwd(remotePathNew)
            if Constants.DOT in remotePath.split(Constants.SLASH)[-1:][0]:
                try:
                    f.retrbinary("RETR " + remotePath.split(Constants.SLASH)[-1:][0], open(remotePath.split(Constants.SLASH)[-1:][0], "wb").write)
                except Exception as directory_error:
                    print(directory_error)
                    print("Oops! Was " + remotePath.split(Constants.SLASH)[-1:][0] + " a directory? I won't download directories.")
            os.chdir(projectPath)
            return
        #create folder
        if not os.path.exists(localPath):
            os.mkdir(localPath)
        names = f.nlst()
        for name in names:
            self.walk_dir(f, remotePath + Constants.SLASH + name, localPath + Constants.SLASH + name, projectPath)
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        
        try:
            connectionName, remotePath, localPathStr = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            
            if driver is None:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                connectionValue = listVariable[connectionName]
                hostFtp = connectionValue["Host"]
                portFtp = connectionValue["Port"]
                usernameFtp = connectionValue["Username"]
                passwordFtp = connectionValue["Password"]
                ftp = ftplib.FTP()
                ftp.connect(hostFtp, int(portFtp))
                
                ftp.login(usernameFtp, passwordFtp)
                data = []
                ftp.dir(data.append)
                remotePath = LocatorHelper.get_value_from_variable(remotePath, listVariable)
                localPath = LocatorHelper.get_value_from_variable(localPathStr, listVariable)
                # get path current project system
                pathProject = os.getcwd()
                
                message = Constants.BLANK
                
                # #return path project system
                self.walk_dir(ftp, remotePath, localPath, pathProject)
                os.chdir(pathProject)
                ftp.quit()
                #Write log file
                itemConnectionName = str(tAction.params[0].name + Constants.COLON + tAction.params[0].value)
                listParamsShow1.append(itemConnectionName)
                itemRemotePath = str(tAction.params[1].name + Constants.COLON + remotePath)
                listParamsShow1.append(itemRemotePath)
                itemLocalPath = str(tAction.params[1].name + Constants.COLON + localPath)
                listParamsShow1.append(itemLocalPath)
                res = RESULT(
                    Constants.RESULT_OK,
                    message,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
        except Exception as ex:
            print("Error action dowload ftp files")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_EmailFromContact:
    def execute(self, xmlPath, driver, executeItem, lstVariables, resourceFolder, listParamsShow1, browserName):
        try:
            tAction = executeItem.testAction
            # emailConnect = tAction.params[0].value
            # smtpServer = tAction.params[1].value
            # port = tAction.params[2].value
            # email = tAction.params[3].value
            # password = tAction.params[4].value
            emailConnect, smtpServer, port, email, password = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            if driver is None:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                email = LocatorHelper.get_value_from_variable(email, lstVariables)
                password = LocatorHelper.get_value_from_variable(password, lstVariables)
                emailConnectStr = {'SmtpServer': smtpServer, 'Port': port, 'Email': email, 'Password': password}
                lstVariables[emailConnect] = emailConnectStr
                #Write log file
                itemEmailConnect = str(tAction.params[0].name + Constants.COLON + tAction.params[0].value)
                listParamsShow1.append(itemEmailConnect)
                itemSmtpServer = str(tAction.params[1].name + Constants.COLON + tAction.params[0].value)
                listParamsShow1.append(itemSmtpServer)
                itemPort = str(tAction.params[2].name + Constants.COLON + tAction.params[0].value)
                listParamsShow1.append(itemPort)
                itemEmail = str(tAction.params[3].name + Constants.COLON + email)
                listParamsShow1.append(itemEmail)
                itemPassword = str(tAction.params[4].name + Constants.COLON + password)
                listParamsShow1.append(itemPassword)
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
        except Exception as ex:
            print("Error action email from contact")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_EndIf():
    """description of class"""
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder,  listParamsShow1, browserName):
        try:
            listParamsShow1[:] = []
            res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        except Exception as ex:
            print("Error action end if")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_OBJECT,
                Constants.MSG_CONTROL_IS_EXIST,
                Constants.MSG_CONTROL_NOT_EXIST,
                Constants.EMPTY_STRING
            )
        return res


class AC_EndLoop:
    
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, indexExecute, listScript, indexLoop):
        
        try:
            countLoop = 1
            index = indexExecute
            while (countLoop > 0) and (index >= 0) and (index < len(listScript)):
                index = index -1
                if (listScript[index] == Constants.AC_ENDLOOP):
                    countLoop = countLoop + 1
                if (listScript[index] == Constants.AC_LOOP):
                    countLoop = countLoop - 1
                
            res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                index
            )
            
        except Exception as ex:
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_OBJECT,
                Constants.MSG_CONTROL_IS_EXIST,
                Constants.MSG_CONTROL_NOT_EXIST,
                Constants.EMPTY_STRING
            )
        return res


class AC_EndWhile:
    """description of class"""
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, indexExecute, listScript, indexLoop):
        
        try:
            countWhile = 1
            index = indexExecute
            while (countWhile > 0) and (index >= 0) and (index < len(listScript)):
                index = index -1
                if (listScript[index] == Constants.AC_WHILE):
                    countWhile = countWhile -1
                
            res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                index
            )
        except Exception as ex:
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_OBJECT,
                Constants.MSG_CONTROL_IS_EXIST,
                Constants.MSG_CONTROL_NOT_EXIST,
                Constants.EMPTY_STRING
            )
        return res


class AC_Enter(object):
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder,  listParamsShow1, browserName):
        tAction = executeItem.testAction
        
        
        #lí do append valueInput và value là vì sau này sẽ cần gọi ra để ghi vào chuỗi stringText của ExecuteHandler.py
        global element
        element = None
        try:
            screen, control, index, valueInput = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
        
            if index == None:
                index = 0
            value = Constants.EMPTY_STRING
            ### #item
            ### #item[0]
            value = LocatorHelper.get_value_from_variable(valueInput, listVariable)
            
            if driver is None:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                elements = LocatorHelper.get_element_by_locator2(driver, xmlPath, screen, executeItem, control, resourceFolder, listVariable, index)
                
                if elements != None:
                    if type(elements) is list:
                        for element in elements:
                            element.clear()
                            element.send_keys(value)
                    else:
                        elements.clear()
                        elements.send_keys(value)
                    value = LocatorHelper.set_value_from_variable(valueInput, value, listVariable)
                    #write log file
                    itemScreen = str("Screen" + Constants.COLON + screen)
                    listParamsShow1.append(itemScreen)
                    itemControl = str("Item" + Constants.COLON + control)
                    listParamsShow1.append(itemControl)
                    itemIndex = str("Index" + Constants.COLON + str(index))
                    listParamsShow1.append(itemIndex)
                    itemValue = str("Value" + Constants.COLON + str(valueInput))
                    listParamsShow1.append(itemValue)
                    res = RESULT(
                        Constants.RESULT_OK,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
                else:
                    res = RESULT(
                        Constants.RESULT_NOK,
                        Constants.MSG_CONTROL_NOT_EXIST,
                        Constants.EMPTY_OBJECT,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
        except Exception as ex:
            print("Error Action Enter")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_ExecuteJavascript:
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        try:
            script = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            
            script = LocatorHelper.get_value_from_variable(script, listVariable)
            
            #execute script
            driver.execute_script(script)
            #write log file
            itemScript = str(tAction.params[0].name + Constants.COLON + script)
            listParamsShow1.append(itemScript)
            res = RESULT(
                Constants.RESULT_OK,
                script,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        except Exception as ex:
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_ExecuteQuery:
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        
        try:
            connectionName, typeFile, queryString, result = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            connectionValue = listVariable[connectionName]
            
            typeStr = connectionValue["Type"]
            serverStr = connectionValue["Server"]
            dbName = connectionValue["DBName"]
            userName = connectionValue["Username"]
            passWord = connectionValue["Password"]
            #connect database
            if typeStr == Constants.MYSQL:
                import pymysql 
                db = pymysql.connect(serverStr,userName,passWord,dbName)
            elif typeStr == Constants.POSTGRESQL:
                import psycopg2
                db = psycopg2.connect("dbname='"+ dbName+"' user='"+userName+"' host='"+serverStr+"' password='"+passWord+"'")
            # elif typeStr == "mongodb":
            #     from pymongo import MongoClient
            #     db = MongoClient(serverStr, 27017)
            elif typeStr == Constants.SQLSERVER:
                import pyodbc
                db = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+serverStr+';DATABASE='+dbName+';UID='+userName+';PWD='+passWord+'')
            elif typeStr == Constants.ORACLE:
                import cx_Oracle
                arr_server = str(serverStr).split(":")
                dsn = cx_Oracle.makedsn(arr_server[0], arr_server[1], dbName)
                db = cx_Oracle.Connection(userName, passWord, dsn)
            conn = db.cursor()
            queryFinal = LocatorHelper.get_value_from_variable(queryString, listVariable)
            if typeFile == "text":
                if typeStr == Constants.MYSQL or typeStr == Constants.POSTGRESQL or typeStr == Constants.ORACLE:
                    if queryString.lower().split(Constants.SPACE)[0] == Constants.SELECT:
                        conn.execute(queryFinal)
                        data = conn.fetchall()
                        dataMain = [list(x) for x in data]
                        if data != None:
                            completed = Constants.MSG_QUERY_DATABASE_SUCCESS
                        else:
                            completed = Constants.MSG_QUERY_DATABASE_FAILED
                    elif queryString.lower().split(Constants.SPACE)[0] == Constants.UPDATE:
                        conn.execute(queryFinal)
                        updated_rows = conn.rowcount
                        db.commit()
                        if updated_rows != 0:
                            completed = Constants.MSG_QUERY_DATABASE_SUCCESS
                            queryStringLower = queryFinal.lower()
                            condition = queryStringLower.split(Constants.WHERE)[1]
                            dbName = queryFinal.split(Constants.SPACE)[1]
                            sqlQuery = Constants.SELECT_ASTERISK_FROM + dbName + Constants.SPACE_WHERE_SPACE + condition
                            conn.execute(sqlQuery)
                            resultConn = conn.fetchall()
                            dataMain = [list(x) for x in resultConn]
                        else:
                            completed = Constants.MSG_QUERY_DATABASE_FAILED
                            dataMain = False
                    elif queryString.lower().split(Constants.SPACE)[0] == Constants.DELETE:
                        conn.execute(queryFinal)
                        rows_deleted = conn.rowcount
                        db.commit()
                        if rows_deleted != 0:
                            completed = Constants.MSG_QUERY_DATABASE_SUCCESS
                            dataMain = True
                        else:
                            completed = Constants.MSG_QUERY_DATABASE_FAILED
                            dataMain = False
                    elif queryString.lower().split(Constants.SPACE)[0] == Constants.INSERT:
                        conn.execute(queryFinal)
                        rows_insert = conn.rowcount
                        db.commit()
                        if rows_insert != 0:
                            completed = Constants.MSG_QUERY_DATABASE_SUCCESS
                            tbName = queryString.split(Constants.SPACE)[2].split(Constants.OPEN_PARENTHESIS)[0]
                            idFindBy = Constants.QUERY_GET_COLUMN_NAME_BY_MYSQL_AND_POSTGRESQL + tbName + "' limit 1"
                            conn.execute(idFindBy)
                            idFindEx = conn.fetchall()
                            idStr = str(idFindEx).split(Constants.SINGLE_QUOTATION)[1]
                            sqlQueryInsert = Constants.SELECT_ASTERISK_FROM + tbName +Constants.SPACE_ORDER_BY_SPACE + idStr + " desc limit 1"
                            conn.execute(sqlQueryInsert)
                            data = conn.fetchall()
                            dataMain = [list(x) for x in data]
                        else:
                            completed = Constants.MSG_QUERY_DATABASE_FAILED
                            dataMain = False
                    else:
                        conn.execute(queryFinal)
                        db.commit()
                        completed = Constants.BLANK
                        dataMain = Constants.MSG_QUERY_DATABASE_SUCCESS
                elif typeStr == Constants.SQLSERVER:
                    if queryString.lower().split(Constants.SPACE)[0] == Constants.SELECT: 
                        conn.execute(queryFinal)
                        data = conn.fetchall()
                        dataMain = [list(x) for x in data]
                        if data != None:
                            completed = Constants.MSG_QUERY_DATABASE_SUCCESS
                        else:
                            completed = Constants.MSG_QUERY_DATABASE_FAILED
                    elif queryString.lower().split(Constants.SPACE)[0] == Constants.UPDATE:
                        conn.execute(queryFinal)
                        updated_rows = conn.rowcount
                        db.commit()
                        if updated_rows != 0:
                            completed = Constants.MSG_QUERY_DATABASE_SUCCESS
                            queryStringLower = queryFinal.lower()
                            condition = queryStringLower.split(Constants.WHERE)[1]
                            dbName = queryFinal.split(Constants.SPACE)[1]
                            sqlQuery = Constants.SELECT_ASTERISK_FROM + dbName + Constants.SPACE_WHERE_SPACE + condition
                            conn.execute(sqlQuery)
                            resultConn = conn.fetchall()
                            dataMain = [list(x) for x in resultConn]
                        else:
                            completed = Constants.MSG_QUERY_DATABASE_FAILED
                            dataMain = False
                    elif queryString.lower().split(Constants.SPACE)[0] == Constants.DELETE:
                        conn.execute(queryFinal)
                        rows_deleted = conn.rowcount
                        db.commit()
                        if rows_deleted != 0:
                            completed = Constants.MSG_QUERY_DATABASE_SUCCESS
                            dataMain = True
                        else:
                            completed = Constants.MSG_QUERY_DATABASE_FAILED
                            dataMain = False
                    elif queryString.lower().split(Constants.SPACE)[0] == Constants.INSERT:
                        conn.execute(queryFinal)
                        rows_insert = conn.rowcount
                        db.commit()
                        if rows_insert != 0:
                            completed = Constants.MSG_QUERY_DATABASE_SUCCESS
                            tbName = queryString.split(Constants.SPACE)[2].split(Constants.OPEN_PARENTHESIS)[0]
                            idFindBy = Constants.QUERY_GET_COLUMN_NAME + tbName + Constants.SINGLE_QUOTATION
                            conn.execute(idFindBy)
                            idFindEx = conn.fetchall()
                            idStr = str(idFindEx).split(Constants.SINGLE_QUOTATION)[1]
                            sqlQueryInsert = Constants.SELECT_TOP_ASTERISK_FROM + tbName +Constants.SPACE_ORDER_BY_SPACE + idStr + Constants.SPACE_DESC
                            conn.execute(sqlQueryInsert)
                            data = conn.fetchall()
                            dataMain = [list(x) for x in data]
                        else:
                            completed = Constants.MSG_QUERY_DATABASE_FAILED
                            dataMain = False
                    else:
                        conn.execute(queryFinal)
                        db.commit()
                        completed = Constants.BLANK
                        dataMain = Constants.MSG_QUERY_DATABASE_SUCCESS
            else:
                if typeStr == Constants.MYSQL or typeStr == Constants.POSTGRESQL:
                    
                    lines = ""
                    for line in codecs.open(queryFinal, encoding='utf-8').read().split('\n'):   
                        if line.startswith("--") is False and line.startswith("/*") is False:
                            lines += line
                    lines = lines.replace(';', ';\n')
                    
                    for line in lines.split('\n'):
                        if line.strip():
                            conn.execute(line)
                    db.commit()
                elif typeStr == Constants.SQLSERVER:
                    lines = ""
                    for line in open(queryFinal, encoding='utf-8').read().split('\n'):   
                        # print(line)
                        if line.startswith("go") is True and line.endswith("") is True:
                            line = line.replace("go",";")
                        
                        if line.startswith("--") is False and line.startswith("/*") is False:
                            lines += line
                            
                    lines = lines.replace(';', ';\n')
                    for line in lines.split('\n'):
                        if line.strip():
                            try:
                                conn.execute(line)
                            except Exception as e:
                                print(e)
                    db.commit()
                elif typeStr == Constants.ORACLE:
                    lines = ""
                    for line in open(queryFinal, encoding='utf-8').read().split('\n'):
                        if line.startswith("--") is False and line.startswith("/*") is False:
                            lines += line
                    lines = lines.replace(';', ';\n')
                    for line in lines.split('\n'):
                        if len(line) != 0:
                            line = line.replace(";", "")
                            if line.startswith("REM"):
                                continue
                            conn.execute(line)
                    db.commit()
                dataMain = Constants.BLANK
                completed = Constants.MSG_QUERY_DATABASE_SUCCESS
            conn.close()
            db.close()
            dataMain = LocatorHelper.set_value_from_variable(result, dataMain, listVariable)
            res = RESULT(
                Constants.RESULT_OK,
                completed,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                str(dataMain),
                Constants.EMPTY_STRING
            )
            
            # elif typeStr == "mongodb":
            #     listFind = []
            #     conn = db[dbName]
            #     tbName = queryString.split(".")[1]
            #     condition = queryString.split(".")[2]
                
            #     posts = conn[tbName]
            #     if condition.split("(")[0] == "find":
            #         for post in posts.find():
            #             listFind.append(post)
            #         prItem = {result: str(listFind)}
            #     elif condition.split("(")[0] == "insert":
            #         resultStr = condition.split("(")[1].split(")")[0]
            #         resultDict = eval(resultStr) #convert str to dict 
            #         results_posts = posts.insert(resultDict)
            #         if results_posts != None:
            #             import pymongo
            #             data = posts.find().sort("_id",pymongo.DESCENDING).limit(1) 
            #             #sắp xếp giảm dần để lấy ra giá trị vừa được thêm vào 
            #             completed = "Insert successful"
            #             for i in data:
            #                 listFind.append(i)
            #             prItem = {result: str(listFind)}
            #         else:
            #             completed = "Insert failed"
            #             prItem = {result: "Failed"}
            #     elif condition.split("(")[0] == "update":
            #         resultStr = condition.split("(")[1].split(")")[0]
            #         resultDict = eval(resultStr) #convert str to dict 
            #         results_posts = posts.update(resultDict)
            #         if results_posts != None:
            #             # import pymongo
            #             # data = posts.find().sort("_id",pymongo.DESCENDING).limit(1) 
            #             #sắp xếp giảm dần để lấy ra giá trị vừa được thêm vào 
            #             completed = "Update successful"
            #             # for i in data:
            #             #     listFind.append(i)
            #             prItem = {result: "Success"}
            #         else:
            #             completed = "Update failed"
            #             prItem = {result: "Failed"}
            #     res = RESULT(
            #         Constants.RESULT_OK,
            #         Constants.EMPTY_STRING,
            #         prItem,
            #         Constants.EMPTY_STRING,
            #         Constants.EMPTY_STRING
            #     )
            #Write log file
            itemConnectionName = str(tAction.params[0].name + Constants.COLON + tAction.params[0].value)
            listParamsShow1.append(itemConnectionName)
            itemType = str(tAction.params[1].name + Constants.COLON + typeFile)
            listParamsShow1.append(itemType)
            itemQuery = str(tAction.params[2].name + Constants.COLON + queryFinal)
            listParamsShow1.append(itemQuery)
            itemResult = str(tAction.params[3].name + Constants.COLON + str(dataMain))
            listParamsShow1.append(itemResult)
        except Exception as e:
            print("Error action execute query")
            print (e)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(e),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_FindElementsBy:
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        try:
            tAction = executeItem.testAction
            returnName, fromParent, index, cssSelector = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            if index in listVariable.keys():
                index = listVariable[index]
                index = index - 1
            else:
                index = LocatorHelper.get_value_from_variable(index, listVariable)
            listCondition = {}
            
            listCondition["css"] = cssSelector
            if fromParent == Constants.BLANK:
                elements = LocatorHelper.get_element_by_locator(driver, listCondition, index)
            else:
                fromParent = LocatorHelper.get_value_from_variable(fromParent, listVariable)
                if index  == 0:
                    driverParent = fromParent[index]
                else:
                    driverParent = fromParent[int(index)-1]
                    
                # driverParent = listVariable[fromParent][int(index)]
                index = 0
                elements = LocatorHelper.get_element_by_locator(driverParent, listCondition, index)
            
            returnName  = LocatorHelper.set_value_from_variable(returnName, elements, listVariable)
            
            
            #write log file
            itemReturnName = str(tAction.params[0].name + Constants.COLON + tAction.params[0].value)
            listParamsShow1.append(itemReturnName)
            itemFromParent = str(tAction.params[1].name + Constants.COLON + str(fromParent))
            listParamsShow1.append(itemFromParent)
            itemIndex = str(tAction.params[2].name + Constants.COLON + str(index))
            listParamsShow1.append(itemIndex)
            itemCssSelector = str(tAction.params[3].name + Constants.COLON + cssSelector)
            listParamsShow1.append(itemCssSelector)
            res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        except Exception as ex:
            print("Error action find elements by")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_GetAllFileInFolder():
    def execute(self,executeFilePath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        
        try:
            tAction = executeItem.testAction
            # pathStr = tAction.params[0].value
            # result = tAction.params[1].value
            pathStr, result = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            pathStr = LocatorHelper.get_value_from_variable(pathStr, listVariable)
           
            listLibrary = os.listdir(pathStr)
            listLibChoose = []
            isCheck = Constants.BLANK
            pathSave = Constants.BLANK
            
            listPath = os.listdir(pathStr)
            for i in listPath:
                k = pathStr + i
                if os.path.isdir(k) == False:
                    listLibChoose.append(k)
            listLibChoose = LocatorHelper.set_value_from_variable(result , listLibChoose, listVariable)
            #write log file
            itemFilePath = str(tAction.params[0].name + Constants.COLON + pathStr)
            listParamsShow1.append(itemFilePath)
            itemResult = str(tAction.params[1].name + Constants.COLON + str(listLibChoose))
            listParamsShow1.append(itemResult)
            res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
            
        except Exception as ex:
            print("Error action get all file in folder")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_GetAttribute:
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        tsName = executeItem.testSuiteName
        try:
            screen, control, index, attribute, variable =  LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            value = Constants.BLANK
            global element
            element = None
            
            if driver is None:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                elements = LocatorHelper.get_element_by_locator2(driver, xmlPath, screen, executeItem, control, resourceFolder, listVariable, index)
                
                attribute = LocatorHelper.get_value_from_variable(attribute, listVariable)
                lstValue = []
                
                if type(elements) is list:
                    for element in elements:
                        value = element.get_attribute(attribute)
                        lstValue.append(value)
                else:
                    value = elements.get_attribute(attribute)
                    lstValue.append(value)
                if len(lstValue) > 1:
                    value = lstValue
                else:
                    value = lstValue[0]
                value = LocatorHelper.set_value_from_variable(variable, str(value), listVariable)
                #write log file
                itemScreen = str("Screen" + Constants.COLON + screen)
                listParamsShow1.append(itemScreen)
                itemControl = str("Control" + Constants.COLON + control)
                listParamsShow1.append(itemControl)
                itemIndex = str("Index" + Constants.COLON + str(index))
                listParamsShow1.append(itemIndex)
                itemAttribute = str("Attribute" + Constants.COLON + attribute)
                listParamsShow1.append(itemAttribute)
                itemVariable = str("Variable" + Constants.COLON + value)
                listParamsShow1.append(itemVariable)
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
                 
        except Exception as ex:
            print (ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_GetCellColor:
    #lấy ra màu bgx ở vị trí đó
    def get_cell_range(self,start_col, start_row, sheet, book,sheetPage):
        sheet = book.sheet_by_name(sheetPage)
        xfx = sheet.cell_xf_index(start_row, start_col)
        xf = book.xf_list[xfx]
        bgx = xf.background.pattern_colour_index
        return bgx
    def execute(self,executeFilePath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        try:
            tAction = executeItem.testAction
            # fileXl = tAction.params[0].value
            # sheetPage = tAction.params[1].value
            # row = tAction.params[2].value
            # column = tAction.params[3].value
            # result = tAction.params[4].value
            fileXl, sheetPage, row, column, result = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            fileXlsx = LocatorHelper.get_value_from_variable(fileXl , listVariable)
            row = LocatorHelper.get_value_from_variable(row , listVariable)
            column = LocatorHelper.get_value_from_variable(column , listVariable)
            index = os.path.basename(fileXlsx).split(Constants.DOT)[1]
            #if là file xls thì sẽ vào đây
            if index == Constants.XLS:
                #mở file excel
                book = open_workbook(fileXlsx, formatting_info=True)
                #đọc vào name sheet của excel
                sheet = book.sheet_by_name(sheetPage)
                #khi truyền vào rangeStr(có thể là E4 hoặc là A5:G9) thì sẽ trả về vị trí(int) của hàng cột của nó
                #trong file excel
                data = self.get_cell_range(int(row), int(column), sheet, book,sheetPage) 
                colorDict = book.colour_map
                bgxColor = colorDict[data]
                #format lại màu theo định dạng hex
                hexColor = Constants.SHARP+Constants.EMPTY_STRING.join(map(chr, bgxColor)).encode(Constants.HEX)
                hexColor = LocatorHelper.set_value_from_variable(result, hexColor, listVariable)
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    hexColor,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            #trường hợp này dành cho file xlsx
            else:
                book = px.load_workbook(fileXlsx, data_only=True)
                sheet = book[sheetPage]
                colorStr = sheet.cell(int(row),int(column)).fill.start_color.index
                hexColor = Constants.SHARP+colorStr[2:]
                hexColor = LocatorHelper.set_value_from_variable(result, hexColor, listVariable)
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    hexColor.lower(),
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            #write log file
            itemFileName = str(tAction.params[0].name + Constants.COLON + fileXlsx)
            listParamsShow1.append(itemFileName)
            itemSheet = str(tAction.params[1].name + Constants.COLON + tAction.params[1].value)
            listParamsShow1.append(itemSheet)
            itemRange = str(tAction.params[2].name + Constants.COLON + tAction.params[2].value)
            listParamsShow1.append(itemRange)
            itemResult = str(tAction.params[3].name + Constants.COLON + hexColor)
            listParamsShow1.append(itemResult)
        except Exception as ex:
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_GetCellValue:
    def get_cell_range(self,start_col, start_row, sheet, book,sheetPage):
        sheet = book.sheet_by_name(sheetPage)
        result = Constants.BLANK
        try:
            result = sheet.cell(start_row, start_col)
            code = Constants.RESULT_OK
        except Exception as e:
            result = e
            code = Constants.RESULT_CRASH
        return (result,code)
    def execute(self,executeFilePath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        
        try:
            tAction = executeItem.testAction
            # fileXl = tAction.params[0].value
            # sheetPage = tAction.params[1].value
            # row = tAction.params[2].value
            # column = tAction.params[3].value
            # result = tAction.params[4].value
            fileXl, sheetPage, row, column, result = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            fileXlsx = LocatorHelper.get_value_from_variable(fileXl , listVariable)
            sheetPage = LocatorHelper.get_value_from_variable(sheetPage , listVariable)
            row = LocatorHelper.get_value_from_variable(row , listVariable)
            column = LocatorHelper.get_value_from_variable(column , listVariable)
            nameFileStr = os.path.basename(fileXlsx).split(Constants.DOT)[1]
            if nameFileStr.lower() == "xls":
                book = open_workbook(fileXlsx)
                sheet = book.sheet_by_name(sheetPage)
                data,code = self.get_cell_range(int(row), int(column), sheet, book,sheetPage)
            else:
                import openpyxl
                book = openpyxl.load_workbook(fileXlsx)
                sheet = book[sheetPage]
                data = sheet.cell(row=int(row), column=int(column))
                # data = valueCell.value
                code = Constants.RESULT_OK
            strData = Constants.BLANK
            if code == Constants.RESULT_OK:
                if type(data.value) is float:
                    index = str(data.value).split(Constants.DOT)[1]
                    if int(index) == 0:
                        strData = str(data.value).split(Constants.DOT)[0]
                    else:
                        strData = str(data.value)
                else:
                    strData = data.value
                
                strData = LocatorHelper.set_value_from_variable(result, strData, listVariable)
                res = RESULT(
                    Constants.RESULT_OK,
                    str(strData),
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                strData = Constants.BLANK
                res = RESULT(
                    Constants.RESULT_CRASH,
                    str(data),
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            #write log file
            itemFileName = str(tAction.params[0].name + Constants.COLON + fileXlsx)
            listParamsShow1.append(itemFileName)
            itemSheet = str(tAction.params[1].name + Constants.COLON + tAction.params[1].value)
            listParamsShow1.append(itemSheet)
            itemRow = str(tAction.params[2].name + Constants.COLON + str(row))
            listParamsShow1.append(itemRow)
            itemColumn = str(tAction.params[3].name + Constants.COLON + str(column))
            listParamsShow1.append(itemColumn)
            itemResult = str(tAction.params[4].name + Constants.COLON + str(strData))
            listParamsShow1.append(itemResult)
        except Exception as ex:
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_GetCheckboxValue():
    def execute(self, projectPath, driver, executeItem, listVariable, resourceFolder,  listParamsShow1, browserName):
        tAction = executeItem.testAction
        try:
            screen, control, index, valueStr = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            global element
            element = None
            
            if driver is None:
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                elements = LocatorHelper.get_element_by_locator2(driver, projectPath, screen, executeItem, control, resourceFolder, listVariable, index)
                if elements != None:
                    variableStr = Constants.EMPTY_STRING
                    #dùng để kiểm tra checkbox đã được checked hay chưa
                    #nếu đã được checked thì trả về cho variableStr giá trị On
                    #sau đó sẽ ghi kết quả variableStr vào Result 
                    if type(elements) is list:
                        for element in elements:
                            boolCheck = element.get_attribute(Constants.ATTR_CHECKED)
                            if boolCheck == Constants.TRUE:
                                variableStr = Constants.ON
                            else:
                                variableStr = Constants.OFF
                    else:
                        boolCheck = elements.get_attribute(Constants.ATTR_CHECKED)
                        if boolCheck == Constants.TRUE:
                            variableStr = Constants.ON
                        else:
                            variableStr = Constants.OFF
                    val = LocatorHelper.set_value_from_variable(valueStr, "'" + variableStr + "'", listVariable)
                    #write log file
                    itemScreen = str("Screen" + Constants.COLON + screen)
                    listParamsShow1.append(itemScreen)
                    itemControl = str("Control" + Constants.COLON + control)
                    listParamsShow1.append(itemControl)
                    itemIndex = str("Index" + Constants.COLON + str(index))
                    listParamsShow1.append(itemIndex)
                    itemValue = str("Value" + Constants.COLON + val)
                    listParamsShow1.append(itemValue) 
                    
                    res = RESULT(
                        Constants.RESULT_OK,
                        val,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
                else:
                    res = RESULT(
                        Constants.RESULT_NOK,
                        Constants.MSG_CONTROL_NOT_EXIST,
                        Constants.EMPTY_OBJECT,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
                
        except Exception as ex:
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_GetColumn:
    #lấy ra giá trị trong file excel theo cột với rangeStr(E1:F6) được truyền từ file Execute....json
    def get_cell_range(self,start_col, start_row, sheet, book,sheetPage):
        sheet = book.sheet_by_name(sheetPage)
        data = []
        for i in range(sheet.nrows):
            value = sheet.cell_value(i, start_col)
            data.append(value)
        return data
    def execute(self,executeFilePath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        
        try:
            tAction = executeItem.testAction
            # fileXl = tAction.params[0].value
            # sheetPage = tAction.params[1].value
            # rangeStr = tAction.params[2].value
            # result = tAction.params[3].value
            fileXl, sheetPage, rangeStr, result = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            fileXlsx = LocatorHelper.get_value_from_variable(fileXl , listVariable)
            sheetPage = LocatorHelper.get_value_from_variable(sheetPage , listVariable)
            book = open_workbook(fileXlsx)
            sheet = book.sheet_by_name(sheetPage)
            sC,sR = CheckHelper.generateDataByIput(rangeStr)
            data = self.get_cell_range(sC, sR, sheet, book,sheetPage) 
            
            lstData = []
            lst = []
            for i in data:
                #kiểm tra nếu vị trí lấy ra mang kiểu là float
                #nếu mà giá trị float đó là 1 số thập phân thì bỏ hết phần sau dấu chấm, chỉ lấy phần nguyên
                if type(i) is float: 
                    index = str(i).split(Constants.DOT)[1]
                    if int(index) == 0:
                        lst.append(str(i).split(Constants.DOT)[0])
                    else:
                        lst.append(str(i))
                else:
                    lst.append(i)  
            # lstData.append(lst)  
            lstData = LocatorHelper.set_value_from_variable(result, lst , listVariable)
            #write log file
            itemFileName = str(tAction.params[0].name + Constants.COLON + fileXlsx)
            listParamsShow1.append(itemFileName)
            itemSheet = str(tAction.params[1].name + Constants.COLON + tAction.params[1].value)
            listParamsShow1.append(itemSheet)
            itemRange = str(tAction.params[2].name + Constants.COLON + tAction.params[2].value)
            listParamsShow1.append(itemRange)
            itemResult = str(tAction.params[3].name + Constants.COLON + str(lstData))
            listParamsShow1.append(itemResult)
            res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                str(lstData),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        except Exception as ex:
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_GetControlBy:
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        try:
            tAction = executeItem.testAction
            # controlName = tAction.params[0].value
            # detectBy = tAction.params[1].value
            # valueStr = tAction.params[2].value
            controlName, detectBy, valueStr = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            
            listCondition = {}
            valueStr = LocatorHelper.get_value_from_variable(valueStr, listVariable)
            
            listCondition[detectBy] = valueStr
            
            if controlName[0] == "#":
                controlName  = LocatorHelper.set_value_from_variable(controlName, listCondition, listVariable)
            else:
                controlName = LocatorHelper.set_value_from_variable(Constants.SHARP+controlName, listCondition, listVariable)
            
            #write log file
            itemControlBy = str(tAction.params[0].name + Constants.COLON + tAction.params[0].value)
            listParamsShow1.append(itemControlBy)
            itemDetectBy = str(tAction.params[1].name + Constants.COLON + tAction.params[1].value)
            listParamsShow1.append(itemDetectBy)
            itemValue = str(tAction.params[2].name + Constants.COLON + tAction.params[2].value)
            listParamsShow1.append(itemValue)
            res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        except Exception as ex:
            print("Error action get control by")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_GetControlValue():
    def execute(self, projectPath, driver, executeItem, listVariable, resourceFolder,  listParamsShow1, browserName):
        element = None
        tAction = executeItem.testAction
        try:
            screen, control, index, value = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
        
            vals = Constants.BLANK
            if index == None:
                index = 0
                
            if driver is None:
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                variable = CheckHelper.get_control_value(driver, projectPath, screen, executeItem, control, resourceFolder, listVariable, index)
                
                variable = LocatorHelper.set_value_from_variable(value, variable, listVariable)
                
                #write log file
                itemScreen = str("Screen" + Constants.COLON + screen)
                listParamsShow1.append(itemScreen)
                itemControl = str("Item" + Constants.COLON + control)
                listParamsShow1.append(itemControl)
                itemIndex = str("Index" + Constants.COLON + str(index))
                listParamsShow1.append(itemIndex)
                itemValue = str("Value" + Constants.COLON + str(variable))
                listParamsShow1.append(itemValue)
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    variable,
                    Constants.EMPTY_STRING
                )
                    
        except Exception as ex:
            print("Error action get control value")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_GetCsvFile:
    def execute(self,executeFilePath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        
        try:
            tAction = executeItem.testAction
            # file_name = tAction.params[0].value
            # result = tAction.params[1].value
            file_name, result = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            file_csv = LocatorHelper.get_value_from_variable(file_name , listVariable)
            nameFileStr = os.path.basename(file_csv).split(Constants.DOT)[1]
            if nameFileStr == "csv":
                with open(file_csv, encoding="utf-8") as csvfile:
                    lstValue = []
                    #mở file csv và đọc sau đó truyền vào list
                    readCSV = csv.reader(csvfile, delimiter = Constants.COMMA)
                    for row in readCSV:
                        lstValue.append(row)
                lstValue = LocatorHelper.set_value_from_variable(result, lstValue , listVariable)
                
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    str(lstValue),
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                lstValue = Constants.BLANK
                res = RESULT(
                    Constants.RESULT_NOK,
                    "File not support!",
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            
            #write log file
            itemFileName = str(tAction.params[0].name + Constants.COLON + file_csv)
            listParamsShow1.append(itemFileName)
            itemResult = str(tAction.params[1].name + Constants.COLON + str(lstValue))
            listParamsShow1.append(itemResult)
            
        except Exception as ex:
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_GetFileExtension:
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        try:
            path, resultStr = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            
            result = LocatorHelper.get_value_from_variable(path, listVariable)
            
            nameFileStr = os.path.basename(result).split(Constants.DOT)[1]
            nameFile = LocatorHelper.set_value_from_variable(resultStr, nameFileStr, listVariable)
            #write log file
            itemFilePath = str(tAction.params[0].name + Constants.COLON + result)
            listParamsShow1.append(itemFilePath)
            itemFileName = str(tAction.params[1].name + Constants.COLON + nameFile)
            listParamsShow1.append(itemFileName)
            res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        except Exception as ex:
            print("Error action get file extension")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_GetFileName:
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        try:
            path, resultStr = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            
            result = LocatorHelper.get_value_from_variable(path, listVariable)
            
            nameFileStr = os.path.basename(result).split(Constants.DOT)[0]
            nameFile = LocatorHelper.set_value_from_variable(resultStr, nameFileStr, listVariable)
            #write log file
            itemFilePath = str(tAction.params[0].name + Constants.COLON + result)
            listParamsShow1.append(itemFilePath)
            itemFileName = str(tAction.params[1].name + Constants.COLON + nameFile)
            listParamsShow1.append(itemFileName)
            res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        except Exception as ex:
            print("Error action get file name")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_GetFileNameWithExtension:
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        try:
            path, resultStr = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            
            result = LocatorHelper.get_value_from_variable(path, listVariable)
            
            nameFileStr = os.path.basename(result)
            nameFile = LocatorHelper.set_value_from_variable(resultStr, nameFileStr, listVariable)
            #write log file
            itemFilePath = str(tAction.params[0].name + Constants.COLON + result)
            listParamsShow1.append(itemFilePath)
            itemFileName = str(tAction.params[1].name + Constants.COLON + nameFile)
            listParamsShow1.append(itemFileName)
            res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        except Exception as ex:
            print("Error action get file name with extension")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_GetFilesInFolder:
    def execute(self, executeFilePath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        
        try:
            tAction = executeItem.testAction
            # pathStr = tAction.params[0].value
            # typeFile = tAction.params[1].value
            # result = tAction.params[2].value
            pathStr, typeFile, result = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            pathStr = LocatorHelper.get_value_from_variable(pathStr, listVariable)
            typeFile = LocatorHelper.get_value_from_variable(typeFile, listVariable)
            listLibrary = os.listdir(pathStr)
            listLibChoose = []
            isCheck = Constants.BLANK
            pathSave = Constants.BLANK
            
            for i in listLibrary:
                if typeFile in i:
                    pathSave = os.path.join(pathStr, i)
                    listLibChoose.append(pathSave)
            listLibChoose = LocatorHelper.set_value_from_variable(result, listLibChoose, listVariable)
            #write log file
            itemFilePath = str(tAction.params[0].name + Constants.COLON + pathStr)
            listParamsShow1.append(itemFilePath)
            itemTypeFile = str(tAction.params[1].name + Constants.COLON + typeFile)
            listParamsShow1.append(itemTypeFile)
            itemResult = str(tAction.params[2].name + Constants.COLON + str(listLibChoose))
            listParamsShow1.append(itemResult)
            res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
            
        except Exception as ex:
            print("Error action get file in folder")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_GetFolders:
    def execute(self,executeFilePath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        
        try:
            tAction = executeItem.testAction
            # pathStr = tAction.params[0].value
            # result = tAction.params[1].value
            pathStr, result = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            pathStr = LocatorHelper.get_value_from_variable(pathStr, listVariable)
            
            listLibrary = os.listdir(pathStr)
            listLibChoose = []
            for i in listLibrary:
                pathSave = pathStr + Constants.SLASH + i
                pathSave = os.path.join(pathStr, i)
                if os.path.isdir(pathSave):
                    listLibChoose.append(pathSave)
            listLibChoose = LocatorHelper.set_value_from_variable(result, listLibChoose, listVariable)
            
            #write log file
            itemPath = str(tAction.params[0].name + Constants.COLON + pathStr)
            listParamsShow1.append(itemPath)
            itemResult = str(tAction.params[1].name + Constants.COLON + str(listLibChoose))
            listParamsShow1.append(itemResult)
            res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
            
        except Exception as ex:
            print("Error action get folders")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_GetFtpFolders():
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        
        try:
            connectionName, remotePath, result = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            if driver is None:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                connectionValue = listVariable[connectionName]
                hostFtp = connectionValue["Host"]
                portFtp = connectionValue["Port"]
                usernameFtp = connectionValue["Username"]
                passwordFtp = connectionValue["Password"]
                ftp = ftplib.FTP()
                ftp.connect(hostFtp, int(portFtp))
                
                ftp.login(usernameFtp, passwordFtp)
                data = []
                ftp.dir(data.append)
                remotePath = LocatorHelper.get_value_from_variable(remotePath, listVariable)
                
                ftp.cwd(remotePath)
                file_list = []
                listFolder = []
                ftp.retrlines("NLST", file_list.append)
                listFolder = file_list
                
                ftp.quit()
                listFolder = LocatorHelper.set_value_from_variable(result, listFolder, listVariable)
                #Write log file
                itemConnectionName = str(tAction.params[0].name + Constants.COLON + tAction.params[0].value)
                listParamsShow1.append(itemConnectionName)
                itemRemote = str(tAction.params[1].name + Constants.COLON + remotePath)
                listParamsShow1.append(itemRemote)
                itemResult = str(tAction.params[2].name + Constants.COLON + str(listFolder))
                listParamsShow1.append(itemResult)
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
        except Exception as ex:
            print("Error action get ftp folders")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_GetJavascriptVariable:
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        try:
            variableName, result = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            getJsonTet = Constants.RETURN_SPACE + variableName + Constants.SEMICOLON
            varResult = driver.execute_script(getJsonTet)
            varResult = LocatorHelper.set_value_from_variable(result, varResult, listVariable)
            
            #write log file
            itemvariableName = str(tAction.params[0].name + Constants.COLON + tAction.params[0].value)
            listParamsShow1.append(itemvariableName)
            itemResult = str(tAction.params[1].name + Constants.COLON + varResult)
            listParamsShow1.append(itemResult)
            res = RESULT(
                Constants.RESULT_OK,
                varResult,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        except Exception as ex:
            print("Error action get javascript variable")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_GetRangeValue:
    #trả về 1 list các giá trị từ vị trí được chỉ định trong rangeStr(ví dụ: A2:G4)
    #những hàm khác ko hiểu có thể xem bên Action GetCellColor
    def get_cell_range(self,start_col, start_row, end_col, end_row, sheet, book,sheetPage):
        result = Constants.BLANK 
        try:
            data = []
            sheet = book.sheet_by_name(sheetPage)
            for row in range(start_row, end_row+1):
                data.append(sheet.row_slice(row, start_col, end_col+1))
            result = data
            code = Constants.RESULT_OK
        except Exception as e:
            result = e
            code = Constants.RESULT_CRASH
        return (result,code)
    def generateColnRow(self,inputData):
        dataBeforeAdd = Constants.EMPTY_STRING 
        dataValid = Constants.EMPTY_STRING
        #reversed lấy dữ liệu theo cơ chế giống stack(vào trước ra sau)
        for i in reversed(inputData):
            try:
                dataBeforeAdd = i + dataBeforeAdd 
                val = int(dataBeforeAdd)
                dataValid = dataBeforeAdd
            except Exception as e:
                break
        startRow = dataValid
        startColumn = inputData.replace(dataValid,Constants.EMPTY_STRING)
        return (startRow, startColumn)
    
    def getNumberFromCharacter(self,char):
        res = 0
        index = -1
        ls = Constants.LIST_ALPHABET_TABLE
        for c in reversed(char):
            index += 1
            for i in ls:
                for key,val in i.items():
                    if str(c) == str(key):
                        res = res + val
                        if index > 0:
                            res += 26
        return res           
    def optimizingData(self,sRow,sCol):
        return (int(sRow) - 1, self.getNumberFromCharacter(sCol))
    #trả về vị trí tương ứng theo hàng, cột của A2:G4 được truyền trong rangeStr
    def generateDataByIput(self,data):
        s = data.split(Constants.COLON)[0] 
        e = data.split(Constants.COLON)[1] 
        sRow, sCol = self.generateColnRow(s)
        eRow, eCol = self.generateColnRow(e)
        sRowOK, sColOK = self.optimizingData(sRow, sCol)
        eRowOK, eColOK = self.optimizingData(eRow, eCol)
        return (sColOK, sRowOK, eColOK, eRowOK)
    def execute(self,executeFilePath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        
        try:
            tAction = executeItem.testAction
            # fileXl = tAction.params[0].value
            # sheetPage = tAction.params[1].value
            # rangeStr = tAction.params[2].value #VD: rangeStr = A2:G4
            # result = tAction.params[3].value
            fileXl, sheetPage, rangeStr, result = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            fileXlsx = LocatorHelper.get_value_from_variable(fileXl, listVariable)
            sheetPage = LocatorHelper.get_value_from_variable(sheetPage , listVariable)
            nameFileStr = os.path.basename(fileXlsx).split(Constants.DOT)[1]
            if nameFileStr == Constants.XLSX or nameFileStr == Constants.XLS:
                book = open_workbook(fileXlsx)
                sheet = book.sheet_by_name(sheetPage)
                sC,sR,eC,eR = self.generateDataByIput(rangeStr)
                data,code = self.get_cell_range(sC, sR, eC, eR, sheet, book,sheetPage)
            
                lis = []
                
                if code == Constants.RESULT_OK:
                    for item in data:
                        lits = []
                        #for theo độ dài của data[0]
                        for i in range(len(data[0])):
                            if i != len(data[0]):
                                #bỏ số 0 phía sau số thập phân
                                if type(item[i].value) is float:
                                    index = str(item[i].value).split(Constants.DOT)[1]
                                    if int(index) == 0:
                                        lits.append(str(item[i].value).split(Constants.DOT)[0]) 
                                    else:
                                        lits.append(str(item[i].value))
                                else:
                                    lits.append(item[i].value)
                        lis.append(lits)
                    
                    lis = LocatorHelper.set_value_from_variable(result, lis , listVariable)
                        
                    res = RESULT(
                        Constants.RESULT_OK,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        str(lis),
                        Constants.EMPTY_STRING
                    )
                else:
                    lis = Constants.BLANK
                    res = RESULT(
                        Constants.RESULT_NOK,
                        "Please choose range valid!",
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
            else:
                lis = Constants.BLANK
                res = RESULT(
                    Constants.RESULT_NOK,
                    "File not support!",
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            
            #write log file
            itemFileName = str(tAction.params[0].name + Constants.COLON + fileXlsx)
            listParamsShow1.append(itemFileName)
            itemSheet = str(tAction.params[1].name + Constants.COLON + tAction.params[1].value)
            listParamsShow1.append(itemSheet)
            itemRange = str(tAction.params[2].name + Constants.COLON + tAction.params[2].value)
            listParamsShow1.append(itemRange)
            itemResult = str(tAction.params[3].name + Constants.COLON + str(lis))
            listParamsShow1.append(itemResult)
        except Exception as ex:
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                "Please check value input!",
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_GetRow:
    #lấy ra list dữ liệu trong file excel theo row được truyền sẵn
    def get_cell_range(self,start_col, start_row, sheet, book,sheetPage):
        sheet = book.sheet_by_name(sheetPage)
        data = []
        for i in range(sheet.ncols): 
            value = sheet.cell_value(start_row, i)
            data.append(value)
        return data
    def execute(self,executeFilePath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        
        try:
            tAction = executeItem.testAction
            # fileXl = tAction.params[0].value
            # sheetPage = tAction.params[1].value
            # rangeStr = tAction.params[2].value
            # result = tAction.params[3].value
            fileXl, sheetPage, rangeStr, result = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            fileXlsx = LocatorHelper.get_value_from_variable(fileXl , listVariable)
            sheetPage = LocatorHelper.get_value_from_variable(sheetPage , listVariable)
            book = xlrd.open_workbook(fileXlsx)
            sheet = book.sheet_by_name(sheetPage)
            sC,sR = CheckHelper.generateDataByIput(rangeStr)
            data = self.get_cell_range(sC, sR, sheet, book,sheetPage)
            lstData = []
            lst = []
            for i in data:
                #có thể xem bên action getRangevalue
                if type(i) is float: 
                    index = str(i).split(Constants.DOT)[1]
                    if int(index) == 0:
                        lst.append(str(i).split(Constants.DOT)[0])
                    else:
                        lst.append(str(i))
                else:
                    lst.append(i)  
            lstData.append(lst)
            lstData = LocatorHelper.set_value_from_variable(result, lstData , listVariable)
            
            #write log file
            itemFileName = str(tAction.params[0].name + Constants.COLON + fileXlsx)
            listParamsShow1.append(itemFileName)
            itemSheet = str(tAction.params[1].name + Constants.COLON + tAction.params[1].value)
            listParamsShow1.append(itemSheet)
            itemRange = str(tAction.params[2].name + Constants.COLON + tAction.params[2].value)
            listParamsShow1.append(itemRange)
            itemResult = str(tAction.params[3].name + Constants.COLON + str(lstData))
            listParamsShow1.append(itemResult)
            res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                str(lstData),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        except Exception as ex:
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_GetTableData(object):
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder,  listParamsShow1, browserName):
        tAction = executeItem.testAction
        
        
        try:
            screen, control, index, value = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            prItem = {} 
            if driver is None:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                elements = LocatorHelper.get_element_by_locator2(driver, xmlPath,screen,executeItem, control, resourceFolder, listVariable, index)
                if type(elements) is list:
                    for element in elements:
                        file_data = []
                        file_header = []
                        body = element.find_element_by_tag_name(Constants.TBODY)
                        body_rows = body.find_elements_by_tag_name(Constants.TR)
                        for row in body_rows:
                            data = row.find_elements_by_tag_name(Constants.TD)
                            file_row = []
                            for datum in data:
                                datum_text = datum.text
                                file_row.append(datum_text) 
                            file_data.append(file_row)
                else:
                    file_data = []
                    file_header = []
                    body = elements.find_element_by_tag_name(Constants.TBODY)
                    body_rows = body.find_elements_by_tag_name(Constants.TR)
                    for row in body_rows:
                        data = row.find_elements_by_tag_name(Constants.TD)
                        file_row = []
                        for datum in data:
                            datum_text = datum.text
                            file_row.append(datum_text) 
                        file_data.append(file_row)
                
                value = LocatorHelper.set_value_from_variable(value, file_data, listVariable)
                #write log file
                itemScreen = str("Screen" + Constants.COLON + screen)
                listParamsShow1.append(itemScreen)
                itemControl = str("Control" + Constants.COLON + control)
                listParamsShow1.append(itemControl)
                itemIndex = str("Index" + Constants.COLON + str(index))
                listParamsShow1.append(itemIndex)
                itemValue = str("Value" + Constants.COLON + str(value))
                listParamsShow1.append(itemValue) 
                
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
        except Exception as ex:
            print("Error Action Get Table Data")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_GetTextFile:
    def execute(self, projectPath, driver, executeItem):
        tAction = executeItem.testAction
        tsName = executeItem.testSuiteName
        tsFilePath = os.path.join(projectPath,Constants.TAG_TESTSUITES,tsName + Constants.EXT_XML)
        acSetVariable = AC_SetVariable()
        filePath = tAction.params[0].value
        variable = tAction.params[1].value
        try:                  
            if os.path.isfile(filePath):
                with open(filePath, Constants.FILE_R) as myfile:
                    content = myfile.read().replace(Constants.NEWLINE, Constants.EMPTY_STRING)
                acSetVariable.execute(tsFilePath, variable, content)
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_OBJECT,
                    variable,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_FILE_IS_NOT_EXIST,
                    Constants.EMPTY_OBJECT,
                    variable,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
        except Exception as ex :            
            res = RESULT(
                Constants.RESULT_NOK,
                ex,
                Constants.EMPTY_OBJECT,
                variable,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_If:
    """description of class"""
    #so sánh 2 giá trị truyền vào với op là đúng hay sai
    def compare(self,arg1, op, arg2):
        ops = {
            Constants.LESS_THANS: operator.lt,
            Constants.LESS_THAN_OR_EQUALS: operator.le,
            Constants.EQUAL_EQUAL: operator.eq,
            Constants.NOT_EQUAL: operator.ne,
            Constants.MORE_THAN_OR_EQUAL: operator.ge,
            Constants.MORE_THAN: operator.gt
        }
        operation = ops.get(op)
        return operation(arg1,arg2)
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, indexExecute, listScript, indexLoop):
        tAction = executeItem.testAction
        try:
            prCondition = LocatorHelper.getValueFromParam(tAction.params,tAction.id)
            booleanCheck = LocatorHelper.get_value_from_variable(prCondition, listVariable)
            
            #     booleanCheck = self.compare(int(value), operators, int(valCondValue))
            #write log file
            itemCondition = str("Condition" + Constants.COLON + prCondition)
            listParamsShow1.append(itemCondition)
            if booleanCheck == True:
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.TRUE,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    indexExecute + 1
                )  
            else:
                countIf = 1
                index = indexExecute + 1
                while (countIf > 0) and (index < len(listScript)):
                    if (listScript[index] == Constants.AC_IF):
                        countIf = countIf + 1
                    elif (listScript[index] == Constants.AC_ENDIF):
                        countIf = countIf -1
                    index = index + 1
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.FALSE,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    index
                )  
            
        except Exception as ex:
            print("Error action If")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                "Error, please check the format or anything!!!",
                Constants.EMPTY_OBJECT,
                Constants.MSG_CONTROL_IS_EXIST,
                Constants.MSG_CONTROL_NOT_EXIST,
                Constants.EMPTY_STRING
            )
        return res


class AC_Loop:
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, indexExecute, listScript, indexLoop):
        tAction = executeItem.testAction
        try:
            prTimes, prVariable = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            lenList = LocatorHelper.get_value_from_variable(prTimes, listVariable)
            if prVariable in listVariable.keys():
                indexLoop = listVariable[prVariable]
            else:
                indexLoop = 0
            if int(indexLoop) < lenList:
                index = indexExecute + 1
                indexLoop = indexLoop + 1
                indexCount = LocatorHelper.set_value_from_variable(prVariable, indexLoop, listVariable)
            else:
                countLoop = 1
                index = indexExecute + 1
                while (countLoop > 0) and (index < len(listScript)):
                    if (listScript[index] == Constants.AC_LOOP):
                        countLoop = countLoop + 1
                    elif (listScript[index] == Constants.AC_ENDLOOP):
                        countLoop = countLoop -1
                    index = index + 1
                listVariable.pop(prVariable)
                indexLoop = 0
            #write log file
            itemPrTime = str('Times' + Constants.COLON + str(prTimes))
            listParamsShow1.append(itemPrTime)
            itemPrVariable = str('Variable' + Constants.COLON + str(indexLoop-1))
            listParamsShow1.append(itemPrVariable)
            
            res = RESULT(
                Constants.RESULT_OK,
                indexLoop,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                index
            )
        except Exception as ex:
            print("Error Action Loop")
            print (ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                "Error format!",
                Constants.EMPTY_OBJECT,
                Constants.MSG_CONTROL_IS_EXIST,
                Constants.MSG_CONTROL_NOT_EXIST,
                Constants.EMPTY_STRING
            )
        return res


class AC_MoveFile:
    def execute(self, projectPath, driver, executeItem):
        tAction = executeItem.testAction
        filePath = tAction.params[0].value       
        newfile = tAction.params[1].value
        try:                  
            file = filePath
            path, file_name = os.path.split(file)
            old_file = os.path.join(path,file_name)         
            os.rename(old_file, newfile)
            res = RESULT(
                Constants.RESULT_OK,
                Constants.MSG_SELECT_BROWSER,
                newfile,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        except Exception as ex :            
            res = RESULT(
                Constants.RESULT_NOK,
                ex,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_MoveFolder:
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        try:
            folderPath, newdirectory = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            
            folderPath = LocatorHelper.get_value_from_variable(folderPath, listVariable)
            newdirectory = LocatorHelper.get_value_from_variable(newdirectory, listVariable)
            
            if os.path.isfile(newdirectory) is False:
                result = shutil.move(folderPath, newdirectory)
            else:
                print("Exixts")
            #write log file
            itemFolderPath = str(tAction.params[0].name + Constants.COLON + folderPath)
            listParamsShow1.append(itemFolderPath)
            itemNewPath = str(tAction.params[1].name + Constants.COLON + newdirectory)
            listParamsShow1.append(itemNewPath)
            
            res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        except Exception as ex :  
            print("Error action move folder")
            print(ex)          
            res = RESULT(
                Constants.RESULT_NOK,
                ex,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_Open:
    def execute(self, executeFilePath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        try:
            url= LocatorHelper.getValueFromParam(tAction.params, tAction.id)
        
            value = Constants.EMPTY_STRING
            value = LocatorHelper.get_value_from_variable(url, listVariable)
            
            #write log file
            itemUrl = str(tAction.params[0].name + Constants.COLON + value)
            listParamsShow1.append(itemUrl)
            if driver is None:
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                driver.get(value)
                time.sleep(3)
                
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
        except Exception as e:
            print("Error action open")
            print(e)
            res = RESULT(
                Constants.RESULT_CRASH,
                "url invalid!",
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_RenameFile:
    def execute(self, projectPath, driver, executeItem):
        tAction = executeItem.testAction
        filePath = tAction.params[0].value    
        newname = tAction.params[1].value       
        try:                  
                file = filePath 
                path, file_name = os.path.split(file)
                old_file = os.path.join(path,file_name)
                newname = os.path.join(path,"example1234.txt")
                os.rename(old_file, newname)            
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.MSG_SELECT_BROWSER,
                    newname,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING                   
                )            
        except Exception as ex :            
            res = RESULT(
                Constants.RESULT_NOK,
                ex,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_RenameFolder:
    def execute(self,projectPath, driver, executeItem):
        tAction = executeItem.testAction
        folderPath = tAction.params[0].value
        newname = tAction.params[1].value
        try:
            if os.listdir(folderPath):
                os.rename(folderPath,newname)
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_NEW_NAME_FOLDER,
                    Constants.EMPTY_OBJECT
                )
        except Exception as ex :            
            res = RESULT(
                Constants.RESULT_NOK,
                ex,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_RightClick():
    def execute(self, projectPath, driver, executeItem, listVariable, resourceFolder,  listParamsShow1, browserName):
        global element
        element = None
        try:
            tAction = executeItem.testAction
            layout, control, index = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            if index == None:
                index = 0
            if driver is None:
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                actionChains = ActionChains(driver)
                elements = LocatorHelper.get_element_by_locator2(driver, projectPath, layout, executeItem, control, resourceFolder, listVariable, index)
                if elements != None:
                    #click chuột phải vào vị trí chỉ định
                    if type(elements) is list:
                        for element in elements:
                            actionChains.context_click(element).perform()
                    else:
                        actionChains.context_click(elements).perform()
                    #write log file
                    itemLayout = str("Screen" + Constants.COLON + layout)
                    listParamsShow1.append(itemLayout)
                    itemControl = str("Item" + Constants.COLON + control)
                    listParamsShow1.append(itemControl)
                    itemIndex = str("Index" + Constants.COLON + str(index))
                    listParamsShow1.append(itemIndex)
                    res = RESULT(
                        Constants.RESULT_OK,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
                else:
                    res = RESULT(
                        Constants.RESULT_NOK,
                        Constants.MSG_CONTROL_NOT_EXIST,
                        Constants.EMPTY_OBJECT,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
        except Exception as ex:
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_Select():
    def execute(self, projectPath, driver, executeItem, listVariable, resourceFolder,  listParamsShow1, browserName):
        tAction = executeItem.testAction
        
        try:
            screen, control, index, itemSelect = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            if index == None:
                index = 0
            global element
            element = None
            
            if driver is None:
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                elements = LocatorHelper.get_element_by_locator2(driver, projectPath,screen,executeItem, control, resourceFolder, listVariable, index)
                
                if type(elements) is list:
                    for element in elements:
                        sl = Select(element)
                        value = Constants.EMPTY_STRING
                        value = LocatorHelper.get_value_from_variable(itemSelect, listVariable)
                        
                        sl.select_by_visible_text(value)
                        value = LocatorHelper.set_value_from_variable(itemSelect, value, listVariable)
                else:
                    sl = Select(elements)
                    value = Constants.EMPTY_STRING
                    value = LocatorHelper.get_value_from_variable(itemSelect, listVariable)
                    
                    sl.select_by_visible_text(value)
                    value = LocatorHelper.set_value_from_variable(itemSelect, value, listVariable)
                
                #write log file
                itemLayout = str("Screen" + Constants.COLON + screen)
                listParamsShow1.append(itemLayout)
                itemControl = str("Item" + Constants.COLON + control)
                listParamsShow1.append(itemControl)
                itemIndex = str("Index" + Constants.COLON + str(index))
                listParamsShow1.append(itemIndex)
                itemSelect = str("Value" + Constants.COLON + value)
                listParamsShow1.append(itemSelect)
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
                
        except Exception as ex:
            res = RESULT(
                Constants.RESULT_CRASH,
                "Please check value input!",
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_SendEmail:
    def execute(self, xmlPath, driver, executeItem, lstVariables, resourceFolder, listParamsShow1, browserName):
        try:
            tAction = executeItem.testAction
            # fromStr = tAction.params[0].value
            # to = tAction.params[1].value
            
            # subject = tAction.params[2].value
            # content = tAction.params[3].value
            # attatchFile = tAction.params[4].value
            fromStr, to, subject, content, attatchFile = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            to = to.replace(Constants.SPACE,Constants.EMPTY_STRING)
            attatchFile = attatchFile.replace(Constants.SPACE,Constants.EMPTY_STRING)
            filename = Constants.EMPTY_STRING
            if driver is None:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                fromStr = lstVariables[fromStr]
            
                smtpServer = fromStr["SmtpServer"]
                port = fromStr["Port"]
                email = fromStr["Email"]
                password = fromStr["Password"]
                to = LocatorHelper.get_value_from_variable(to, lstVariables)
                subject = LocatorHelper.get_value_from_variable(subject, lstVariables)
                content = LocatorHelper.get_value_from_variable(content, lstVariables)
                
                if Constants.COMMAS in to:
                    listTo = to.split(Constants.COMMAS)
                
                if attatchFile == "None":
                    attatchFile = Constants.EMPTY_STRING
                    if Constants.COMMAS in to:
                        for i in listTo:
                            msg = MIMEMultipart()
                            msg['From'] = email
                            msg['To'] = i
                            msg['Subject'] = subject
                            body = content
                            msg.attach(MIMEText(body, 'plain'))
                            server = smtplib.SMTP(smtpServer, int(port))
                            server.starttls()
                            server.login(email, password)
                            text = msg.as_string()
                            server.sendmail(email, i, text)
                            server.quit()
                    else:
                        msg = MIMEMultipart()
                        msg['From'] = email
                        msg['To'] = to
                        msg['Subject'] = subject
                        body = content
                        msg.attach(MIMEText(body, 'plain'))
                        server = smtplib.SMTP(smtpServer, int(port))
                        server.starttls()
                        server.login(email, password)
                        text = msg.as_string()
                        server.sendmail(email, to, text)
                        server.quit()
                else:
                    if Constants.COMMAS in attatchFile:
                        listAttatch = attatchFile.split(Constants.COMMAS)
                        if Constants.COMMAS in to:
                            for j in listTo:
                                msg = MIMEMultipart()    
                                msg['From'] = email
                                msg['To'] = j
                                msg['Subject'] = subject
                                body = content
                                try:
                                    msg.attach(MIMEText(body, 'plain'))
                                    for i in listAttatch:
                                        filename = LocatorHelper.get_value_from_variable(i, lstVariables)
                                        attachment = open(filename, "rb")
                                        part = MIMEBase('application', 'octet-stream')
                                        part.set_payload((attachment).read())
                                        encoders.encode_base64(part)
                                        part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
                                        msg.attach(part)
                                    server = smtplib.SMTP(smtpServer, int(port))
                                    server.starttls()
                                    server.login(email, password)
                                    text = msg.as_string()
                                    server.sendmail(email, j, text)
                                    server.quit()
                                except Exception as e:
                                    print(str(e))
                        else:
                            msg = MIMEMultipart()    
                            msg['From'] = email
                            msg['To'] = to
                            msg['Subject'] = subject
                            body = content
                            try:
                                msg.attach(MIMEText(body, 'plain'))
                                for i in listAttatch:
                                    filename = LocatorHelper.get_value_from_variable(i, lstVariables)
                                    attachment = open(filename, "rb")
                                    part = MIMEBase('application', 'octet-stream')
                                    part.set_payload((attachment).read())
                                    encoders.encode_base64(part)
                                    part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
                                    msg.attach(part)
                                server = smtplib.SMTP(smtpServer, int(port))
                                server.starttls()
                                server.login(email, password)
                                text = msg.as_string()
                                server.sendmail(email, to, text)
                                server.quit()
                            except Exception as e:
                                print(str(e))
                    else:
                        if Constants.COMMAS in to:
                            for i in listTo:
                                msg = MIMEMultipart()    
                                msg['From'] = email
                                msg['To'] = i
                                msg['Subject'] = subject
                                body = content
                                try:
                                    msg.attach(MIMEText(body, 'plain'))
                                    filename = LocatorHelper.get_value_from_variable(attatchFile, lstVariables)
                                    attachment = open(filename, "rb")
                                    part = MIMEBase('application', 'octet-stream')
                                    part.set_payload((attachment).read())
                                    encoders.encode_base64(part)
                                    part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
                                    msg.attach(part)
                                    server = smtplib.SMTP(smtpServer, int(port))
                                    server.starttls()
                                    server.login(email, password)
                                    text = msg.as_string()
                                    server.sendmail(email, i, text)
                                    server.quit()
                                except Exception as e:
                                    print(str(e))
                        else:
                            msg = MIMEMultipart()    
                            msg['From'] = email
                            msg['To'] = to
                            msg['Subject'] = subject
                            body = content
                            try:
                                msg.attach(MIMEText(body, 'plain'))
                                filename = LocatorHelper.get_value_from_variable(attatchFile, lstVariables)
                                attachment = open(filename, "rb")
                                part = MIMEBase('application', 'octet-stream')
                                part.set_payload((attachment).read())
                                encoders.encode_base64(part)
                                part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
                                msg.attach(part)
                                server = smtplib.SMTP(smtpServer, int(port))
                                server.starttls()
                                server.login(email, password)
                                text = msg.as_string()
                                server.sendmail(email, to, text)
                                server.quit()
                            except Exception as e:
                                print(str(e))
                
                #Write log file
                itemFrom = str(tAction.params[0].name + Constants.COLON + tAction.params[0].value)
                listParamsShow1.append(itemFrom)
                itemTo = str(tAction.params[1].name + Constants.COLON + to)
                listParamsShow1.append(itemTo)
                itemSubject = str(tAction.params[2].name + Constants.COLON + subject)
                listParamsShow1.append(itemSubject)
                itemContent = str(tAction.params[3].name + Constants.COLON + content)
                listParamsShow1.append(itemContent)
                itemAttachFile = str(tAction.params[4].name + Constants.COLON + filename)
                listParamsShow1.append(itemAttachFile)
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
        except Exception as ex:
            print("Error action send email")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_SendKeys():
    """description of class"""
    def execute(self, xmlPath, driver, executeItem):
        try:
            tAction = executeItem.testAction
            # keyspress = tAction.params[0].value
            keyspress = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            #pyautogui.press(keyspress)
            res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        except Exception as ex:
            print (ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                ex,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_SetCellValue:
    def execute(self,executeFilePath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        
        try:
            tAction = executeItem.testAction
            fileXl, sheetPage, row, column, result = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            row = LocatorHelper.get_value_from_variable(row, listVariable)
            column = LocatorHelper.get_value_from_variable(column, listVariable)
            
            fileXlsx = LocatorHelper.get_value_from_variable(fileXl , listVariable)
            sheetPage = LocatorHelper.get_value_from_variable(sheetPage , listVariable)
            
            result = LocatorHelper.get_value_from_variable(result , listVariable)
            
            nameFileStr = os.path.basename(fileXlsx).split(Constants.DOT)[1]
            
            if nameFileStr == Constants.XLSX:
                book = openpyxl.load_workbook(fileXlsx)
                sheet = book[sheetPage]
                sheet.cell(row=int(row), column=int(column)).value = str(result)
                book.save(fileXlsx)
            else:
                book = open_workbook(fileXlsx)
                sheet = book.sheet_by_name(sheetPage)
                rb = open_workbook(fileXlsx)
                wb = copy(rb)
                sheet = wb.get_sheet(sheetPage)
                sheet.write(int(row),int(column),str(result))
                wb.save(fileXlsx)
            
            #write log file
            itemFileName = str(tAction.params[0].name + Constants.COLON + fileXlsx)
            listParamsShow1.append(itemFileName)
            itemSheet = str(tAction.params[1].name + Constants.COLON + tAction.params[1].value)
            listParamsShow1.append(itemSheet)
            itemRow = str(tAction.params[2].name + Constants.COLON + str(row))
            listParamsShow1.append(itemRow)
            itemColumn = str(tAction.params[3].name + Constants.COLON + str(column))
            listParamsShow1.append(itemColumn)
            itemResult = str(tAction.params[4].name + Constants.COLON + str(result))
            listParamsShow1.append(itemResult)
            
            res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        except Exception as ex:
            print("Error action set cell value")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_SetCheckboxValue():
    def execute(self, projectPath, driver, executeItem, listVariable, resourceFolder,  listParamsShow1, browserName):
        tAction = executeItem.testAction
        try:
            screen, control, index, valueStr = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            global element
            element = None
            if driver is None:
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                valueConvert = Constants.EMPTY_STRING
                elements = LocatorHelper.get_element_by_locator2(driver, projectPath, screen, executeItem, control, resourceFolder, listVariable, index)
                #convert On thành True, off thành False để so sánh dễ dàng hơn
                valueStr = LocatorHelper.get_value_from_variable(valueStr, listVariable)
                
                if valueStr == Constants.ON:
                    valueConvert = Constants.TRUE
                elif valueStr == Constants.OFF or valueStr == "None":
                    valueConvert = Constants.FALSE
                else:
                    valueConvert = valueStr
                
                if elements != None:
                    if type(elements) is list:
                        for element in elements:
                            if element.tag_name == Constants.ATTR_INPUT and element.get_attribute(Constants.ATTR_TYPE) == Constants.ATTR_CHECKBOX:
                                #kiểm tra nếu đã checked thì thôi còn chưa thì click vào
                                if element.get_attribute(Constants.ATTR_CHECKED) == valueConvert.lower():
                                    print("ok")
                                else:
                                    if element.get_attribute(Constants.ATTR_CHECKED) == None:
                                        if valueConvert == "true" or valueConvert == "on":
                                            element.click()
                                    elif element.get_attribute(Constants.ATTR_CHECKED) != valueConvert.lower():
                                        element.click()
                                res = RESULT(
                                    Constants.RESULT_OK,
                                    Constants.ATTR_CHECKED,
                                    Constants.EMPTY_STRING,
                                    Constants.EMPTY_STRING,
                                    Constants.EMPTY_STRING,
                                    Constants.EMPTY_STRING
                                )
                            else:
                                res = RESULT(
                                    Constants.RESULT_OK,
                                    Constants.MSG_ACTION_NOT_SUPPORT,
                                    Constants.EMPTY_STRING,
                                    Constants.EMPTY_STRING,
                                    Constants.EMPTY_STRING,
                                    Constants.EMPTY_STRING
                                )
                    else:
                        if elements.tag_name == Constants.ATTR_INPUT and elements.get_attribute(Constants.ATTR_TYPE) == Constants.ATTR_CHECKBOX:
                            #kiểm tra nếu đã checked thì thôi còn chưa thì click vào
                            if elements.get_attribute(Constants.ATTR_CHECKED) == valueConvert.lower():
                                print("ok")
                            else:
                                if elements.get_attribute(Constants.ATTR_CHECKED) == None:
                                    if valueConvert == "true" or valueConvert == "on":
                                        elements.click()
                                elif elements.get_attribute(Constants.ATTR_CHECKED) != valueConvert.lower():
                                    elements.click()
                            res = RESULT(
                                Constants.RESULT_OK,
                                Constants.ATTR_CHECKED,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING
                            )
                        else:
                            res = RESULT(
                                Constants.RESULT_OK,
                                Constants.MSG_ACTION_NOT_SUPPORT,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING
                            )
                else:
                    res = RESULT(
                        Constants.RESULT_NOK,
                        Constants.MSG_CONTROL_NOT_EXIST,
                        Constants.EMPTY_OBJECT,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
                #write log file
                itemScreen = str("Screen" + Constants.COLON + screen)
                listParamsShow1.append(itemScreen)
                itemControl = str("Control" + Constants.COLON + control)
                listParamsShow1.append(itemControl)
                itemIndex = str("Index" + Constants.COLON + str(index))
                listParamsShow1.append(itemIndex)
                itemValue = str("Value" + Constants.COLON + valueConvert)
                listParamsShow1.append(itemValue) 
                
        except Exception as ex:
            res = RESULT(
                Constants.RESULT_CRASH,
                "Please check value input!",
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_SetControlFocus(object):
    """description of class"""
    def import_class_from_string(self, moduleName, className):
        from importlib import import_module
        mod = import_module(moduleName+ Constants.DOT +className)
        klass = getattr(mod, className)
        return klass
    def execute(self, projectPath, driver ,executeItem):
        try:
            tAction = executeItem.testAction
            tsName = executeItem.testSuiteName
            tcId = executeItem.testCaseId
            acRow = tAction.row
            element = None
            acDoesControlExist = eval( Constants.AC_DOES_CONTROL_EXIST)
            resDoesControlExist = acDoesControlExist().execute(projectPath, driver, executeItem)
            if resDoesControlExist.resultCode == Constants.RESULT_NOK:
                res = RESULT(
                    Constants.RESULT_CRASH,
                    Constants.MSG_CONTROL_NOT_EXIST,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
                return res
            elif resDoesControlExist.resultCode == Constants.RESULT_CRASH and resDoesControlExist.resultMsg == Constants.MSG_LAYOUT_FILE_NOT_EXIST:
                res = RESULT(
                    Constants.RESULT_CRASH,
                    Constants.MSG_LAYOUT_FILE_NOT_EXIST,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
                return res
            else:
                element = resDoesControlExist.resultObj
                element.click()
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
        except Exception as ex:
            res = RESULT(
                Constants.RESULT_CRASH,
                ex,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_SetControlValue():
    def execute(self, projectPath, driver, executeItem,listVariable, resourceFolder, listParamsShow1, browserName):
        global element
        element = None
        tAction = executeItem.testAction
        try:
            screen, control, index, valueStr = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            value = Constants.EMPTY_STRING
            
            value = LocatorHelper.get_value_from_variable(valueStr, listVariable)
            
            if driver is None:
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                elements = LocatorHelper.get_element_by_locator2(driver, projectPath, screen, executeItem, control, resourceFolder, listVariable, index)
                
                if type(elements) is list:
                    for element in elements:
                        if element.tag_name == Constants.ATTR_INPUT:
                            if element.get_attribute(Constants.ATTR_TYPE) == Constants.ATTR_TEXT:
                                value = value
                                for strV in value:
                                    element.send_keys(strV)
                                
                                res = RESULT(
                                    Constants.RESULT_OK,
                                    Constants.EMPTY_STRING,
                                    Constants.EMPTY_OBJECT,
                                    Constants.EMPTY_STRING,
                                    Constants.EMPTY_STRING,
                                    Constants.EMPTY_STRING
                                )
                            elif element.get_attribute(Constants.ATTR_TYPE) == Constants.ATTR_CHECKBOX:
                                if value == Constants.ON:
                                    valueConvert = Constants.TRUE
                                elif value == Constants.OFF:
                                    valueConvert = Constants.FALSE
                                else:
                                    valueConvert = valueStr
                                
                                if element.get_attribute(Constants.ATTR_CHECKED) == valueConvert.lower():
                                    res = RESULT(
                                        Constants.RESULT_OK,
                                        Constants.ATTR_CHECKED,
                                        Constants.EMPTY_OBJECT,
                                        Constants.EMPTY_STRING,
                                        Constants.EMPTY_STRING,
                                        Constants.EMPTY_STRING
                                    )
                                else:
                                    element.click()
                                    res = RESULT(
                                        Constants.RESULT_OK,
                                        Constants.EMPTY_STRING,
                                        Constants.EMPTY_OBJECT,
                                        Constants.EMPTY_STRING,
                                        Constants.EMPTY_STRING,
                                        Constants.EMPTY_STRING
                                    )
                            else:
                                res = RESULT(
                                    Constants.RESULT_NOK,
                                    Constants.MSG_INVALID_CONTROL,
                                    Constants.EMPTY_OBJECT,
                                    Constants.EMPTY_STRING,
                                    Constants.EMPTY_STRING,
                                    Constants.EMPTY_STRING
                                )
                        elif element.tag_name == Constants.ATTR_SELECT:
                            sl = Select(element)
                            sl.select_by_visible_text(value)
                            res = RESULT(
                                Constants.RESULT_OK,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_OBJECT,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING
                            )
                        elif element.tag_name == Constants.ATTR_TEXTAREA:
                            element.send_keys(value)
                            res = RESULT(
                                Constants.RESULT_OK,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_OBJECT,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING
                            )
                        else:
                            res = RESULT(
                                Constants.RESULT_NOK,
                                Constants.MSG_ACTION_NOT_SUPPORT,
                                Constants.EMPTY_OBJECT,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING
                            )
                else:
                    if elements.tag_name == Constants.ATTR_INPUT:
                        if elements.get_attribute(Constants.ATTR_TYPE) == Constants.ATTR_TEXT:
                            value = value
                            for strV in value:
                                elements.send_keys(strV)
                            
                            res = RESULT(
                                Constants.RESULT_OK,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_OBJECT,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING
                            )
                        elif elements.get_attribute(Constants.ATTR_TYPE) == Constants.ATTR_CHECKBOX:
                            if value == Constants.ON:
                                valueConvert = Constants.TRUE
                            elif value == Constants.OFF:
                                valueConvert = Constants.FALSE
                            else:
                                valueConvert = valueStr
                            
                            if elements.get_attribute(Constants.ATTR_CHECKED) == valueConvert.lower():
                                res = RESULT(
                                    Constants.RESULT_OK,
                                    Constants.ATTR_CHECKED,
                                    Constants.EMPTY_OBJECT,
                                    Constants.EMPTY_STRING,
                                    Constants.EMPTY_STRING,
                                    Constants.EMPTY_STRING
                                )
                            else:
                                elements.click()
                                res = RESULT(
                                    Constants.RESULT_OK,
                                    Constants.EMPTY_STRING,
                                    Constants.EMPTY_OBJECT,
                                    Constants.EMPTY_STRING,
                                    Constants.EMPTY_STRING,
                                    Constants.EMPTY_STRING
                                )
                        else:
                            res = RESULT(
                                Constants.RESULT_NOK,
                                Constants.MSG_INVALID_CONTROL,
                                Constants.EMPTY_OBJECT,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING
                            )
                    elif elements.tag_name == Constants.ATTR_SELECT:
                        sl = Select(elements)
                        sl.select_by_visible_text(value)
                        res = RESULT(
                            Constants.RESULT_OK,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_OBJECT,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING
                        )
                    elif elements.tag_name == Constants.ATTR_TEXTAREA:
                        elements.send_keys(value)
                        res = RESULT(
                            Constants.RESULT_OK,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_OBJECT,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING
                        )
                    else:
                        res = RESULT(
                            Constants.RESULT_NOK,
                            Constants.MSG_ACTION_NOT_SUPPORT,
                            Constants.EMPTY_OBJECT,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING
                        )
                #write log file
                itemScreen = str("Screen" + Constants.COLON + screen)
                listParamsShow1.append(itemScreen)
                itemControl = str("Control" + Constants.COLON + control)
                listParamsShow1.append(itemControl)
                itemIndex = str("Index" + Constants.COLON + str(index))
                listParamsShow1.append(itemIndex)
                itemValue = str("Value" + Constants.COLON + value)
                listParamsShow1.append(itemValue)
                
        except Exception as ex:
            print (ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_SetJavascriptVariable:
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        try:
            variableName, value = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
        
            result = LocatorHelper.get_value_from_variable(value, listVariable)
            
            jsonText = Constants.WINDOW_DOT + variableName + Constants.SPACE_EQUAL_SPACE_QUOTATIONMARKS + result + Constants.QUOTATIONMARKS_SEMICOLON
            driver.execute_script(jsonText)
            #write log file
            itemvariableName = str(tAction.params[0].name + Constants.COLON + tAction.params[0].value)
            listParamsShow1.append(itemvariableName)
            itemValue = str(tAction.params[1].name + Constants.COLON + result)
            listParamsShow1.append(itemValue)
            res = RESULT(
                Constants.RESULT_OK,
                jsonText,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        except Exception as ex:
            print("Error action Set Javascript Variable")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_SetRangeValue:
    def execute(self,executeFilePath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        
        try:
            tAction = executeItem.testAction
            # fileXl = tAction.params[0].value
            # sheetPage = tAction.params[1].value
            # row = tAction.params[2].value
            # column = tAction.params[3].value
            # result = tAction.params[4].value
            fileXl, sheetPage, row, column, result = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            row = LocatorHelper.get_value_from_variable(row, listVariable)
            column = LocatorHelper.get_value_from_variable(column, listVariable)
            fileXlsx = LocatorHelper.get_value_from_variable(fileXl , listVariable)
            sheetPage = LocatorHelper.get_value_from_variable(sheetPage , listVariable)
            result = LocatorHelper.get_value_from_variable(result , listVariable)
            nameFileStr = os.path.basename(fileXlsx).split(Constants.DOT)[1]
            if nameFileStr == Constants.XLSX:
                book = openpyxl.load_workbook(fileXlsx)
                sheet = book[sheetPage]
                rowV = int(row)
                columnV = int(column)
                booleanStr = ""
                
                if "\n" in result:
                    result = result.replace("\n","")
                
                try:
                    result = ast.literal_eval(result)
                    booleanStr = Constants.TRUE
                except Exception as e:
                    booleanStr = Constants.FALSE
                if booleanStr == Constants.TRUE:
                    for i in result:
                        if type(i) is not list:
                            sheet.cell(row=rowV, column=columnV).value = i
                            columnV += 1
                        else:
                            columnVs = columnV
                            for j in i:
                                sheet.cell(row=rowV, column=columnVs).value = j
                                columnVs += 1
                            rowV += 1
                else:
                    sheet.cell(row=rowV, column=columnV).value = result
                book.save(fileXlsx)
            else:
                book = open_workbook(fileXlsx)
                sheet = book.sheet_by_name(sheetPage)
                rb = open_workbook(fileXlsx)
                wb = copy(rb)
                sheet = wb.get_sheet(sheetPage)
                rowV = int(row)
                columnV = int(column)
                
                for i in result:
                    if type(i) is not list:
                        sheet.write(rowV, columnV, i)
                        columnV += 1
                    else:
                        columnVs = columnV
                        for j in i:
                            sheet.write(rowV, columnVs, j)
                            columnVs += 1
                        rowV += 1
                wb.save(fileXlsx)
            #write log file
            itemFileName = str(tAction.params[0].name + Constants.COLON + fileXlsx)
            listParamsShow1.append(itemFileName)
            itemSheet = str(tAction.params[1].name + Constants.COLON + tAction.params[1].value)
            listParamsShow1.append(itemSheet)
            itemRow = str(tAction.params[2].name + Constants.COLON + str(row))
            listParamsShow1.append(itemRow)
            itemColumn = str(tAction.params[3].name + Constants.COLON + str(column))
            listParamsShow1.append(itemColumn)
            itemResult = str(tAction.params[4].name + Constants.COLON + str(result))
            listParamsShow1.append(itemResult)
            
            res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        except Exception as ex:
            print("Error action set range value")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_SetTextFile:
    def import_class_from_string(self, moduleName, className):
        from importlib import import_module
        mod = import_module(moduleName + Constants.DOT + className)
        klass = getattr(mod, className)
        return klass
    def execute(self, projectPath, driver, executeItem):
        tAction = executeItem.testAction
        fileName = tAction.params[0].value
        text = tAction.params[1].value
        typeFile = Constants.NEW
        try:
            if typeFile == Constants.NEW:
                if os.path.isfile(fileName):
                    res = RESULT(
                        Constants.RESULT_NOK,
                        Constants.FILE_NAME_IS_EXIST,
                        Constants.EMPTY_OBJECT,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
                else:
                    path, file_name = os.path.split(fileName)
                    arrFolder = path.split(Constants.SLASH)
                    acCheckFolderExist = eval( Constants.AC_CHECK_FOLDER_EXIST)
                    newPath = Constants.EMPTY_STRING
                    acCreateFolder = eval( Constants.AC_CREATE_FOLDER)
                    for key in arrFolder:
                        newPath = newPath + key
                        if arrFolder.index(key) == 0 and os.path.exists(newPath) == False:
                            res = RESULT(
                                Constants.RESULT_NOK,
                                Constants.MSG_FOLDER_IS_EXIST,
                                Constants.EMPTY_OBJECT,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING
                            )
                            return res
                        elif os.path.exists(newPath) == False:
                            os.makedirs(newPath)
                        newPath = newPath + Constants.SLASH
                    if os.path.exists(path):
                        f = open(fileName, Constants.FILE_W)
                        f.write(text)
                        f.close()
                        res = RESULT(
                            Constants.RESULT_OK,
                            Constants.MSG_SET_TEXT_FILE_SUCCESS,
                            Constants.EMPTY_OBJECT,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING
                        )
            else:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
        except Exception as e:
            print (e)
            res = RESULT(
                Constants.RESULT_CRASH,
                e,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_SetVariable:
    def execute(self, xmlPath, driver, executeItem, lstVariables, resourceFolder, listParamsShow1, browserName):
        try:
            tAction = executeItem.testAction
            nameStr, variableType, valueStr = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            check = Constants.EMPTY_STRING
            valueStr = LocatorHelper.get_value_from_variable(valueStr, lstVariables)
            
            value = LocatorHelper.set_value_from_variable(nameStr, valueStr, lstVariables)
           
            itemName = str(tAction.params[0].name + Constants.COLON + tAction.params[0].value)
            listParamsShow1.append(itemName)
            itemVariableType = str(tAction.params[1].name + Constants.COLON + tAction.params[1].value)
            listParamsShow1.append(itemVariableType)
            itemValue = str(tAction.params[2].name + Constants.COLON + str(value))
            listParamsShow1.append(itemValue)
            res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,
                value,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        except Exception as ex:
            print("Error action set variable")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_Sleep:
    def execute(self, projectPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        try:
            timeSleep = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
        
            value = Constants.EMPTY_STRING
            if driver is None:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                value = LocatorHelper.get_value_from_variable(timeSleep, listVariable)
                value = float(value)
                if value >= 0:
                    time.sleep(value / 1000.0)
                    res = RESULT(
                        Constants.RESULT_OK,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
                    
                else:
                    res = RESULT(
                        Constants.RESULT_NOK,
                        "Please input number > 0.",
                        Constants.EMPTY_OBJECT,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
                
                #write log file
                itemTimeSleep = str(tAction.params[0].name + Constants.COLON + str(value))
                listParamsShow1.append(itemTimeSleep)
        except Exception as e:
            print("Error action sleep")
            print(e)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(e),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_SwitchTabPage:
    def execute(self, projectPath, driver, executeItem):
        try:
            tAction = executeItem.testAction
            if driver is None:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                index = tAction.params[0].value
                tab = driver.window_handles
                index = int(index)
                if math.isnan(index):
                    res = RESULT(
                        Constants.RESULT_NOK,
                        Constants.MSG_INDEX_IS_NOT_A_NUMBER,
                        Constants.EMPTY_OBJECT,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
                else:
                    if len(tab) > index:
                        if index == 0:
                            driver.switch_to_window(tab[0])
                            res = RESULT(
                                Constants.RESULT_OK,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_OBJECT,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING
                            )
                        else:
                            newIndex = len(tab) - index
                            driver.switch_to_window(tab[newIndex])
                            res = RESULT(
                                Constants.RESULT_OK,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_OBJECT,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING,
                                Constants.EMPTY_STRING
                            )
                    else:
                        res = RESULT(
                            Constants.RESULT_NOK,
                            Constants.MSG_TAB_LENGTH_IS_SMALLER,
                            Constants.EMPTY_OBJECT,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING
                        )
        except Exception as ex:
            print (ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                ex,
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_TakeFullScreen(object):
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        try:
            if tAction.id in listVariable.keys():
                imageIdex = listVariable[tAction.id] + 1
            else:
                imageIdex = 1
            imageNameF = str(imageIdex)+"_" + tAction.id+ Constants.EXT_PNG
            imagePath = os.path.join(listVariable["testResult"], imageNameF)
            if driver is None:
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.MSG_SELECT_BROWSER, 
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                try:
                    total_width = driver.execute_script("return document.body.offsetWidth")
                    total_height = driver.execute_script("return document.body.parentNode.scrollHeight")
                    viewport_width = driver.execute_script("return document.body.clientWidth")
                    viewport_height = driver.execute_script("return window.innerHeight")
                    
                    el = driver.find_element_by_tag_name('body')
                    location = el.location
                    size = el.size
                    x = location['x']
                    y = location['y']
                    w = size['width']
                    h = size['height']
                    width = x + w
                    height = y + h
                    if height < total_height:
                        device_pixel_ratio = driver.execute_script('return window.devicePixelRatio')
                        total_height = driver.execute_script('return document.body.parentNode.scrollHeight')
                        viewport_height = driver.execute_script('return window.innerHeight')
                        total_width = driver.execute_script('return document.body.offsetWidth')
                        viewport_width = driver.execute_script("return document.body.clientWidth")
                        # this implementation assume (viewport_width == total_width)
                        assert(viewport_width == total_width)
                        # scroll the page, take screenshots and save screenshots to slices
                        offset = 0  # height
                        slices = {}
                        while offset < total_height:
                            if offset + viewport_height > total_height:
                                offset = total_height - viewport_height
                            driver.execute_script('window.scrollTo({0}, {1})'.format(0, offset))
                            time.sleep(5)
                            img = Image.open(BytesIO(driver.get_screenshot_as_png()))
                            slices[offset] = img
                            offset = offset + viewport_height
                        # combine image slices
                        stitched_image = Image.new('RGB', (total_width * device_pixel_ratio, total_height * device_pixel_ratio))
                        for offset, image in slices.items():
                            stitched_image.paste(image, (0, offset * device_pixel_ratio))
                        stitched_image.save(imagePath)
                    else:
                        el.screenshot(imagePath)
                    listVariable[tAction.id] = imageIdex
                except Exception as ex:
                    print(ex)
                
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.EMPTY_STRING,
                    imageNameF,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
                
        except Exception as ex:
            print("Error action take full screen")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_TakeScreenshot():
    def execute(self,executeFilePath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
    
        try:
            tAction = executeItem.testAction
            if tAction.id in listVariable.keys():
                imageIdex = listVariable[tAction.id] + 1
            else:
                imageIdex = 1
            imageNameF = str(imageIdex)+"_" + tAction.id+ Constants.EXT_PNG
            imagePath = os.path.join(listVariable["testResult"], imageNameF)
            listVariable[tAction.id] = imageIdex
            #snapshot.save(imagePath)
            driver.save_screenshot(imagePath)
            res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,
                imageNameF,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        except Exception as ex:
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_TakeScreenshotWithResult():
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        try:
            imageNameF = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
        
            if tAction.id in listVariable.keys():
                imageIdex = listVariable[tAction.id] + 1
            else:
                imageIdex = 1
            getValue = LocatorHelper.get_value_from_variable(imageNameF, listVariable)
            
            if "testData" not in imageNameF and "testResult" not in imageNameF:
                listData = getValue.split("/")
                nameFolder = listData[-2]
                if nameFolder == "TestDatas":
                    imageNameF = "$testData/" + listData[-1]
            
            imagePath = LocatorHelper.get_value_from_variable(getValue, listVariable)
            
            # imagePath = os.path.join(listVariable["#testResult"], imageNameSuccess)
            listVariable[tAction.id] = imageIdex
           
            driver.save_screenshot(imagePath)
            res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,
                imageNameF,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
            #write log file
            itemName = str(tAction.params[0].name + Constants.COLON + imagePath)
            listParamsShow1.append(itemName)
        except Exception as ex:
            print("Error action takescreenshot with result")
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_Type():
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        try:
            typeStr = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            if driver is None:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                if Constants.SHARP in typeStr:
                    value = LocatorHelper.get_value_from_variable(typeStr, listVariable)
                    pyautogui.typewrite(value)
                elif Constants.SQUARR_BRACKETS_LEFT not in typeStr:
                    pyautogui.typewrite(typeStr)
                else:
                    typeStr = typeStr.replace(Constants.SPACE, Constants.EMPTY_STRING).lower()
                    if len(typeStr.split(Constants.PLUS)) > 1:
                        if len(typeStr.split(Constants.PLUS)) == 2 and Constants.SQUARR_BRACKETS_LEFT not in typeStr.split(Constants.PLUS)[1]:
                            keyA = typeStr.split(Constants.PLUS)[0]
                            indexA1 = keyA.index(Constants.SQUARR_BRACKETS_LEFT)
                            indexA2 = keyA.index(Constants.SQUARR_BRACKETS_RIGHT)
                            indexA = keyA[indexA1+1:indexA2]
                            keyB = typeStr.split(Constants.PLUS)[1]
                            pyautogui.keyDown(indexA)
                            pyautogui.keyDown(keyB)
                            pyautogui.keyUp(keyB)
                            pyautogui.keyUp(indexA)
                        elif len(typeStr.split(Constants.PLUS)) == 2 and Constants.SQUARR_BRACKETS_LEFT in typeStr.split(Constants.PLUS)[1]:
                            keyA = typeStr.split(Constants.PLUS)[0]
                            indexA1 = keyA.index(Constants.SQUARR_BRACKETS_LEFT)
                            indexA2 = keyA.index(Constants.SQUARR_BRACKETS_RIGHT)
                            indexA = keyA[indexA1+1:indexA2]
                            keyB = typeStr.split(Constants.PLUS)[1]
                            indexB1 = keyB.index(Constants.SQUARR_BRACKETS_LEFT)
                            indexB2 = keyB.index(Constants.SQUARR_BRACKETS_RIGHT)
                            indexB = keyB[indexB1+1:indexB2]
                            pyautogui.keyDown(indexA)
                            pyautogui.keyDown(indexB)
                            pyautogui.keyUp(indexB)
                            pyautogui.keyUp(indexA)
                        elif len(typeStr.split(Constants.PLUS)) == 3 and Constants.SQUARR_BRACKETS_LEFT in typeStr.split(Constants.PLUS)[2]:
                            keyA = typeStr.split(Constants.PLUS)[0]
                            indexA1 = keyA.index(Constants.SQUARR_BRACKETS_LEFT)
                            indexA2 = keyA.index(Constants.SQUARR_BRACKETS_RIGHT)
                            indexA = keyA[indexA1+1:indexA2]
                            keyB = typeStr.split(Constants.PLUS)[1]
                            indexB1 = keyB.index(Constants.SQUARR_BRACKETS_LEFT)
                            indexB2 = keyB.index(Constants.SQUARR_BRACKETS_RIGHT)
                            indexB = keyB[indexB1+1:indexB2]
                            keyC = typeStr.split(Constants.PLUS)[2]
                            indexC1 = keyC.index(Constants.SQUARR_BRACKETS_LEFT)
                            indexC2 = keyC.index(Constants.SQUARR_BRACKETS_RIGHT)
                            indexC = keyC[indexC1+1:indexC2]
                            pyautogui.keyDown(indexA)
                            pyautogui.keyDown(indexB)
                            pyautogui.keyDown(indexC)
                            pyautogui.keyUp(indexC)
                            pyautogui.keyUp(indexB)
                            pyautogui.keyUp(indexA)
                        elif len(typeStr.split(Constants.PLUS)) == 3 and Constants.SQUARR_BRACKETS_LEFT not in typeStr.split(Constants.PLUS)[2]:
                            keyA = typeStr.split(Constants.PLUS)[0]
                            indexA1 = keyA.index(Constants.SQUARR_BRACKETS_LEFT)
                            indexA2 = keyA.index(Constants.SQUARR_BRACKETS_RIGHT)
                            indexA = keyA[indexA1+1:indexA2]
                            keyB = typeStr.split(Constants.PLUS)[1]
                            indexB1 = keyB.index(Constants.SQUARR_BRACKETS_LEFT)
                            indexB2 = keyB.index(Constants.SQUARR_BRACKETS_RIGHT)
                            indexB = keyB[indexB1+1:indexB2]
                            keyC = typeStr.split(Constants.PLUS)[2]
                            pyautogui.keyDown(indexA)
                            pyautogui.keyDown(indexB)
                            pyautogui.keyDown(keyC)
                            pyautogui.keyUp(keyC)
                            pyautogui.keyUp(indexB)
                            pyautogui.keyUp(indexA)
                    else:
                        index1 = typeStr.index(Constants.SQUARR_BRACKETS_LEFT)
                        index2 = typeStr.index(Constants.SQUARR_BRACKETS_RIGHT)
                        index = typeStr[index1+1:index2]
                        pyautogui.keyDown(index)
                        pyautogui.keyUp(index)
                #write log file
                itemType = str(tAction.params[0].name + Constants.COLON + typeStr)
                listParamsShow1.append(itemType)
                res = RESULT(
                    Constants.RESULT_OK,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
        except Exception as ex:
            print("Error Action Type")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_UploadFile():
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder,  listParamsShow1, browserName):
        tAction = executeItem.testAction
        try:
            screen, control, index, pathStr = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            if index == None:
                index = 0
            if driver is None:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                pathStr = LocatorHelper.get_value_from_variable(pathStr, listVariable)
                sendPath = ""
                for i in pathStr.split("/"):
                    sendPath += os.path.join("\\", i)
                elements = LocatorHelper.get_element_by_locator2(driver, xmlPath, screen, executeItem, control, resourceFolder, listVariable, index)
                
                if elements != None:
                    for element in elements:
                        #th?c hi?n click ? v? tri ???c ch? ??nh
                        element.send_keys(sendPath)
                        time.sleep(3)
                    #write log file
                    itemScreen = str(tAction.params[0].name + Constants.COLON + tAction.params[0].value)
                    listParamsShow1.append(itemScreen)
                    itemControl = str(tAction.params[1].name + Constants.COLON + tAction.params[1].value)
                    listParamsShow1.append(itemControl)
                    itemIndex = str("Index" + Constants.COLON + str(index))
                    listParamsShow1.append(itemIndex)
                    itemFileName = str("Result" + Constants.COLON + sendPath)
                    listParamsShow1.append(itemFileName)
                    res = RESULT(
                        Constants.RESULT_OK,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_OBJECT,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
                else:
                    res = RESULT(
                        Constants.RESULT_NOK,
                        Constants.MSG_CONTROL_NOT_EXIST,
                        Constants.EMPTY_OBJECT,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
        except Exception as ex:
            print("Error action Upload file")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_UploadFTPFile():
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        
        try:
            connectionName, remotePath, localPathStr = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            if driver is None:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                connectionValue = listVariable[connectionName]
                hostFtp = connectionValue["Host"]
                portFtp = connectionValue["Port"]
                usernameFtp = connectionValue["Username"]
                passwordFtp = connectionValue["Password"]
                ftp = ftplib.FTP()
                ftp.connect(hostFtp, int(portFtp))
                
                ftp.login(usernameFtp, passwordFtp)
                data = []
                ftp.dir(data.append)
                remotePath = LocatorHelper.get_value_from_variable(remotePath, listVariable)
                localPath = LocatorHelper.get_value_from_variable(localPathStr, listVariable)
                pathProject = os.getcwd()
                
                message = Constants.BLANK
                # file location
                ftp.cwd(remotePath)
                readFileToBinary = open(localPath, 'rb')
                getNameFile = localPath.split(Constants.SLASH)[-1:][0]
                localSplit = localPath.split(Constants.SLASH)
                localSplit.pop(-1)
                pathJoin = Constants.BLANK
                for i in localSplit:
                    pathJoin = pathJoin + i + Constants.SLASH
                os.chdir(pathJoin)
                fh = open(getNameFile, 'rb')
                try:
                    ftp.storbinary('STOR '+ getNameFile +'', fh)
                    message = "Upload successful"
                except Exception as directory_error:
                    print(directory_error)
                    message = "Oops! Was " + getNameFile + " a directory? I won't upload directories."
                os.chdir(pathProject)
                ftp.quit()
                #Write log file
                itemConnectionName = str(tAction.params[0].name + Constants.COLON + tAction.params[0].value)
                listParamsShow1.append(itemConnectionName)
                itemRemotePath = str(tAction.params[1].name + Constants.COLON + remotePath)
                listParamsShow1.append(itemRemotePath)
                itemLocalPath = str(tAction.params[2].name + Constants.COLON + localPath)
                listParamsShow1.append(itemLocalPath)
                res = RESULT(
                    Constants.RESULT_OK,
                    message,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
        except Exception as ex:
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_UseBrowser:
    def load_driverconfig_file(self,xmlPath):
        DOMTree = xml.dom.minidom.parse(xmlPath)
        tsEntity = DOMTree.getElementsByTagName(Constants.TAG_CHROMEVERSION)[0]
        return tsEntity
    
    def load_driverFirefoxconfig_file(self,xmlPath):
        DOMTree = xml.dom.minidom.parse(xmlPath)
        tsEntity = DOMTree.getElementsByTagName("FirefoxVersion")[0]
        return tsEntity
    def execute(self, browserName, device, ServerIP, ServerPort,environment, mode, isRecordVideo, listParamsShow1):
        
        desired_caps = {}
        try:
            if environment.environmentOS.strip().lower() == Constants.ANDROID.strip().lower():
                if browserName.strip().lower() == Constants.CHROME.strip().lower():
                    desired_caps = {
                        "platformName": Constants.ANDROID,
                        "deviceName": device.deviceName,
                        "browserName": Constants.CHROME,
                        "chromeOptions": {
                        "args": ['disable-popup-blocking']
                        }
                    }
                    driver = webdriver.Remote(ServerIP+':'+ServerPort+'/wd/hub', desired_caps)
                    res = RESULT(
                        Constants.RESULT_OK,
                        Constants.EMPTY_STRING,
                        driver,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
                else:
                    res = RESULT(
                        Constants.RESULT_NOK,
                        Constants.MSG_BROWSER_NOT_SUPPORT_YET,
                        Constants.EMPTY_OBJECT,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
            elif environment.environmentOS.strip().lower() == Constants.MAC.strip().lower():
                if browserName.strip().lower() == Constants.CHROME.strip().lower():
                    PROJECT_ROOT = os.getcwd()
                    DRIVER_BIN = os.path.join(PROJECT_ROOT, "bin/mac/chromedriver/chromedriver") 
                    print (DRIVER_BIN)
                    chrome_options = Options()
                    chrome_options.add_argument(Constants.CHROME_OPTION_MAXSIZE)
                    chrome_options.add_argument("--kiosk")
                    chrome_options.add_argument("--disable-web-security")
                    chrome_options.add_argument(Constants.CHROME_OPTION_FULLSCREEN)
                    driver = webdriver.Chrome(executable_path = DRIVER_BIN, port=0, chrome_options=chrome_options)
                    WebDriverWait(driver, 100)
                    res = RESULT(
                        Constants.RESULT_OK,
                        Constants.EMPTY_STRING,
                        driver,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
                elif browserName.strip().lower() == 'safari':
                    driver = webdriver.Safari(executable_path = '/usr/bin/safaridriver')
                    driver.maximize_window()
                    res = RESULT(
                        Constants.RESULT_OK,
                        Constants.EMPTY_STRING,
                        driver,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
                else:
                    res = RESULT(
                        Constants.RESULT_NOK,
                        Constants.MSG_BROWSER_NOT_SUPPORT_YET,
                        Constants.EMPTY_OBJECT,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
            elif environment.environmentOS.strip().lower() == Constants.WINDOW.strip().lower():
                if browserName.strip().lower() == Constants.CHROME.strip().lower():
                    import winreg
                    hKey = winreg.OpenKey(winreg.HKEY_CURRENT_USER, "Software\Google\Chrome\BLBeacon")
                    version = winreg.QueryValueEx(hKey, 'version')
                    versionChrome = str(version[0].split(Constants.DOT)[0])     
                    PROJECT_ROOT = os.getcwd()
                    
                    # read driver config file xml
                    configPath = os.path.join(PROJECT_ROOT, Constants.DRIVERCONFIG)
                    chVersion = self.load_driverconfig_file(configPath)
                    #read versions
                    driverVS=None
                    chromeVS=None
                    lsVersions = chVersion.getElementsByTagName(Constants.TAG_VERSION)
                    for vs in lsVersions:
                        chromeDef = vs.getElementsByTagName(Constants.TAG_CHROME)[0]
                        chromeVS = chromeDef.childNodes[0].data
                        if chromeVS == versionChrome:
                            driverDef = vs.getElementsByTagName(Constants.TAG_DRIVER)[0]
                            driverVS = driverDef.childNodes[0].data
                            break
                    if driverVS !=None:
                        DRIVER_BIN = os.path.join(PROJECT_ROOT, "bin/window/chromedriver",driverVS,Constants.CHROMEDRIVER) 
                        chrome_options = Options()
                        if mode.modeType == Constants.DESKTOP:
                            chrome_options.add_argument(Constants.CHROME_OPTION_FULLSCREEN)
                            chrome_options.add_argument(Constants.CHROME_OPTION_DISABLE_INFOBARS)
                            # visual display
                            # if isRecordVideo != Constants.TRUE:
                            #     chrome_options.add_argument("--headless")
                        elif mode.modeType == Constants.MOBILE:
                            sizeScreen = device.deviceResolution
                            chrome_options.add_argument("--window-size="+ sizeScreen +"")
                            chrome_options.add_argument(Constants.CHROME_OPTION_DISABLE_INFOBARS)
                            chrome_options.add_argument("--disable-notifications")
                            chrome_options.add_argument("--disable-web-security")
                        driver = webdriver.Chrome(executable_path = DRIVER_BIN, port=0, chrome_options=chrome_options)
                        itemValue = str("value" + Constants.COLON + browserName.strip().lower())
                        listParamsShow1.append(itemValue)
                        # driver.set_window_position(0,0)
                        WebDriverWait(driver, 100)
                        res = RESULT(
                            Constants.RESULT_OK,
                            Constants.EMPTY_STRING,
                            driver,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING
                        )
                    else:
                        res = RESULT(
                            Constants.RESULT_OK,
                            "driver not found!",
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING
                        )
                elif browserName.strip().lower() == Constants.IE:
                    PROJECT_ROOT = os.getcwd()
                    DRIVER_BIN = os.path.join(PROJECT_ROOT,"bin/window/iedriver",Constants.IEDRIVER)
                    driver = webdriver.Ie(DRIVER_BIN)
                    if mode.modeType == Constants.DESKTOP:
                        driver.maximize_window()
                    elif mode.modeType == Constants.MOBILE:
                        sizeScreen = device.deviceResolution
                        withSize = int(sizeScreen.split(",")[0])
                        heightSize = int(sizeScreen.split(",")[1])
                        driver.set_window_position(20,10)
                        driver.set_window_size(withSize,heightSize)
                    res = RESULT(
                        Constants.RESULT_OK,
                        Constants.EMPTY_STRING,
                        driver,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
                elif browserName.strip().lower() == "edge":
                    PROJECT_ROOT = os.getcwd()
                    if environment.environmentName.lower() == "window10":
                        DRIVER_BIN = os.path.join(PROJECT_ROOT,"C:/Windows/System32",Constants.MICROSOFTWEBDRIVER)
                        driver = webdriver.Edge(DRIVER_BIN)
                        if mode.modeType == Constants.DESKTOP:
                            driver.maximize_window()
                        elif mode.modeType == Constants.MOBILE:
                            sizeScreen = device.deviceResolution
                            withSize = int(sizeScreen.split(",")[0])
                            heightSize = int(sizeScreen.split(",")[1])
                            driver.set_window_position(20,10)
                            driver.set_window_size(withSize,heightSize)
                        res = RESULT(
                            Constants.RESULT_OK,
                            Constants.EMPTY_STRING,
                            driver,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING
                        )
                    else:
                        res = RESULT(
                            Constants.RESULT_OK,
                            "Window 7 not support microsoft edge!!!",
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING
                        )
                elif browserName.strip().lower() == Constants.FIREFOX:
                    import win32api
                    info = win32api.GetFileVersionInfo("C:/Program Files/Mozilla Firefox/firefox.exe", "\\")
                    ms = info['ProductVersionMS']
                    ls = info['ProductVersionLS']
                    versionFireFox = win32api.HIWORD(ms)
                    PROJECT_ROOT = os.getcwd()
                    configPath = os.path.join(PROJECT_ROOT, "DriverConfigFirefox.xml")
                    firefoxVersion = self.load_driverFirefoxconfig_file(configPath)
                    driverVS=None
                    firefoxVS=None
                    lsVersions = firefoxVersion.getElementsByTagName(Constants.TAG_VERSION)
                    for vs in lsVersions:
                        firefoxDef = vs.getElementsByTagName("Firefox")[0]
                        firefoxVS = firefoxDef.childNodes[0].data
                        if int(firefoxVS) == versionFireFox:
                            driverDef = vs.getElementsByTagName(Constants.TAG_DRIVER)[0]
                            driverVS = driverDef.childNodes[0].data
                            break
                    if driverVS !=None:
                        DRIVER_BIN = os.path.join(PROJECT_ROOT,"bin/window/firefoxdriver",driverVS,Constants.FIREFOXDRIVER)
                        if isRecordVideo != Constants.TRUE:
                            from selenium.webdriver.firefox.options import Options as FirefoxOptions
                            options = FirefoxOptions()
                            options.add_argument("--headless")
                            driver = webdriver.Firefox(executable_path = DRIVER_BIN, options=options)
                        else:
                            driver = webdriver.Firefox(executable_path = DRIVER_BIN)
                        if mode.modeType == Constants.DESKTOP:
                            driver.maximize_window()
                        elif mode.modeType == Constants.MOBILE:
                            sizeScreen = device.deviceResolution
                            withSize = int(sizeScreen.split(",")[0])
                            heightSize = int(sizeScreen.split(",")[1])
                            driver.set_window_position(20,10)
                            driver.set_window_size(withSize,heightSize)
                        res = RESULT(
                            Constants.RESULT_OK,
                            Constants.EMPTY_STRING,
                            driver,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING
                        )
                    else:
                        res = RESULT(
                            Constants.RESULT_OK,
                            "driver not found!",
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING,
                            Constants.EMPTY_STRING
                        )
                else:
                    res = RESULT(
                        Constants.RESULT_NOK,
                        Constants.MSG_BROWSER_NOT_SUPPORT_YET,
                        Constants.EMPTY_OBJECT,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
            else:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_DEVICE_NOT_SUPPORT_YET,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
        except Exception as e:
            print("Error action use browser")
            print (e)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(e),
                Constants.EMPTY_OBJECT,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class AC_While:
    """description of class"""
    #so sánh 2 giá trị truyền vào với op là đúng hay sai
    def compare(self,arg1, op, arg2):
        ops = {
            Constants.LESS_THANS: operator.lt,
            Constants.LESS_THAN_OR_EQUALS: operator.le,
            Constants.EQUAL_EQUAL: operator.eq,
            Constants.NOT_EQUAL: operator.ne,
            Constants.MORE_THAN_OR_EQUAL: operator.ge,
            Constants.MORE_THAN: operator.gt
        }
        operation = ops.get(op)
        return operation(arg1,arg2)
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, indexExecute, listScript, indexLoop):
        tAction = executeItem.testAction
        try:
            listParamsShow = []
            prCondition = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            booleanCheck = LocatorHelper.get_value_from_variable(prCondition, listVariable)
            
            #write log file
            itemCondition = str("Condition" + Constants.COLON + prCondition)
            listParamsShow1.append(itemCondition)
            if booleanCheck == True:
                index = indexExecute + 1
            else:
                countWhile = 1
                index = indexExecute + 1
                while (countWhile > 0) and (index < len(listScript)):
                    if (listScript[index] == Constants.AC_WHILE):
                        countWhile = countWhile + 1
                    elif (listScript[index] == Constants.AC_ENDWHILE):
                        countWhile = countWhile -1
                    index = index + 1
            
            res = RESULT(
                Constants.RESULT_OK,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                index
            )  
            
        except Exception as ex:
            print("Error action While")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                "Error, please check the format or anything!!!",
                Constants.EMPTY_OBJECT,
                Constants.MSG_CONTROL_IS_EXIST,
                Constants.MSG_CONTROL_NOT_EXIST,
                Constants.EMPTY_STRING
            )
        return res


class AC_WriteToCSV():
    def execute(self, xmlPath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName):
        tAction = executeItem.testAction
        
        try:
            data, fileName, appendStr = LocatorHelper.getValueFromParam(tAction.params, tAction.id)
            value = []
            booleanStr = Constants.BLANK
            getValue = LocatorHelper.get_value_from_variable(fileName, listVariable)
            
            if "testData" not in fileName and "testResult" not in fileName:
                listData = getValue.split("/")
                nameFolder = listData[-2]
                if nameFolder == "TestDatas":
                    fileName = "$testData/" + listData[-1]
            
            csvPath = LocatorHelper.get_value_from_variable(getValue, listVariable)
            data = LocatorHelper.get_value_from_variable(data, listVariable)
            
            if driver is None:
                res = RESULT(
                    Constants.RESULT_NOK,
                    Constants.MSG_SELECT_BROWSER,
                    Constants.EMPTY_OBJECT,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING,
                    Constants.EMPTY_STRING
                )
            else:
                if fileName.split(Constants.DOT)[1] == Constants.CSV:
                    listValue = Constants.BLANK
                    
                    checkStr = Constants.BLANK
                    try:
                        data = ast.literal_eval(data)
                        booleanStr = Constants.TRUE
                    except Exception as e:
                        booleanStr = Constants.FALSE
                    if booleanStr == Constants.TRUE:
                        if len(data) > 0:
                            checkStr = isinstance(data[0], list)
                            for j in data:
                                if str(checkStr) == Constants.TRUESTR:
                                    varst = Constants.BLANK
                                    for k in j:
                                        varst += str(k) + Constants.COMMAS
                                    listValue += varst + Constants.NEWLINE
                                else:
                                    listValue += str(j) + Constants.COMMAS
                        else:
                            listValue = Constants.BLANK
                    else:
                        listValue = data + Constants.COMMAS
                    if str(appendStr).lower() == Constants.TRUE:
                        with codecs.open(csvPath, "a+", encoding='utf-8-sig') as text_file:
                            text_file.write(listValue + Constants.NEWLINE)
                    else:
                        with codecs.open(csvPath, "a+", encoding='utf-8-sig') as text_file:
                            text_file.write(Constants.BLANK)
                    
                    res = RESULT(
                        Constants.RESULT_OK,
                        Constants.EMPTY_STRING,
                        fileName,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
                else:
                    res = RESULT(
                        Constants.RESULT_OK,
                        "File not support!",
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING,
                        Constants.EMPTY_STRING
                    )
            #write log file
            itemData = str(tAction.params[0].name + Constants.COLON + tAction.params[0].value)
            listParamsShow1.append(itemData)
            itemFileName = str(tAction.params[1].name + Constants.COLON + csvPath)
            listParamsShow1.append(itemFileName)
            itemAppend = str(tAction.params[2].name + Constants.COLON + appendStr)
            listParamsShow1.append(itemAppend)
        except Exception as ex:
            print("Error action write to csv")
            print(ex)
            res = RESULT(
                Constants.RESULT_CRASH,
                str(ex),
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING,
                Constants.EMPTY_STRING
            )
        return res


class Constants():
    # LIST ACTIONS #
    AC_USE_BROWSER = "AC_UseBrowser"
    AC_OPEN = "AC_Open"
    AC_TAKESCREENSHOT = "AC_Takescreenshot"
    AC_CAPTURESCREEN = "AC_CaptureScreen"
    AC_DOESCONTROLEXIST = "AC_DoesControlExist"
    AC_CHECK_CONTROL_EXIST = "AC_CheckControlExist"
    AC_CHECK_FILE_EXIST = "AC_CheckFileExist"
    AC_CHECK_FOLDER_EXIST = "AC_CheckFolderExist"
    AC_CREATE_FOLDER = "AC_CreateFolder"
    AC_DOES_CONTROL_EXIST = "AC_DoesControlExist"
    AC_GET_CONTROL_VALUE = "AC_GetControlValue"
    AC_CHECK_ALERT_MESSAGE = "AC_CheckAlertMessage"
    AC_CHECK_CONTROL_ATTRIBUTE ='AC_CheckControlAttribute'
    AC_SET_VARIABLE = "AC_SetVariable"
    AC_IF = "AC_If"
    AC_ENDIF = "AC_EndIf"
    AC_LOOP = "AC_Loop"
    AC_ENDLOOP = "AC_EndLoop"
    AC_GETRANGEVALUE = "get range value"
    AC_GETCELLVALUE = "get cell value"
    AC_GETCOLUMN = "get column"
    AC_GETROW = "get row"
    AC_GETCSVFILE = "get csv file"
    AC_GETCELLCOLOR = "get cell color"
    AC_CHECKCELLCOLOR = "check cell color"
    AC_CHECKVALUE = "check value"
    AC_GET = "get"
    AC_GETCONTROLVALUE = "get control value"
    AC_GETJAVASCRIPTVARIABLE = "get javascript variable"
    AC_CONNECTDATABASE= "connect database"
    AC_EXECUTEQUERY = "execute query"
    AC_CHECKCONTROLVALUE = "AC_CheckControlValue"
    AC_CHECKCONTROLATTRIBUTE = "AC_CheckControlAttribute"
    AC_COMPAREIMAGE = "AC_CompareImage"
    AC_TAKEFULLSCREEN = "AC_TakeFullScreen"
    AC_TAKE_FULL_SCREEN = "take full screen"
    AC_CHECK_CONTROL_VALUE = "check control value"
    AC_COMPAREIMAGE = "AC_CompareImage"
    AC_COMPARE_IMAGE = "compare image"
    AC_GET_ATTRIBUTE = "AC_GetAttribute"
    AC_CONNECT_FTP = "AC_ConnectFTP"
    AC_WHILE = "AC_While"
    AC_ENDWHILE = "AC_EndWhile"
    # COMMONS #
    ANDROID = "android"
    MAC = "macOS"
    WINDOW = "windowOS"
    CHROME = "chrome"
    IE = "ie"
    FIREFOX = "firefox"
    ACTION_MODULE = "Actions"
    TEST_SUITES = "TestSuites"
    LAYOUTS = "Layouts"
    REPORTS = "Reports"
    RESULT = "Result"
    EVIDENCE = "Evidence"
    EMPTY_STRING = ""
    EMPTY_OBJECT = {}
    NEW = "new"
    APPEND = "append"
    BACKUP = "backup"
    FORMAT_TIME = "%Y%m%d%H%M%S"
    FORMAT_DATETIME = "%Y-%m-%d_%H:%M:%S"
    CONFIG = 'Config'
    OK = 'OK'
    NG = 'NG'
    PASSED = 'Passed'
    FAILED = 'Failed'
    ERROR = 'Error'
    NEWLINE = "\n"
    FILE_R = "r" 
    FILE_W = "w+"
    PATH_LIB = 'Lib'
    EMPTY = 'empty'
    RGBA = "rgba"
    VIDEO = "Video"
    HEX = "hex"
    XLS = "xls"
    XLSX = "xlsx"
    CSV = "csv"
    FROM = 'From'
    TO = 'To'
    SUBJECT = 'Subject'
    # NUMBER
    NO_0 = '0'
    # EXTENSIONS FILE TYPES#    
    EXT_XML = ".xml"
    EXT_PNG = ".png"
    EXT_JSON = ".json"
    EXT_AVI = ".avi"
    EXT_MP4 = '.mp4'
    EXT_TXT = ".txt"
    EXT_WEBM = ".webm"
    
    # BOOLEAN #
    TRUE = "true"
    FALSE = "false"
    ON = "on"
    OFF = "off"
    ERROR = "error"
    TRUESTR = "True"
    # CHARACTER
    CHAR_W = 'w'
    # SPECIAL-CHARACTER
    DOT = "."
    SLASH = "/"
    UNDERSCORE = "_"
    DOUBLE_SLASH = "//"
    DOUBLE_BACKSLASH = "\\"
    BLANK = ""
    SEPARATOR = "_"
    UTF_8 = 'utf-8'
    DOUBLE_SPACE = '  '
    COLON = ":"
    SPACE = " "
    SHARP = "#"
    DOLLAR = "$"
    OPEN_PARENTHESIS = "("
    CLOSE_PARENTHESIS = ")"
    SPACE_EQUAL_SPACE = " = "
    SQUARR_BRACKETS_RIGHT = "]"
    SQUARR_BRACKETS_LEFT = "["
    COMMA = ','
    COMMAS = ","
    ASTERISK = "*"
    HYPHEN = "-"
    PLUS = "+"
    # MESSAGES #
    MSG_AC_NOT_SUPPORT_YET = "This action is not supported yet."
    MSG_BROWSER_NOT_SUPPORT_YET = "This Browser is not supported yet."
    MSG_DEVICE_NOT_SUPPORT_YET = "This Device is not supported yet."
    MSG_SELECT_BROWSER = "Please select Browser before running."
    MSG_SELECT_FILE = "File exist."
    MSG_SELECT_NFILE = "File  not  exist."
    MSG_SELECT_REMOVE = "File is delete."
    MSG_SELECT_NREMOVE = "File is not delete."
    MSG_MOVE_FILE = "Move file success."
   
    MSG_STRING_IS_NOT_NUMBER = "Please input number > 0."
    MSG_FILE_IS_EXIST = "File is exist."
    MSG_FILE_IS_NOT_EXIST = "File is not exist"
    MSG_FOLDER_IS_EXIST = "Folder is exist."
    MSG_FOLDER_IS_NOT_EXIST = "Folder is not exist."
    MSG_SET_TEXT_FILE_SUCCESS = "Set text file success."
    MSG_CREATER_FOLDER = "Folder has been created."
    MSG_ERROR = "Error!"
    MSG_LAYOUT_FILE_NOT_EXIST = "Layout file not exist!"
    MSG_CONTROL_NOT_EXIST = "Control not exist!"
    MSG_CONTROL_IS_EXIST = "Control is exist."
    MSG_TEXT_IS_FOUND = "Text is found."
    MSG_TEXT_IS_NOT_FOUND = "Text is not found."
    MSG_MAPPING = "Value is mapping."
    MSG_NOT_MAPPING = "Value is not mapping."
    MSG_INVALID_CONTROL = "Invalid control."
    MSG_ATTRIBUTE_NOT_EXIST = "Input attribute."
    MSG_TAB_LENGTH_IS_SMALLER = "Current tab is smaller the index"
    FILE_NAME_IS_EXIT = "File name exist. Please choose another file name."
    MSG_TESTSUITE_EMPTY = 'Testsuite Name Empty!'
    MSG_ACTION_NOT_SUPPORT = "This action does not support for this control"
    MSG_QUERY_DATABASE_SUCCESS = "Successful"
    MSG_QUERY_DATABASE_FAILED = "Failed"
    RESULT_OK = "1"
    RESULT_NOK = "2"
    RESULT_CRASH = "3"
    RESULT_PASSED = "4"
    RESULT_FAILED = "5"
    RESULT_STR_PASS = "PASS"
    RESULT_STR_FAILED = "FAILED"
    RESULT_STR_ABORT = "ABORT"
    # TAGXML #
    TAG_TESTSUITE_MODEL = "TestsuiteModel"
    TAG_LAYOUT_MODEL = "LayoutModel"
    TAG_REPORT_MODEL = "ReportModel"
    TAG_CONTROL = "Control"
    TAG_CONTROLS = "Controls"
    TAG_CLASSNAME = "ClassName"
    TAG_NAME = "Name"
    TAG_VALUE = "Value"
    TAG_PARAM = "Param"
    TAG_TESTCASE = "Testcases"
    TAG_ID = "Id"
    TAG_ACTION = "Action"
    TAG_DEVICE = "Device"
    TAG_TYPE = "Type"
    TAG_SERVER = "Server"
    TAG_IP = "IP"
    TAG_PORT = "Port"
    TAG_TESTSUITE = "Testsuite"
    TAG_TESTSUITES = "Testsuites"
    TAG_LANGUAGE= "Language"
    TAG_EN = "en"
    TAG_CLASS = "Class"
    TAG_DEFINITION = "Definition"
    TAG_ROW = "Row"
    TAG_PROJECT = "Project"
    TAG_CONTENT = "Content"
    TAG_INFOR = "Infor"
    TAG_CHROMEVERSION = "ChromeVersion"
    TAG_VERSION = "Version"
    TAG_CHROME = "Chrome"
    TAG_DRIVER = "Driver"
    TAG_DEVICENAME = "DeviceName"
    TAG_DEVICETYPE = "DeviceType"
    TAG_STARTTIME = "StartTime"
    TAG_ENDTIME = "EndTime"
    TAG_RUNBY = "RunBy"
    TAG_RUNMACHINE = "RunMachine"
    TAG_DESCRIPTION = "Description"
    TAG_ROWOBJECTIVES = "RowObjectives"
    TAG_ROWINITIAL = "RowInitial"
    TAG_ROWFINAL = "RowFinal"
    TAG_OBJECTIVE = "Objective"
    TAG_OBJECTIVES = "Objectives"
    TAG_INITIAL = "Initial"
    TAG_ACTIONS = "Actions"
    TAG_TESTCASES = "Testcases"
    TAG_TITLE = "Title"
    TAG_PARAMS = "Params"
    TAG_STATUS = "Status"
    TAG_EXPECTED = "Expected"
    TAG_RECORDED = "Recorded"
    TAG_NOTE = "Note"
    TAG_RESULT = "Result"
    TAG_VARIABLES = "Variables"
    TAG_VARIABLE = "Variable"
    TAG_TOTAL = "Total"
    TAG_PASSED = "Passed"
    TAG_FAILED = "Failed"
    TAG_ERROR = "Error"
    TAG_OK = "OK"
    TAG_NG = "NG"
    TAG_TESTSCRIPTS = "TestScripts"
    TAG_ACTIONNAME = "ActionName"
    TAG_RESULT_CODE = "ResultCode"
    TAG_RESULT_MSG = "ResultMsg"
    TAG_RESULT_EXPECTED = "ResultExpected"
    TAG_RESULT_RECORDED = "ResultRecorded"
    TAG_RESULT_OBJECT = "ResultObj"
    TAG_RUNNING_USER = "RunningUser"
    TAG_TEST_RESULT = "TestResult"
    TAG_CONDITION = "Condition"
    TAG_ATTRIBUTES = "Attributes"
    TAG_LOG = "Log"
    TAG_RESULT_NEXTSTEP = "ResultNextStep"
    
    VALUE_XPATH = "xpath"
    VALUE_ID = "id"
    VALUE_NAME = "name"
    VALUE_None = "None"
    VALUE_CLASS = "class"
    VALUE_TEXT = "textContent"
    VALUE_HREF = "href"
    VALUE_TAGNAME = "type"
    VALUE_CSS = "css"
    # FILE #
    EXECUTECONFIG_FILE = "ExecuteConfig"
    CHROMEDRIVER = "chromedriver.exe"
    DRIVERCONFIG = "DriverConfig.xml"
    IEDRIVER = "IEDriverServer.exe"
    FIREFOXDRIVER = "geckodriver.exe"
    MICROSOFTWEBDRIVER = "MicrosoftWebDriver.exe"
    # ATTRIBUTE #
    ATTR_VALUE = "value"
    ATTR_PLACEHOLDER = "placeholder"
    ATTR_TYPE = "type"
    ATTR_CHECKBOX = "checkbox"
    ATTR_RADIO = "radio"
    ATTR_TAGNAME = "tagName"
    ATTR_INPUT = "input"
    ATTR_TEXT = "text"
    ATTR_PASSWORD = "password"
    ATTR_TEXTAREA = "textarea"
    ATTR_COLOR = "color"
    ATTR_BACKGROUNDCOLOR = "background-color" 
    ATTR_CHECKED = "checked"
    ATTR_SELECT = "select"
    ATTR_SUBMIT = "submit"
    ATTR_A = "a"
    ATTR_LABEL = "label"
    ATTR_SPAN = "span"
    ATTR_BUTTON = "button"
    ATTR_DIV = "div"
    ATTR_TITLE = "title"
    ATTR_H = [
        "h1",
        "h2",
        "h3",
        "h4",
        "h5",
        "h6"
    ]
    ATTR_MULTIPLE = "multiple"
    
    # SELENIUM #
    CHROME_OPTION_MAXSIZE = "--start-maximized"
    CHROME_OPTION_FULLSCREEN = "--start-fullscreen"
    CHROME_OPTION_DISABLE_INFOBARS = "disable-infobars"
    # LANGUAGE #
    LANG_EN = "en"
    #Folder Path
    # FD_ACTION = "C:/Users/Administrator/Desktop/BotTest/04_Execute/Actions/"
    FD_ACTION = "./Actions/"
    # FD_RESOURCE = "C:/Users/Administrator/Desktop/BotTest_Resources/"
    FD_RESOURCE = "Config/Result/"
    FD_VIDEO = "http://13.115.37.177:9999/bottest/bottest_resources/"
    
    
    
    SLASH_RESULT_SLASH = "/Results/"
    RESULT_UNDERSCORE = "Result_"
    SLASH_TESTDATA_SLASH = "/TestDatas/"
    TESTDATA_SLASH = "TestDatas/"
    SLASH_RESULT = "/Results"
    PERCENT_INT = "%20"
    SPACE_STEP_SPACE = " step "
    SPACE_START_SPACE = " Start "
    SPACE_END_SPACE = " End "
    SPACE_EXECUTE_SPACE = " Execute "
    SPACE_START_LOOP_WITH_SPACE = " Start Loop with "
    SPACE_END_LOOP_WITH_SPACE = " End Loop with "
    ACTION_SPACE_SQUARR = "Action ["
    SQUARR_SPACE = "] "
    SLASH_TESTDATA = "/testData"
    SLASH_TESTRESULT = "/testResult"
    SPACE_START_WHILE_WITH_SPACE = " Start While with "
    SPACE_END_WHILE_WITH_SPACE = " End While with "
    
    SLASH_EXECUTE_UNDERSCORE = "/Execute_"
    A_SQUARR_BRACKETS_LEFT_HREF = "a[href='"
    HREF_SQUARR_BRACKETS_RIGHT = "']"
    WINDOW_DOT = 'window.'
    SPACE_EQUAL_SPACE_QUOTATIONMARKS =' = "'
    QUOTATIONMARKS_SEMICOLON = '";'
    RETURN_SPACE = "return "
    SEMICOLON = ";"
    SHARP_IMAGECOUNT_TAKEFULLSCREEN = "#imageCountTakeFullScreen"
    SHARP_IMAGECOUNT_CHECKCONTROLVALUE = "#imageCountCheckControlValue"
    SHARP_IMAGECOUNT_CHECKCONTROLATTRIBUTE = "#imageCountCheckControlAttribute"
    SHARP_IMAGECOUNT_COMPAREIMAGE = "#imageCountCompareImage"
    
    #OPERATORS
    EQUAL_EQUAL = "=="
    MORE_THAN_OR_EQUAL = ">="
    MORE_THAN = ">"
    LESS_THANS = "<"
    LESS_THAN_OR_EQUALS = "<="
    NOT_EQUAL = "!="
    #KEYWORD
    KWD_LEN = "len"
    #ALPHABET
    LIST_ALPHABET_TABLE = [
        {'A':0},
        {'B':1},
        {'C':2},
        {'D':3},
        {'E':4},
        {'F':5},
        {'G':6},
        {'H':7},
        {'I':8},
        {'J':9},
        {'K':10},
        {'L':11},
        {'M':12},
        {'N':13},
        {'O':14},
        {'P':15},
        {'Q':16},
        {'R':17},
        {'S':18},
        {'T':19},
        {'U':20},
        {'V':21},
        {'W':22},
        {'X':23},
        {'Y':24},
        {'Z':25}
    ]
    #ALPHABETICAL Hiragana and Katakana
    ALPHABETICAL_HIRAGANA = "あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやゆよらりるれろわをん"
    ALPHABETICAL_KATAKANA = "アイウエオカキクケコサシスセソタチツテトナニヌネノハヒフヘホマミムメモヤユヨラリルレロワヲンガギグゲゴザジズゼゾダヂヅデドバビブベボパピプペポキャキュキョシャシュショチャチュチョニャニュニョヒャヒュヒョミャミュミョリャリュリョギャギュギョジャジュジョヂャヂュヂョビャビュビョピャピュピョ"
    #Database
    MYSQL = "mysql"
    POSTGRESQL = "postgresql"
    SQLSERVER = "sqlserver"
    ORACLE = "oracle"
    #Mode Type
    DESKTOP = "desktop"
    MOBILE = "mobile"
    #TYPE Data
    STRING = "string"
    INT = "int"
    DATE = "date"
    THEAD = 'thead'
    TBODY = 'tbody'
    TR = 'tr'
    TD = 'td'
    TH = 'TH'
    RANDOM = 'random'
    TEXT = 'text'
    NOW = 'now'
    HIRAGANA = 'hiragana'
    KATAKANA = 'katakana'
    CHARFULLSIZE = "charfullsize"
    HALFSIZEKATAKANA = "halfsizekatakana"
    REMOVE = "remove"
    REPLACE = "replace"
    LEN = "len"
    DYNAMIC = "BOT_LAYOUT_DYNAMIC"
    #OPERATOR STRING
    EQUAL = "equal"
    GREATER_THAN = "greater than"
    LESS_THAN = "less than"
    GREATER_THAN_OR_EQUAL = "greater than or equal"
    LESS_THAN_OR_EQUAL = "less than or equal"
    CONTAIN = "contain"
    INCONTAIN = "incontain"
    WILDCARD = "wildcard"
    #Query
    SELECT = "select"
    UPDATE = "update"
    DELETE = "delete"
    INSERT = "insert"
    WHERE = "where"
    SPACE_WHERE_SPACE = " where "
    SELECT_ASTERISK_FROM = "select * from "
    SINGLE_QUOTATION = "'"
    QUERY_GET_COLUMN_NAME = "SELECT top 1 COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '"
    QUERY_GET_COLUMN_NAME_BY_MYSQL_AND_POSTGRESQL = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '"
    SELECT_TOP_ASTERISK_FROM = "select top 1 * from "
    SPACE_ORDER_BY_SPACE = " order by "
    SPACE_DESC = " desc"
    def __setattr__(self, *_):
        pass


class Device:
    deviceResolution = ""
    deviceName = ""
    def __init__(self,deviceResolution, deviceName):
        self.deviceName = deviceName
        self.deviceResolution = deviceResolution


class Environment:
    environmentOS = ""
    environmentName = ""
    environmentResolution = ""
    environmentLocation = ""
    def __init__(self, environmentOS, environmentName,environmentResolution, environmentLocation):
        self.environmentOS = environmentOS
        self.environmentName = environmentName
        self.environmentResolution = environmentResolution
        self.environmentLocation = environmentLocation
        


class ExecuteAction:
    
    testSuiteName = ""
    testCaseName = ""
    testAction = {}
    # def __init__(self, testSuiteName, testCaseId, testAction):
    #     self.testSuiteName = testSuiteName
    #     self.testCaseId = testCaseId
    #     self.testAction = testAction
    def __init__(self, testSuiteName, testCaseName, testAction):
        self.testSuiteName = testSuiteName
        self.testCaseName = testCaseName
        self.testAction = testAction


class ExecuteEntity:
    listExecute = []
    executeFilePath = ""
    device = {}
    serverIP = ""
    serverPort = ""
    environment = {}
    mode = {}
    def __init__(self, listExecute, executeFilePath, device, serverIP, serverPort, environment, mode):
        self.listExecute = listExecute
        self.executeFilePath = executeFilePath
        self.device = device
        self.serverIP = serverIP
        self.serverPort = serverPort
        self.environment = environment
        self.mode = mode


class ExecuteTestsuiteEntity:
    projectPath = ""
    testSuitePath = ""
    testcases = []
    def __init__(self, projectPath, testSuitePath, testcases):
        self.projectPath = projectPath
        self.testSuitePath = testSuitePath
        self.testcases = testcases
        


class Mode:
    modeName = ""
    modeType = ""
    def __init__(self, modeName, modeType):
        self.modeName = modeName
        self.modeType = modeType


class Param:
    name = ""
    value = ""
    def __init__(self, name, value):
        self.name = name
        self.value = value
    
        


class RESULT:
    # 1:pass or 2:fail, 3: exception
    resultCode = "" 
    # store the message if fail or exception
    resultMsg = ""
    # store the element value or image path if the action have take screenshot or data file name
    resultObj = {}
    resultExpected = ""
    resultRecorded = "" 
    resultNextStep = 0 
    def __init__(self, resultCode, resultMsg, resultObj, resultExpected, resultRecorded, resultNextStep):
        self.resultCode = resultCode
        self.resultMsg = resultMsg
        self.resultObj = resultObj
        self.resultExpected = resultExpected
        self.resultRecorded = resultRecorded
        self.resultNextStep = resultNextStep


class TestAction:
    id = ""
    name = ""
    params = []
    def __init__(self, id, name, params):
        self.id = id
        self.name = name
        self.params = params


class TestsuiteEntity:
    testSuiteName = ""
    testCaseId = ""
    def __init__(self, testSuiteName, testCaseId):
        self.testSuiteName = testSuiteName
        self.testCaseId = testCaseId
        


class ExecuteHandler():
    driver = None
    # dùng để ghi kết quả dữ liệu vào file result
    def write_result_data(self, position, resultFilePath, res, actionName, runningUser, testSuiteName, imageName, videoName, runningDate, isRecordVideo, indexTab, indexExecute):
        data = json.load(codecs.open(resultFilePath, 'r', 'utf-8-sig'))
        
        testResult = data[Constants.TAG_TEST_RESULT]
        
        if testResult == {}:
            testResult[Constants.RESULT] = Constants.RESULT_STR_PASS
            if isRecordVideo == Constants.TRUE:
                testResult[Constants.VIDEO] = videoName
            else:
                testResult[Constants.VIDEO] = ""
        lsTestcases = data[Constants.TAG_TESTCASES]
        
        tc = lsTestcases[indexTab]
        lsTestscripts = tc[Constants.TAG_TESTSCRIPTS]
        tc[Constants.RESULT] =  Constants.RESULT_STR_PASS
        sumList = 0
        if indexTab > 0:
            for i in range(indexTab):
                sumList += len(lsTestcases[i][Constants.TAG_TESTSCRIPTS])
        if indexExecute >= sumList:
            indexList = indexExecute - sumList
        else:
            indexList = indexExecute
        ts = lsTestscripts[indexList]
        tsResult = ts[Constants.TAG_RESULT]
        tsResult[Constants.TAG_RESULT_CODE] = res.resultCode
        if actionName == Constants.AC_TAKESCREENSHOT or actionName == Constants.AC_TAKE_FULL_SCREEN:
            tsResult[Constants.TAG_RESULT_MSG] = imageName
        else:
            tsResult[Constants.TAG_RESULT_MSG] = res.resultMsg
        
        tsResult[Constants.TAG_RESULT_OBJECT] = str(res.resultObj)
        tsResult[Constants.TAG_RESULT_EXPECTED] = res.resultExpected
        tsResult[Constants.TAG_RESULT_RECORDED] = res.resultRecorded
        tsResult[Constants.TAG_RESULT_NEXTSTEP] = res.resultNextStep
        
        if res.resultCode != "1":
            testResult[Constants.RESULT] = Constants.RESULT_STR_FAILED
            tc[Constants.RESULT] = Constants.RESULT_STR_FAILED
        with open(resultFilePath, "w") as jsonFile:
            json.dump(data, jsonFile)
            time.sleep(0.5)
    def write_result_testcase(self, position, resultFilePath, res, actionName, runningUser, testSuiteName, imageName, videoName):
        data = json.load(codecs.open(resultFilePath, 'r', 'utf-8-sig'))
        lsTestcases = data[Constants.TAG_TESTCASES]
        for tc in lsTestcases:
            lsTestscripts = tc[Constants.TAG_TESTSCRIPTS]
            listResultCode = []
            
            for ts in lsTestscripts:
                resultObj = ts[Constants.TAG_RESULT]
                if Constants.TAG_RESULT_CODE in resultObj.keys():
                    listResultCode.append(ts[Constants.TAG_RESULT][Constants.TAG_RESULT_CODE])
            if "2" in listResultCode or "3" in listResultCode:
                tc[Constants.RESULT] = Constants.RESULT_STR_FAILED
            with open(resultFilePath, "w") as jsonFile:
                json.dump(data, jsonFile)
    # function get number of image name
    def loop_index_images(self, listVariable, nameImage, countImg):
        if nameImage in listVariable.keys():
            countImg = listVariable[nameImage]
        return countImg
    
    def create_testResult_folder(self, resultPath, runningUser, runningDate, listVariable):
        localPath = resultPath + Constants.SLASH + Constants.RESULT_UNDERSCORE + runningUser + Constants.UNDERSCORE + runningDate
        if not os.path.exists(localPath):
            os.mkdir(localPath)
        listVariable["testResult"] =  localPath
        return localPath
    
    def build_testData_folder_path(self, resultPath, listVariable):
        testDataPath = resultPath.replace("Results","TestDatas")
        listVariable["testData"] = testDataPath
        return testDataPath
    #thực thi 
    def execute(self, executeEntity, resultPath, runningUser, runningDate, timeOut, resourceFolder, isRecordVideo, listScript):    
        global driver       
        global element
        driver = None     
        element = None
        
        listVariable = {}
        localPath = self.create_testResult_folder(resultPath, runningUser, runningDate, listVariable)
        
        testDataPath = self.build_testData_folder_path(resultPath, listVariable)
        resultFilePath = localPath + Constants.EXT_JSON
        logFilePath = os.path.join(localPath,Constants.TAG_LOG + Constants.UNDERSCORE + runningUser + Constants.UNDERSCORE + runningDate + Constants.EXT_TXT)
        listExecute = executeEntity.listExecute
        
        # start record screen
        videoName = Constants.TAG_RESULT + Constants.UNDERSCORE + runningUser + Constants.UNDERSCORE + runningDate + Constants.EXT_WEBM
        videoPath = os.path.join(localPath, videoName)
        screenResolution = executeEntity.device.deviceResolution
        if screenResolution == Constants.EMPTY_STRING:
            img = ImageGrab.grab()
            width = img.width - 20
            height =img.height - 20
        else:
            img = ImageGrab.grab()
            width = img.width - 20
            height =img.height - 20
        left = 0
        top = 0
        if isRecordVideo == Constants.TRUE:
            fourcc = cv2.VideoWriter_fourcc('V','P','8','0')
            vid = cv2.VideoWriter(videoPath, fourcc, 1.0, (width,height))
        count = 0
        
        #clone Excute file to Result file
        copyfile(executeEntity.executeFilePath, resultFilePath)        
        listStringText = []
       
        indexExecute = 0
        # indexLoop = 0
        indexTab = 0
        browserName = Constants.BLANK
        while indexExecute < len(listExecute):
            executeItem = listExecute[indexExecute]
            count = count + 1
            imageNameF = Constants.EMPTY_STRING
            tAction = executeItem.testAction
            tsName = executeItem.testSuiteName
            tcName = executeItem.testCaseName
            
            res = None 
            if driver != None:
                if isRecordVideo == Constants.TRUE:
                    img = ImageGrab.grab(bbox=(left, top, width, height))
                    img_np = np.array(img)
                    frame = cv2.cvtColor(img_np, cv2.COLOR_BGR2RGB)
                    vid.write(frame)
                    key = cv2.waitKey(1)
            if tAction.id == Constants.AC_USE_BROWSER: 
                acUseBrowser = AC_UseBrowser()
                listParamsShow1 = []
                browserName = tAction.params[0].value
                res = acUseBrowser.execute(browserName, executeEntity.device, None, None, executeEntity.environment, executeEntity.mode, isRecordVideo, listParamsShow1)
                if res.resultCode == Constants.RESULT_OK:
                    driver = res.resultObj
                    self.write_result_data(count, resultFilePath, res, tAction.id, runningUser, tsName, imageNameF, videoName, runningDate, isRecordVideo, indexTab, indexExecute)
                    indexExecute = indexExecute + 1
            elif tAction.id == Constants.AC_IF or tAction.id == Constants.AC_WHILE or tAction.id == Constants.AC_ENDWHILE or tAction.id == Constants.AC_LOOP or tAction.id == Constants.AC_ENDLOOP:
                if tAction.id == Constants.AC_LOOP:
                    indexLoop = 0
                actionClass = eval( tAction.id)
                listParamsShow1 = []
                res = actionClass().execute(executeEntity.executeFilePath, driver, executeItem, listVariable,
                                            resourceFolder, listParamsShow1, indexExecute, listScript, indexLoop)
                self.write_result_data(count, resultFilePath, res, tAction.id, runningUser, tsName, imageNameF, videoName, runningDate, isRecordVideo, indexTab, indexExecute)
                indexExecute = res.resultNextStep
                if tAction.id == Constants.AC_LOOP:
                    indexLoop = res.resultMsg
            else:
                className = tAction.id
                actionClass = eval( className)
                listParamsShow1 = []
                res = actionClass().execute(executeEntity.executeFilePath, driver, executeItem, listVariable, resourceFolder, listParamsShow1, browserName)
                self.write_result_data(count, resultFilePath, res, tAction.id, runningUser, tsName, imageNameF, videoName, runningDate, isRecordVideo, indexTab, indexExecute)
                indexExecute = indexExecute + 1
            if res != None:
                stringText = Constants.EMPTY_STRING
                if res.resultCode == Constants.RESULT_OK or res.resultCode == Constants.RESULT_PASSED or res.resultCode == Constants.RESULT_NOK:
                    
                    print(strftime("%Y%m%d %H%M%S", gmtime())+ Constants.SPACE_STEP_SPACE + str(count)  + Constants.SPACE_EXECUTE_SPACE + Constants.ACTION_SPACE_SQUARR + tAction.id + Constants.SQUARR_SPACE + str(listParamsShow1))
                    stringText = strftime("%Y%m%d %H%M%S", gmtime())+ Constants.SPACE_STEP_SPACE + str(count)  + Constants.SPACE_EXECUTE_SPACE + Constants.ACTION_SPACE_SQUARR + tAction.id + Constants.SQUARR_SPACE + str(listParamsShow1)
                    
                else:
                    print(strftime("%Y%m%d %H%M%S", gmtime())+ Constants.SPACE_STEP_SPACE + str(count)  + Constants.SPACE_EXECUTE_SPACE + Constants.ACTION_SPACE_SQUARR + tAction.id + Constants.SQUARR_SPACE + str(listParamsShow1))
                    stringText = strftime("%Y%m%d %H%M%S", gmtime())+ Constants.SPACE_STEP_SPACE + str(count) + Constants.SPACE_EXECUTE_SPACE + Constants.ACTION_SPACE_SQUARR + tAction.id + Constants.SQUARR_SPACE + str(listParamsShow1)
                    # if driver!=None:  
                    driver.quit()
                    # break
                
                if tAction.id == Constants.AC_LOOP:
                    listStringText.append(res.resultObj)
                elif tAction.id == Constants.AC_WHILE:
                    listStringText.append(res.resultObj)
                listStringText.append(stringText)
                timeSleep = timeOut
                if timeSleep.isdigit():
                    timeSleep = float(timeSleep)
                    # time.sleep(timeSleep / 1000.0)
                    time.sleep(0.1)
            key = count
            if indexExecute == len(listExecute):
                if isRecordVideo == Constants.TRUE:
                    img = ImageGrab.grab(bbox=(left, top, width, height))
                    img_np = np.array(img)
                    frame = cv2.cvtColor(img_np, cv2.COLOR_BGR2RGB)
                    vid.write(frame)
                    key = cv2.waitKey(1)
                if listExecute[indexExecute-1].testAction.id != "AC_CloseBrowser":
                    if driver != None:
                        driver.close()
                break
            
            if listExecute[indexExecute].testAction.id == Constants.AC_USE_BROWSER:
                indexTab += 1
                if listExecute[indexExecute-1].testAction.id != "AC_CloseBrowser":
                    driver.close()
         
        self.write_result_testcase(count, resultFilePath, res, tAction.id, runningUser, tsName, imageNameF, videoName)    
        #mở file Log và ghi kết quả chạy vào
        with open(logFilePath, "w", encoding='utf-8-sig') as text_file:
            valueStr = Constants.EMPTY_STRING
            for x in listStringText:
                if type(x) is list:
                    for i in x:
                        valueStr = i
                        text_file.write(valueStr + Constants.NEWLINE)
                else:
                    valueStr = x
                    text_file.write(valueStr + Constants.NEWLINE)
        if isRecordVideo == Constants.TRUE:
            vid.release()
            cv2.destroyAllWindows()


def load_testcase_file(xmlPath):
    DOMTree = xml.dom.minidom.parse(xmlPath)
    tsEntity = DOMTree.getElementsByTagName(Constants.TAG_TESTSUITE_MODEL)[0]
    return tsEntity


def load_testsuite_file(jsonPath):
    # with open(jsonPath) as f:
    #     tsEntityJson = json.load(f)
    tsEntityJson = json.load(codecs.open(jsonPath, 'r', 'utf-8-sig'))
    # tsEntityJson = json.loads(open(jsonPath).decode('utf-8-sig'))
    return tsEntityJson


def get_device_info(executeEntity):
    defDevice = executeEntity.getElementsByTagName(Constants.TAG_DEVICE)[0]
    defName = defDevice.getElementsByTagName(Constants.TAG_NAME)[0]
    defType = defDevice.getElementsByTagName(Constants.TAG_TYPE)[0]
    deviceName = defName.childNodes[0].data
    deviceType = defType.childNodes[0].data
    return deviceName, deviceType


def get_device_info_json(executeEntity):
    deviceResolution = executeEntity[Constants.TAG_DEVICE]['Resolution']
    deviceName = executeEntity[Constants.TAG_DEVICE]['DeviceName']
    # deviceType = executeEntity[Constants.TAG_DEVICE][Constants.TAG_TYPE]
    return deviceResolution,deviceName


def get_environment_info_json(executeEntity):
    environmentOS = executeEntity['Environment']['OS']
    environmentName = executeEntity['Environment']['Name']
    environmentResolution = executeEntity['Environment']['Resolution']
    environmentLocation = executeEntity['Environment']['Location']
    return environmentOS, environmentName, environmentResolution, environmentLocation


def get_mode_info_json(executeEntity):
    modeName = executeEntity['Mode']['Name']
    modeType = executeEntity['Mode']['Type']
    return modeName, modeType


def runExecute(args):
    try:
        runningUser = args[1]  #user3
        runningDate = args[2].replace("~","_")  #20181220_111708
        testsuiteName = args[3]   #Demo
        testsuiteName = testsuiteName.replace("~",Constants.SPACE)
        timeOut = args[4]   #300
        resourceFolder = args[5] #Config/Result/
        isRecordVideo = args[6]
        resultPath = resourceFolder + Constants.SLASH + testsuiteName + Constants.SLASH_RESULT
        jsonPath = resultPath + Constants.SLASH_EXECUTE_UNDERSCORE + runningUser + Constants.UNDERSCORE + runningDate + Constants.EXT_JSON
        
        tsModel = load_testsuite_file(jsonPath)
        tsName = tsModel[Constants.TAG_TESTSUITE]
        # deviceName, deviceType = get_device_info_json(tsModel)
        deviceResolution, deviceName = get_device_info_json(tsModel)
        # device = Device(deviceName,deviceType)
        device = Device(deviceResolution,deviceName)
        #Environment
        environmentOS, environmentName, environmentResolution, environmentLocation = get_environment_info_json(tsModel)
        environment = Environment(environmentOS,environmentName,environmentResolution, environmentLocation)
        #Mode
        modeName, modeType = get_mode_info_json(tsModel)
        mode = Mode(modeName, modeType)
        lsTestCases = tsModel[Constants.TAG_TESTCASE]
        lsExAction = []
        listScript= []
        for tc in lsTestCases:
            tcNamme = tc[Constants.TAG_NAME]
            #tcDescription = tc[Constants.TAG_DESCRIPTION]
            lsTestScripts = tc[Constants.TAG_TESTSCRIPTS]
            for ac in lsTestScripts:
                acId = ac[Constants.TAG_CLASSNAME]
                acName = ac[Constants.TAG_LANGUAGE][Constants.LANG_EN][Constants.TAG_ACTIONNAME]
                
                try:
                   acParams = ac[Constants.TAG_PARAMS]
                except:
                    acParams= None
                lsParams = []
                #check action param
                if acParams!=None and len(acParams) > 0:
                    for pr in acParams:
                        if pr != None:
                            prName = pr[Constants.TAG_NAME]
                            prValue = pr[Constants.TAG_VALUE]
                            prItem = Param(prName, prValue)
                            lsParams.append(prItem)
                testAction = TestAction(acId, acName, lsParams)
                exAction = ExecuteAction(tsName, tcNamme, testAction)
                listScript.append(testAction.id)
                lsExAction.append(exAction)
        
        serverIP  = tsModel["Server"]["Ip"] #"http://127.0.0.1"
        serverPORT = tsModel["Server"]["Port"]      #4723"
        exEntity = ExecuteEntity(lsExAction, jsonPath, device, serverIP, str(serverPORT), environment, mode)
        exHandler = ExecuteHandler()
        exHandler.execute(exEntity, resultPath, runningUser, runningDate, timeOut, resourceFolder, isRecordVideo, listScript)
    except Exception as ex:
        print ('Error: ' + str(ex))
        


class ActionList:
    @staticmethod
    def list_Actions():
        listActions = '''[
            {
                "data": {
                    "id": 2,
                    "name": "open Url",
                    "action_type_id": 3,
                    "created_by": "System",
                    "updated_by": "System",
                    "delete_flag": "",
                    "created_at": "2018-11-13T03:27:49.245Z",
                    "updated_at": "2018-11-13T03:27:49.245Z",
                    "description": "open {0}",
                    "test_class": "AC_Open",
                    "type": "in",
                    
                        
                },
                "params": [
                    {
                        "id": 2,
                        "name": "Url",
                        "test_action_id": 2,
                        "param_type": "string",
                    }
                ]
            },
            {
                "data": {
                    "id": 3,
                    "name": "enter value to item",
                    "action_type_id": 3,
                    "created_by": "System",
                    "updated_by": "System",
                    "delete_flag": "",
                    "created_at": "2018-11-13T03:27:49.260Z",
                    "updated_at": "2018-11-13T03:27:49.260Z",
                    "description": "input {2} into {1}",
                    "test_class": "AC_Enter",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 3,
                        "name": "Screen",
                        "test_action_id": 3,
                        "param_type": "layout"
                    },
                    {
                        "id": 4,
                        "name": "Item",
                        "test_action_id": 3,
                        "param_type": "control"
                    },
                    {
                        "id": 114,
                        "name": "Index",
                        "test_action_id": 3,
                        "param_type": "number"
                    },
                    {
                        "id": 5,
                        "name": "Value",
                        "test_action_id": 3,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 4,
                    "name": "click",
                    "action_type_id": 3,
                    "created_by": "System",
                    "updated_by": "System",
                    "delete_flag": "",
                    "created_at": "2018-11-13T03:27:49.260Z",
                    "updated_at": "2018-11-13T03:27:49.260Z",
                    "description": "click on {1}",
                    "test_class": "AC_Click",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 6,
                        "name": "Screen",
                        "test_action_id": 4,
                        "param_type": "layout"
                    },
                    {
                        "id": 7,
                        "name": "Item",
                        "test_action_id": 4,
                        "param_type": "control"
                    },
                    {
                        "id": 114,
                        "name": "Index",
                        "test_action_id": 4,
                        "param_type": "number"
                    },
                ]
            },
            {
                "data": {
                    "id": 5,
                    "name": "take screenshot",
                    "action_type_id": 3,
                    "created_by": "System",
                    "updated_by": "System",
                    "delete_flag": "",
                    "created_at": "2018-11-13T03:27:49.260Z",
                    "updated_at": "2018-11-13T03:27:49.260Z",
                    "description": "take screenshot",
                    "test_class": "AC_TakeScreenshot",
                    "type": "out",
                    
                },
                "params": []
            },
            {
                "data": {
                    "id": 6,
                    "name": "close browser",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2018-11-14T04:20:26.188Z",
                    "updated_at": "2018-11-14T04:20:26.188Z",
                    "description": "close browser",
                    "test_class": "AC_CloseBrowser",
                    "type": "in",
                    
                },
                "params": []
            },
            {
                "data": {
                    "id": 7,
                    "name": "set variable",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2018-11-14T05:21:17.991Z",
                    "updated_at": "2018-11-14T05:21:17.991Z",
                    "description": "set {2} for {0}",
                    "test_class": "AC_SetVariable",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 8,
                        "name": "Name",
                        "test_action_id": 7,
                        "param_type": "string"
                    },
                    {
                        "id": 9,
                        "name": "Variable Type",
                        "test_action_id": 7,
                        "param_type": "list=Set Value,Set Password"
                    },
                    {
                        "id": 10,
                        "name": "Value",
                        "test_action_id": 7,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 8,
                    "name": "if",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2018-11-21T09:57:36.490Z",
                    "updated_at": "2018-11-21T09:57:36.490Z",
                    "description": "check condition: {0} ",
                    "test_class": "AC_If",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 11,
                        "name": "Condition",
                        "test_action_id": 8,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 9,
                    "name": "end if",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "testing",
                    "delete_flag": "",
                    "created_at": "2018-11-21T10:01:06.144Z",
                    "updated_at": "2018-12-19T04:32:04.819Z",
                    "description": "end if",
                    "test_class": "AC_EndIf",
                    "type": "in",
                    
                },
                "params": []
            },
            {
                "data": {
                    "id": 10,
                    "name": "loop",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2018-11-21T10:01:31.494Z",
                    "updated_at": "2018-11-21T10:01:31.494Z",
                    "description": "loop {0} times",
                    "test_class": "AC_Loop",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 12,
                        "name": "Times",
                        "test_action_id": 10,
                        "param_type": "string"
                    },
                    {
                        "id": 13,
                        "name": "Variable",
                        "test_action_id": 10,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 11,
                    "name": "end loop",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2018-11-21T10:02:14.483Z",
                    "updated_at": "2018-11-21T10:02:14.483Z",
                    "description": "end loop",
                    "test_class": "AC_EndLoop",
                    "type": "in",
                    
                },
                "params": []
            },
            {
                "data": {
                    "id": 12,
                    "name": "excel get range value",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2018-11-23T08:36:33.369Z",
                    "updated_at": "2018-11-23T08:36:33.369Z",
                    "description": "read {2} range from file [{0}][{1}]",
                    "test_class": "AC_GetRangeValue",
                    "type":"in",
                    
                },
                "params": [
                    {
                        "id": 14,
                        "name": "File name",
                        "test_action_id": 12,
                        "param_type": "filepath"
                    },
                    {
                        "id": 15,
                        "name": "Sheet name",
                        "test_action_id": 12,
                        "param_type": "string"
                    },
                    {
                        "id": 16,
                        "name": "Range",
                        "test_action_id": 12,
                        "param_type": "string"
                    },
                    {
                        "id": 17,
                        "name": "Result",
                        "test_action_id": 12,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 12,
                    "name": "excel get column",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2018-11-23T08:36:33.369Z",
                    "updated_at": "2018-11-23T08:36:33.369Z",
                    "description": "read {2} range from file [{0}][{1}]",
                    "test_class": "AC_GetColumn",
                    "type":"in",
                    
                },
                "params": [
                    {
                        "id": 14,
                        "name": "File name",
                        "test_action_id": 12,
                        "param_type": "filepath"
                    },
                    {
                        "id": 15,
                        "name": "Sheet name",
                        "test_action_id": 12,
                        "param_type": "string"
                    },
                    {
                        "id": 16,
                        "name": "Range",
                        "test_action_id": 12,
                        "param_type": "string"
                    },
                    {
                        "id": 17,
                        "name": "Result",
                        "test_action_id": 12,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 25,
                    "name": "excel get cell value",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-03-22T03:59:53.626Z",
                    "updated_at": "2019-03-22T03:59:53.626Z",
                    "description": "get value at {2} cell from file [{0}][{1}]",
                    "test_class": "AC_GetCellValue",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 44,
                        "name": "File name",
                        "test_action_id": 25,
                        "param_type": "filepath"
                    },
                    {
                        "id": 45,
                        "name": "Sheet name",
                        "test_action_id": 25,
                        "param_type": "string"
                    },
                    {
                        "id": 46,
                        "name": "Row",
                        "test_action_id": 25,
                        "param_type": "string"
                    },
                    {
                        "id": 47,
                        "name": "Column",
                        "test_action_id": 25,
                        "param_type": "string"
                    },
                    {
                        "id": 48,
                        "name": "Result",
                        "test_action_id": 25,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 26,
                    "name": "excel check cell value",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-03-22T04:02:06.804Z",
                    "updated_at": "2019-03-22T04:02:06.804Z",
                    "description": "check {3} equal value of {2} cell in file [{0}][{1}]",
                    "test_class": "AC_CheckCellValue",
                    "type": "out",
                    
                },
                "params": [
                    {
                        "id": 49,
                        "name": "File name",
                        "test_action_id": 26,
                        "param_type": "filepath"
                    },
                    {
                        "id": 50,
                        "name": "Sheet name",
                        "test_action_id": 26,
                        "param_type": "string"
                    },
                    {
                        "id": 51,
                        "name": "Row",
                        "test_action_id": 26,
                        "param_type": "string"
                    },
                    {
                        "id": 52,
                        "name": "Column",
                        "test_action_id": 26,
                        "param_type": "string"
                    },
                    {
                        "id": 53,
                        "name": "Result",
                        "test_action_id": 26,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 49,
                    "name": "excel set cell value",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-06-17T05:33:37.762Z",
                    "updated_at": "2019-06-17T05:33:37.762Z",
                    "description": "write {3} into {2} cell of file [{0}][{1}]",
                    "test_class": "AC_SetCellValue",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 109,
                        "name": "File name",
                        "test_action_id": 49,
                        "param_type": "filepath"
                    },
                    {
                        "id": 110,
                        "name": "Sheet name",
                        "test_action_id": 49,
                        "param_type": "string"
                    },
                    {
                        "id": 111,
                        "name": "Row",
                        "test_action_id": 49,
                        "param_type": "string"
                    },
                    {
                        "id": 112,
                        "name": "Column",
                        "test_action_id": 49,
                        "param_type": "string"
                    },
                    {
                        "id": 113,
                        "name": "Value",
                        "test_action_id": 49,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 56,
                    "name": "excel set range value",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-07-03T07:08:49.176Z",
                    "updated_at": "2019-07-03T07:08:49.176Z",
                    "description": "excel: write {3} into {2} of file [{0}][{1}]",
                    "test_class": "AC_SetRangeValue",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 125,
                        "name": "File name",
                        "test_action_id": 56,
                        "param_type": "filepath"
                    },
                    {
                        "id": 126,
                        "name": "Sheet name",
                        "test_action_id": 56,
                        "param_type": "string"
                    },
                    {
                        "id": 127,
                        "name": "Row",
                        "test_action_id": 56,
                        "param_type": "string"
                    },
                    {
                        "id": 128,
                        "name": "Column",
                        "test_action_id": 56,
                        "param_type": "string"
                    },
                    {
                        "id": 129,
                        "name": "Value",
                        "test_action_id": 56,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 13,
                    "name": "sleep",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2018-12-04T04:51:21.144Z",
                    "updated_at": "2018-12-04T04:51:21.144Z",
                    "description": "wait {0} miliseconds",
                    "test_class": "AC_Sleep",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 18,
                        "name": "Miliseconds",
                        "test_action_id": 13,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 14,
                    "name": "select",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2018-12-04T09:08:56.670Z",
                    "updated_at": "2018-12-04T09:08:56.670Z",
                    "description": "select {2} in {1} dropdown",
                    "test_class": "AC_Select",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 19,
                        "name": "Screen",
                        "test_action_id": 14,
                        "param_type": "layout"
                    },
                    {
                        "id": 20,
                        "name": "Item",
                        "test_action_id": 14,
                        "param_type": "control"
                    },
                    {
                        "id": 114,
                        "name": "Index",
                        "test_action_id": 14,
                        "param_type": "number"
                    },
                    {
                        "id": 21,
                        "name": "Value",
                        "test_action_id": 14,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 15,
                    "name": "set checkbox on/off",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2018-12-11T07:13:10.937Z",
                    "updated_at": "2018-12-11T07:13:10.937Z",
                    "description": "set {2} for {1}",
                    "test_class": "AC_SetCheckboxValue",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 22,
                        "name": "Screen",
                        "test_action_id": 15,
                        "param_type": "layout"
                    },
                    {
                        "id": 23,
                        "name": "Item",
                        "test_action_id": 15,
                        "param_type": "control"
                    },
                    {
                        "id": 114,
                        "name": "Index",
                        "test_action_id": 15,
                        "param_type": "number"
                    },
                    {
                        "id": 24,
                        "name": "Value",
                        "test_action_id": 15,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 16,
                    "name": "get checkbox value",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2018-12-11T07:14:35.685Z",
                    "updated_at": "2018-12-11T07:14:35.685Z",
                    "description": "get value {1} to {2}",
                    "test_class": "AC_GetCheckboxValue",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 25,
                        "name": "Screen",
                        "test_action_id": 16,
                        "param_type": "layout"
                    },
                    {
                        "id": 26,
                        "name": "Item",
                        "test_action_id": 16,
                        "param_type": "control"
                    },
                    {
                        "id": 114,
                        "name": "Index",
                        "test_action_id": 16,
                        "param_type": "number"
                    },
                    {
                        "id": 27,
                        "name": "Variable",
                        "test_action_id": 16,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 17,
                    "name": "check value",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2018-12-11T07:16:02.792Z",
                    "updated_at": "2018-12-11T07:16:02.792Z",
                    "description": "check {0} {1} {2}",
                    "test_class": "AC_CheckValue",
                    "type": "out",
                    
                },
                "params": [
                    {
                        "id": 28,
                        "name": "Variable",
                        "test_action_id": 17,
                        "param_type": "string"
                    },
                    {
                        "id": 95,
                        "name": "Operator",
                        "test_action_id": 17,
                        "param_type": "list=equal,greater than,less than,greater than or equal,less than or equal,contain,incontain,wildcard"
                    },
                    {
                        "id": 96,
                        "name": "Expected",
                        "test_action_id": 17,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 18,
                    "name": "set javascript variable",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2018-12-11T07:18:10.242Z",
                    "updated_at": "2018-12-11T07:18:10.242Z",
                    "description": "set {0} for {1} js",
                    "test_class": "AC_SetJavascriptVariable",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 30,
                        "name": "Variable",
                        "test_action_id": 18,
                        "param_type": "string"
                    },
                    {
                        "id": 31,
                        "name": "Value",
                        "test_action_id": 18,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 19,
                    "name": "execute javascript",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2018-12-11T07:18:49.209Z",
                    "updated_at": "2018-12-11T07:18:49.209Z",
                    "description": "execute javascript",
                    "test_class": "AC_ExecuteJavascript",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 32,
                        "name": "Script",
                        "test_action_id": 19,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 20,
                    "name": "get javascript variable",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2018-12-11T07:19:24.755Z",
                    "updated_at": "2018-12-11T07:19:24.755Z",
                    "description": "get {0} js to {1}",
                    "test_class": "AC_GetJavascriptVariable",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 33,
                        "name": "Variable",
                        "test_action_id": 20,
                        "param_type": "string"
                    },
                    {
                        "id": 34,
                        "name": "Result",
                        "test_action_id": 20,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 21,
                    "name": "execute testcase",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "testing",
                    "delete_flag": "",
                    "created_at": "2019-02-12T08:30:13.553Z",
                    "updated_at": "2019-02-12T08:32:31.246Z",
                    "description": "execute {0} testcase",
                    "test_class": "AC_ExecuteTestcase",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 35,
                        "name": "Testcase",
                        "test_action_id": 21,
                        "param_type": "testcase"
                    }
                ]
            },
            {
                "data": {
                    "id": 22,
                    "name": "take full screenshot",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "testing",
                    "delete_flag": "",
                    "created_at": "2019-03-14T03:43:11.472Z",
                    "updated_at": "2019-03-14T03:43:28.785Z",
                    "description": "take full screenshot",
                    "test_class": "AC_TakeFullScreen",
                    "type": "out",
                    
                },
                "params": []
            },
            {
                "data": {
                    "id": 29,
                    "name": "read csv file",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-03-22T04:17:28.264Z",
                    "updated_at": "2019-03-22T04:17:28.264Z",
                    "description": "read {0} file into {1}",
                    "test_class": "AC_GetCsvFile",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 60,
                        "name": "File name",
                        "test_action_id": 29,
                        "param_type": "filepath"
                    },
                    {
                        "id": 61,
                        "name": "Result",
                        "test_action_id": 29,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 42,
                    "name": "write to csv",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-05-15T08:47:15.109Z",
                    "updated_at": "2019-05-15T08:47:15.109Z",
                    "description": "write {0} to {1}",
                    "test_class": "AC_WriteToCSV",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 85,
                        "name": "Data",
                        "test_action_id": 42,
                        "param_type": "string"
                    },
                    {
                        "id": 86,
                        "name": "File name",
                        "test_action_id": 42,
                        "param_type": "filepath"
                    },
                    {
                        "id": 87,
                        "name": "Append",
                        "test_action_id": 42,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 30,
                    "name": "set control value",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "testing",
                    "delete_flag": "",
                    "created_at": "2019-03-22T04:19:39.917Z",
                    "updated_at": "2019-03-22T04:32:26.549Z",
                    "description": "set {2} to {1} item",
                    "test_class": "AC_SetControlValue",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 62,
                        "name": "Screen",
                        "test_action_id": 30,
                        "param_type": "layout"
                    },
                    {
                        "id": 63,
                        "name": "Item",
                        "test_action_id": 30,
                        "param_type": "control"
                    },
                    {
                        "id": 114,
                        "name": "Index",
                        "test_action_id": 30,
                        "param_type": "number"
                    },
                    {
                        "id": 64,
                        "name": "Value",
                        "test_action_id": 30,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 32,
                    "name": "get control value",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "testing",
                    "delete_flag": "",
                    "created_at": "2019-03-22T04:22:02.176Z",
                    "updated_at": "2019-03-22T04:34:10.953Z",
                    "description": "get value of {1} item to {2}",
                    "test_class": "AC_GetControlValue",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 65,
                        "name": "Screen",
                        "test_action_id": 32,
                        "param_type": "layout"
                    },
                    {
                        "id": 66,
                        "name": "Item",
                        "test_action_id": 32,
                        "param_type": "control"
                    },
                    {
                        "id": 114,
                        "name": "Index",
                        "test_action_id": 32,
                        "param_type": "number"
                    },
                    {
                        "id": 67,
                        "name": "Return Variable",
                        "test_action_id": 32,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 33,
                    "name": "check control value",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-03-22T04:35:28.402Z",
                    "updated_at": "2019-03-22T04:35:28.402Z",
                    "description": "check value of {1} {2} {3} with screenshot",
                    "test_class": "AC_CheckControlValue",
                    "type": "out",
                    
                },
                "params": [
                    {
                        "id": 68,
                        "name": "Screen",
                        "test_action_id": 33,
                        "param_type": "layout"
                    },
                    {
                        "id": 69,
                        "name": "Item",
                        "test_action_id": 33,
                        "param_type": "control"
                    },
                    {
                        "id": 114,
                        "name": "Index",
                        "test_action_id": 33,
                        "param_type": "number"
                    },
                    {
                        "id": 93,
                        "name": "Operator",
                        "test_action_id": 33,
                        "param_type": "list=equal,greater than,less than,greater than or equal,less than or equal,contain,incontain,wildcard"
                    },
                    {
                        "id": 94,
                        "name": "Expected",
                        "test_action_id": 33,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 34,
                    "name": "compare image",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-03-22T04:37:10.227Z",
                    "updated_at": "2019-03-22T04:37:10.227Z",
                    "description": "compare image: {0} with {1}",
                    "test_class": "AC_CompareImage",
                    "type": "out",
                    
                },
                "params": [
                    {
                        "id": 71,
                        "name": "Recorded image",
                        "test_action_id": 34,
                        "param_type": "string"
                    },
                    {
                        "id": 72,
                        "name": "Expected image",
                        "test_action_id": 34,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 35,
                    "name": "take screenshot to variable",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-03-22T04:39:17.429Z",
                    "updated_at": "2019-03-22T04:39:17.429Z",
                    "description": "take screenshot to {0}",
                    "test_class": "AC_TakeScreenshotWithResult",
                    "type": "out",
                    
                },
                "params": [
                    {
                        "id": 73,
                        "name": "Image name",
                        "test_action_id": 35,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 36,
                    "name": "connect database",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-03-22T04:40:30.989Z",
                    "updated_at": "2019-03-22T04:40:30.989Z",
                    "description": "connect database {2}",
                    "test_class": "AC_ConnectDatabase",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 74,
                        "name": "Connection name",
                        "test_action_id": 36,
                        "param_type": "string"
                    },
                    {
                        "id": 75,
                        "name": "Type sql",
                        "test_action_id": 36,
                        "param_type": "list=sqlserver,mysql,oracle,mongodb"
                    },
                    {
                        "id": 76,
                        "name": "Server string",
                        "test_action_id": 36,
                        "param_type": "string"
                    },
                    {
                        "id": 77,
                        "name": "Database name",
                        "test_action_id": 36,
                        "param_type": "string"
                    },
                    {
                        "id": 78,
                        "name": "Username",
                        "test_action_id": 36,
                        "param_type": "string"
                    },
                    {
                        "id": 79,
                        "name": "Password",
                        "test_action_id": 36,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 37,
                    "name": "execute query",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-03-22T04:43:04.940Z",
                    "updated_at": "2019-03-22T04:43:04.940Z",
                    "description": "execute query",
                    "test_class": "AC_ExecuteQuery",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 80,
                        "name": "Connection name",
                        "test_action_id": 37,
                        "param_type": "string"
                    },
                    {
                        "id": 80,
                        "name": "Type",
                        "test_action_id": 37,
                        "param_type": "list=text,file"
                    },
                    {
                        "id": 81,
                        "name": "Query string",
                        "test_action_id": 37,
                        "param_type": "string"
                    },
                    {
                        "id": 82,
                        "name": "Result",
                        "test_action_id": 37,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 38,
                    "name": "check control attribute",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "testing",
                    "delete_flag": "",
                    "created_at": "2019-04-23T06:41:57.082Z",
                    "updated_at": "2019-04-23T07:18:11.838Z",
                    "description": "Check {2} attribute of {1} equal {3}",
                    "test_class": "AC_CheckControlAttribute",
                    "type": "out",
                    
                },
                "params": [
                    {
                        "id": 83,
                        "name": "Screen",
                        "test_action_id": 38,
                        "param_type": "layout"
                    },
                    {
                        "id": 84,
                        "name": "Item",
                        "test_action_id": 38,
                        "param_type": "control"
                    },
                    {
                        "id": 114,
                        "name": "Index",
                        "test_action_id": 38,
                        "param_type": "number"
                    },
                    {
                        "id": 85,
                        "name": "Attribute",
                        "test_action_id": 38,
                        "param_type": "string"
                    },
                    {
                        "id": 86,
                        "name": "Value",
                        "test_action_id": 38,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 43,
                    "name": "check format",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-05-22T04:06:27.102Z",
                    "updated_at": "2019-05-22T04:06:27.102Z",
                    "description": "is {1} has {0} format?",
                    "test_class": "AC_CheckValueFormat",
                    "type": "out",
                    
                },
                "params": [
                    {
                        "id": 87,
                        "name": "Format",
                        "test_action_id": 43,
                        "param_type": "string"
                    },
                    {
                        "id": 88,
                        "name": "Value or variable",
                        "test_action_id": 43,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 44,
                    "name": "get screen table",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-05-24T03:47:15.115Z",
                    "updated_at": "2019-05-24T03:47:15.115Z",
                    "description": "get value of {1} table to {2}",
                    "test_class": "AC_GetTableData",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 90,
                        "name": "Screen",
                        "test_action_id": 44,
                        "param_type": "layout"
                    },
                    {
                        "id": 91,
                        "name": "Item",
                        "test_action_id": 44,
                        "param_type": "control"
                    },
                    {
                        "id": 114,
                        "name": "Index",
                        "test_action_id": 44,
                        "param_type": "number"
                    },
                    {
                        "id": 92,
                        "name": "Value",
                        "test_action_id": 44,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 45,
                    "name": "ftp connect",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-06-12T10:47:03.493Z",
                    "updated_at": "2019-06-12T10:47:03.493Z",
                    "description": "connect ftp server: {0}",
                    "test_class": "AC_ConnectFTP",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 93,
                        "name": "Connect name",
                        "test_action_id": 45,
                        "param_type": "string"
                    },
                    {
                        "id": 94,
                        "name": "Host",
                        "test_action_id": 45,
                        "param_type": "string"
                    },
                    {
                        "id": 95,
                        "name": "Port",
                        "test_action_id": 45,
                        "param_type": "string"
                    },
                    {
                        "id": 96,
                        "name": "Username",
                        "test_action_id": 45,
                        "param_type": "string"
                    },
                    {
                        "id": 97,
                        "name": "Password",
                        "test_action_id": 45,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 46,
                    "name": "ftp download file",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "testing",
                    "delete_flag": "",
                    "created_at": "2019-06-12T10:49:45.418Z",
                    "updated_at": "2019-06-25T08:48:19.566Z",
                    "description": "ftp: download {2} at {1}",
                    "test_class": "AC_DowloadFTPFile",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 98,
                        "name": "Connect name",
                        "test_action_id": 46,
                        "param_type": "string"
                    },
                    {
                        "id": 99,
                        "name": "Remote Path",
                        "test_action_id": 46,
                        "param_type": "string"
                    },
                    {
                        "id": 100,
                        "name": "Local path",
                        "test_action_id": 46,
                        "param_type": "filepath"
                    },
                    {
                        "id": 101,
                        "name": "File name",
                        "test_action_id": 46,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 47,
                    "name": "ftp download folders",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "testing",
                    "delete_flag": "",
                    "created_at": "2019-06-12T10:53:54.282Z",
                    "updated_at": "2019-06-25T08:48:50.359Z",
                    "description": "ftp download {0}",
                    "test_class": "AC_DowloadFTPFiles",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 102,
                        "name": "Connect name",
                        "test_action_id": 47,
                        "param_type": "string"
                    },
                    {
                        "id": 103,
                        "name": "Remote path",
                        "test_action_id": 47,
                        "param_type": "string"
                    },
                    {
                        "id": 104,
                        "name": "Local path",
                        "test_action_id": 47,
                        "param_type": "filepath"
                    }
                ]
            },
            {
                "data": {
                    "id": 48,
                    "name": "ftp upload file",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "testing",
                    "delete_flag": "",
                    "created_at": "2019-06-12T10:55:31.687Z",
                    "updated_at": "2019-06-25T08:49:15.394Z",
                    "description": "ftp upload {1}",
                    "test_class": "AC_UploadFTPFile",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 105,
                        "name": "Connect name",
                        "test_action_id": 48,
                        "param_type": "string"
                    },
                    {
                        "id": 106,
                        "name": "Remote path",
                        "test_action_id": 48,
                        "param_type": "string"
                    },
                    {
                        "id": 107,
                        "name": "Local path",
                        "test_action_id": 48,
                        "param_type": "filepath"
                    }
                ]
            },
            {
                "data": {
                    "id": 60,
                    "name": "ftp get folders",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-07-05T10:13:34.816Z",
                    "updated_at": "2019-07-05T10:13:34.816Z",
                    "description": "ftp: get folders in {0}",
                    "test_class": "AC_GetFtpFolders",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 135,
                        "name": "Connect name",
                        "test_action_id": 60,
                        "param_type": "string"
                    },
                    {
                        "id": 136,
                        "name": "Remote path",
                        "test_action_id": 60,
                        "param_type": "string"
                    },
                    {
                        "id": 137,
                        "name": "Result",
                        "test_action_id": 60,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 66,
                    "name": "ftp create folder",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-08-02T04:14:17.469Z",
                    "updated_at": "2019-08-02T04:14:17.469Z",
                    "description": "create {0} in ftp ",
                    "test_class": "AC_CreateFolderFTP",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 154,
                        "name": "Connect name",
                        "test_action_id": 66,
                        "param_type": "string"
                    },
                    {
                        "id": 155,
                        "name": "Dirname",
                        "test_action_id": 66,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 67,
                    "name": "ftp delete",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-08-02T04:15:20.026Z",
                    "updated_at": "2019-08-02T04:15:20.026Z",
                    "description": "delete {0} in ftp",
                    "test_class": "AC_DeleteInFTP",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 156,
                        "name": "Connect name",
                        "test_action_id": 67,
                        "param_type": "string"
                    },
                    {
                        "id": 157,
                        "name": "Dirname",
                        "test_action_id": 67,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 73,
                    "name": "ftp check file exist",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-08-06T08:12:27.691Z",
                    "updated_at": "2019-08-06T08:12:27.691Z",
                    "description": "check {1} type exist on ftp {0}",
                    "test_class": "AC_CheckFileExistFTP",
                    "type": "out",
                    
                },
                "params": [
                    {
                        "id": 166,
                        "name": "Connect name",
                        "test_action_id": 73,
                        "param_type": "string"
                    },
                    {
                        "id": 167,
                        "name": "Remote path",
                        "test_action_id": 73,
                        "param_type": "string"
                    },
                    {
                        "id": 168,
                        "name": "Type file",
                        "test_action_id": 73,
                        "param_type": "string"
                    },
                    {
                        "id": 169,
                        "name": "Result",
                        "test_action_id": 73,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 50,
                    "name": "capture image of control",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-06-17T05:52:16.946Z",
                    "updated_at": "2019-06-17T05:52:16.946Z",
                    "description": "capture image of {1}",
                    "test_class": "AC_CaptureControl",
                    "type": "out",
                    
                },
                "params": [
                    {
                        "id": 113,
                        "name": "Screen",
                        "test_action_id": 50,
                        "param_type": "layout"
                    },
                    {
                        "id": 114,
                        "name": "Item",
                        "test_action_id": 50,
                        "param_type": "control"
                    },
                    {
                        "id": 114,
                        "name": "Index",
                        "test_action_id": 50,
                        "param_type": "number"
                    },
                    {
                        "id": 115,
                        "name": "File name",
                        "test_action_id": 50,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 51,
                    "name": "get attribute of element",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-06-18T03:44:09.216Z",
                    "updated_at": "2019-06-18T03:44:09.216Z",
                    "description": "get {2} in {1}",
                    "test_class": "AC_GetAttribute",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 116,
                        "name": "Screen",
                        "test_action_id": 51,
                        "param_type": "layout"
                    },
                    {
                        "id": 117,
                        "name": "Item",
                        "test_action_id": 51,
                        "param_type": "control"
                    },
                    {
                        "id": 114,
                        "name": "Index",
                        "test_action_id": 51,
                        "param_type": "number"
                    },
                    {
                        "id": 118,
                        "name": "Attribute",
                        "test_action_id": 51,
                        "param_type": "string"
                    },
                    {
                        "id": 119,
                        "name": "Value",
                        "test_action_id": 51,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 52,
                    "name": "while",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-07-02T03:56:41.625Z",
                    "updated_at": "2019-07-02T03:56:41.625Z",
                    "description": "loop with condition {0}",
                    "test_class": "AC_While",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 120,
                        "name": "Condition",
                        "test_action_id": 52,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 53,
                    "name": "end while",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-07-02T03:58:57.381Z",
                    "updated_at": "2019-07-02T03:58:57.381Z",
                    "description": "end while",
                    "test_class": "AC_EndWhile",
                    "type": "in",
                    
                },
                "params": []
            },
            {
                "data": {
                    "id": 54,
                    "name": "get list files",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-07-02T04:48:44.804Z",
                    "updated_at": "2019-07-02T04:48:44.804Z",
                    "description": "get {1} files in {0}",
                    "test_class": "AC_GetFilesInFolder",
                    "type": "in"
                },
                "params": [
                    {
                        "id": 121,
                        "name": "Path",
                        "test_action_id": 54,
                        "param_type": "filepath"
                    },
                    {
                        "id": 122,
                        "name": "Type",
                        "test_action_id": 54,
                        "param_type": "string"
                    },
                    {
                        "id": 123,
                        "name": "Result",
                        "test_action_id": 54,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 55,
                    "name": "send keys",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-07-03T04:22:49.289Z",
                    "updated_at": "2019-07-03T04:22:49.289Z",
                    "description": "send {0} keys",
                    "test_class": "AC_Type",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 124,
                        "name": "Value",
                        "test_action_id": 55,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 61,
                    "name": "get control by",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-07-09T03:43:15.004Z",
                    "updated_at": "2019-07-09T03:43:15.004Z",
                    "description": "get control {0} by {1}:{2}",
                    "test_class": "AC_GetControlBy",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 137,
                        "name": "Control name",
                        "test_action_id": 61,
                        "param_type": "string"
                    },
                    {
                        "id": 138,
                        "name": "Detect by",
                        "test_action_id": 61,
                        "param_type": "list=id,href,xpath,class,name,text,css"
                    },
                    {
                        "id": 139,
                        "name": "Value",
                        "test_action_id": 61,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 62,
                    "name": "define email sender",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-07-15T02:36:37.915Z",
                    "updated_at": "2019-07-15T02:36:37.915Z",
                    "description": "define sender: {2}",
                    "test_class": "AC_EmailFromContact",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 141,
                        "name": "Connect email",
                        "test_action_id": 62,
                        "param_type": "string"
                    },
                    {
                        "id": 142,
                        "name": "Smtp server",
                        "test_action_id": 62,
                        "param_type": "string"
                    },
                    {
                        "id": 143,
                        "name": "Port",
                        "test_action_id": 62,
                        "param_type": "string"
                    },
                    {
                        "id": 144,
                        "name": "Email",
                        "test_action_id": 62,
                        "param_type": "string"
                    },
                    {
                        "id": 145,
                        "name": "Password",
                        "test_action_id": 62,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 63,
                    "name": "send email",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-07-15T02:38:51.348Z",
                    "updated_at": "2019-07-15T02:38:51.348Z",
                    "description": "send email to {1}",
                    "test_class": "AC_SendEmail",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 147,
                        "name": "Connect email",
                        "test_action_id": 63,
                        "param_type": "string"
                    },
                    {
                        "id": 148,
                        "name": "To",
                        "test_action_id": 63,
                        "param_type": "string"
                    },
                    {
                        "id": 149,
                        "name": "Subject",
                        "test_action_id": 63,
                        "param_type": "string"
                    },
                    {
                        "id": 150,
                        "name": "Content",
                        "test_action_id": 63,
                        "param_type": "string"
                    },
                    {
                        "id": 151,
                        "name": "Attatch file",
                        "test_action_id": 63,
                        "param_type": "filepath"
                    }
                ]
            },
            {
                "data": {
                    "id": 64,
                    "name": "get local folders",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-07-16T08:40:06.075Z",
                    "updated_at": "2019-07-16T08:40:06.075Z",
                    "description": "get all folders in {0}",
                    "test_class": "AC_GetFolders",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 153,
                        "name": "Path",
                        "test_action_id": 64,
                        "param_type": "string"
                    },
                    {
                        "id": 154,
                        "name": "Result",
                        "test_action_id": 64,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 65,
                    "name": "copy file",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-07-31T08:47:24.470Z",
                    "updated_at": "2019-07-31T08:47:24.470Z",
                    "description": "copy {0} file to {1}",
                    "test_class": "AC_CopyFile",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 155,
                        "name": "File path",
                        "test_action_id": 65,
                        "param_type": "filepath"
                    },
                    {
                        "id": 156,
                        "name": "New file",
                        "test_action_id": 65,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 68,
                    "name": "get all file in folder",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-08-02T07:33:59.014Z",
                    "updated_at": "2019-08-02T07:33:59.014Z",
                    "description": "get all file in {0}",
                    "test_class": "AC_GetAllFileInFolder",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 157,
                        "name": "Path",
                        "test_action_id": 68,
                        "param_type": "filepath"
                    },
                    {
                        "id": 158,
                        "name": "Result",
                        "test_action_id": 68,
                        "param_type": "string"
                    }
                ]
            },
            {
                "data": {
                    "id": 69,
                    "name": "delete file",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-08-02T09:28:06.476Z",
                    "updated_at": "2019-08-02T09:28:06.476Z",
                    "description": "delete {0}",
                    "test_class": "AC_DeleteFile",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 159,
                        "name": "File path",
                        "test_action_id": 69,
                        "param_type": "filepath"
                    }
                ]
            },
            {
                "data": {
                    "id": 70,
                    "name": "move folder",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-08-05T06:30:43.705Z",
                    "updated_at": "2019-08-05T06:30:43.705Z",
                    "description": "move {0} into {1}",
                    "test_class": "AC_MoveFolder",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 160,
                        "name": "Folder path",
                        "test_action_id": 70,
                        "param_type": "string"
                    },
                    {
                        "id": 161,
                        "name": "New directory",
                        "test_action_id": 70,
                        "param_type": "string"
                    }
                ]
            },
            
            {
                "data": {
                    "id": 73,
                    "name": "use testdata",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-08-06T04:57:31.512Z",
                    "updated_at": "2019-08-06T04:57:31.512Z",
                    "description": "use {1} testdata of {0}",
                    "test_class": "AC_UseTestData",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 166,
                        "name": "Screen",
                        "test_action_id": 73,
                        "param_type": "testdata"
                    },
                    {
                        "id": 167,
                        "name": "Casenum",
                        "test_action_id": 73,
                        "param_type": "casenum"
                    }
                ]
            },
            {
                "data": {
                    "id": 74,
                    "name": "check testdata",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-08-06T04:57:31.512Z",
                    "updated_at": "2019-08-06T04:57:31.512Z",
                    "description": "check {1} testdata of {0}",
                    "test_class": "AC_CheckData",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 168,
                        "name": "Screen",
                        "test_action_id": 74,
                        "param_type": "testdata"
                    },
                    {
                        "id": 169,
                        "name": "Casenum",
                        "test_action_id": 74,
                        "param_type": "casenum"
                    }
                ]
            },
            {
                "data": {
                    "id": 75,
                    "name": "Restfull API",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-08-06T04:57:31.512Z",
                    "updated_at": "2019-08-06T04:57:31.512Z",
                    "description": "call API {0} {1} testdata of {6}",
                    "test_class": "AC_API",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id":170,
                        "test_action_id": 75,
                        "name": "Method",
                        "param_type": "list=POST,GET,PUT,UPDATE,DELETE"
                    },
                    {
                        "id":180,
                        "test_action_id": 75,
                        "name": "url",
                        "param_type": "string"
                    },
                    {
                        "name": "Params",
                        "param_type":"key-value"
                    },
                    {
                        "name": "Authorization",
                        "param_type":"auth",
                    },
                    {
                        "name": "Header",
                        "param_type":"header",
                    },
                    {
                        "name": "Body",
                        "param_type":"body"
                    },
                    {
                        "name": "output",
                        "param_type": "variable"
                    }
                
                ]
            },
            {
                "data": {
                    "id": 76,
                    "name": "right click",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-08-06T04:57:31.512Z",
                    "updated_at": "2019-08-06T04:57:31.512Z",
                    "description": "click on {1}",
                    "test_class": "AC_RightClick",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 168,
                        "name": "Screen",
                        "test_action_id": 76,
                        "param_type": "layout"
                    },
                    {
                        "id": 169,
                        "name": "Item",
                        "test_action_id": 76,
                        "param_type": "control"
                    },
                    {
                        "id": 114,
                        "name": "Index",
                        "test_action_id": 38,
                        "param_type": "number"
                    }
                ]
            },
            {
                "data": {
                    "id": 77,
                    "name": "find elements by",
                    "action_type_id": 3,
                    "created_by": "Administrator",
                    "updated_by": "Administrator",
                    "delete_flag": "",
                    "created_at": "2019-07-09T03:43:15.004Z",
                    "updated_at": "2019-07-09T03:43:15.004Z",
                    "description": "find elements {0} by {3}",
                    "test_class": "AC_FindElementsBy",
                    "type": "in",
                    
                },
                "params": [
                    {
                        "id": 137,
                        "name": "Return name",
                        "test_action_id": 77,
                        "param_type": "string"
                    },
                    {
                        "id": 137,
                        "name": "From parent",
                        "test_action_id": 77,
                        "param_type": "string"
                    },
                    {
                        "id": 137,
                        "name": "Index",
                        "test_action_id": 77,
                        "param_type": "string"
                    },
                    {
                        "id": 139,
                        "name": "Css Selector",
                        "test_action_id": 77,
                        "param_type": "string"
                    }
                ]
            }
        ]'''
        return listActions


class BottestLib:
    #random number
    @staticmethod
    def random_number(inputData):
        value = Constants.BLANK
        for i in range(int(inputData)):
            value += str(randint(0,9))
        return value
    
    #random text and number
    @staticmethod
    def random_text(inputData):
        import random
        lettersAndDigits = string.ascii_letters + string.digits
        value = ''.join(random.choice(lettersAndDigits) for i in range(int(inputData)))
        return value
    
    #random date
    @staticmethod
    def random_date(inputData):
        from datetime import datetime, timedelta
        import random
        min_year= 1900
        max_year= datetime.now().year
        start = datetime(min_year, 1, 1, 00, 00, 00)
        years = max_year - min_year+1
        end = start + timedelta(days=365 * years)
        #random ra 1 ngày trong khoảng từ 1900 đến năm hiện tại
        random_date = start + (end - start) * random.random()
        pattern = (((inputData.replace("yyyy", '%Y')).replace("dd", "%d")).replace("mmm", "%b")).replace("mm", "%m")
        
        value = random_date.strftime(pattern)
        return value
    
    #random now
    @staticmethod
    def random_now(inputData):
        import datetime
        date_now = datetime.datetime.now()
        pattern = (((inputData.replace("yyyy", '%Y')).replace("dd", "%d")).replace("mmm", "%b")).replace("mm", "%m")
        value = date_now.strftime(pattern)
        return value
    
    #random hiragana
    @staticmethod
    def random_hiragana(inputData):
        import random
        import string
        hiragana = Constants.ALPHABETICAL_HIRAGANA
        value = ''.join(random.choice(hiragana) for m in range(int(inputData)))
        return value
    
    #random katakana
    @staticmethod
    def radom_katakana(inputData):
        import random
        import string
        katakana = Constants.ALPHABETICAL_KATAKANA
        value = ''.join(random.choice(katakana) for m in range(int(inputData)))
        return value
    
    #random charfullsize
    @staticmethod
    def random_char_full_size(inputData):
        import random
        import string
        lettersAndDigits = string.ascii_letters + string.digits
        strChar = ''.join(random.choice(lettersAndDigits) for i in range(int(inputData)))
        value = jaconv.h2z(strChar, ascii=True, digit = True)
        return value
    


class CheckHelper:
    @staticmethod
    def check_value_helper(driver, recorded, expected, listVariable,operator):
        boolValue = Constants.EMPTY_STRING
        if operator == Constants.EQUAL:
            if str(expected) == str(recorded):
                boolValue = Constants.TRUE
            else:
                boolValue = Constants.FALSE
        elif operator == Constants.GREATER_THAN:
            if str(expected) < str(recorded):
                boolValue = Constants.TRUE
            else:
                boolValue = Constants.FALSE
        elif operator == Constants.LESS_THAN:
            if str(expected) > str(recorded):
                boolValue = Constants.TRUE
            else:
                boolValue = Constants.FALSE
        elif operator == Constants.GREATER_THAN_OR_EQUAL:
            if str(expected) <= str(recorded):
                boolValue = Constants.TRUE
            else:
                boolValue = Constants.FALSE
        elif operator == Constants.LESS_THAN_OR_EQUAL:
            if str(expected) >= str(recorded):
                boolValue = Constants.TRUE
            else:
                boolValue = Constants.FALSE
        elif operator == Constants.CONTAIN:
            if str(recorded) in str(expected):
                boolValue = Constants.TRUE
            else:
                boolValue = Constants.FALSE
        elif operator == Constants.INCONTAIN:
            if str(expected) in str(recorded):
                boolValue = Constants.TRUE
            else:
                boolValue = Constants.FALSE
        return boolValue
    
    @staticmethod
    def get_control_value( driver, executeFilePath, screenName, executeItem, control, resourceFolder, listVariable, index):
        
        elements = LocatorHelper.get_element_by_locator2(driver,executeFilePath, screenName,executeItem, control, resourceFolder,listVariable, index)
        lstVariable = []
        #lấy ra value của attibute tương ứng
        #nếu là textbox, dropdowlist, a tag,heading tag, span, lable thì trả về text
        # nếu là checkbox, radio button, thì trả về on/off
        #nếu là button thì trả về title
        for element in elements:
            variable = Constants.EMPTY_STRING
            if element.tag_name.lower() == Constants.ATTR_INPUT:
                if element.get_attribute(Constants.ATTR_TYPE) == Constants.ATTR_TEXT or element.get_attribute(Constants.ATTR_TYPE) == Constants.ATTR_PASSWORD:
                    variable = element.get_attribute(Constants.ATTR_VALUE)
                elif element.get_attribute(Constants.ATTR_TYPE) == Constants.ATTR_CHECKBOX:
                    vals = element.get_attribute(Constants.ATTR_CHECKED)
                    if vals != None:
                        variable = vals
                    else:
                        variable = Constants.FALSE
                elif element.get_attribute(Constants.ATTR_TYPE) == Constants.ATTR_SUBMIT:
                    variable = element.get_attribute(Constants.ATTR_VALUE)
                    
                else:
                    variable = element.get_attribute(Constants.ATTR_VALUE)
            elif element.tag_name.lower() == Constants.ATTR_SELECT:
                if element.get_attribute(Constants.ATTR_MULTIPLE) == Constants.TRUE:
                    variable = "Action not support!"
                else:
                    slt = Select(element)
                    lstVariable = []
                    for option in slt.options:
                        lstOption = []
                        valueStr = option.get_attribute('value')
                        # textStr = option.text
                        lstOption.append(valueStr)
                        # lstOption.append(textStr)
                        lstVariable.append(lstOption)
                    
                    variable = str(lstVariable)
                
            elif element.tag_name.lower() == Constants.ATTR_A:
                variable = element.text
                
            elif element.tag_name.lower() == Constants.ATTR_LABEL:
                variable = element.text
                
            elif element.tag_name.lower() == Constants.ATTR_SPAN:
                variable = element.text
            elif element.tag_name.lower() == Constants.ATTR_BUTTON:
                variable = element.get_attribute(Constants.ATTR_TITLE)
            
            elif (element.tag_name.lower() in Constants.ATTR_H) == True:
                variable = element.text
                
            else:
                try :
                    variable = element.text
                except Exception as ex:
                    variable = Constants.BLANK
            lstVariable.append(variable)
        if len(lstVariable) > 1:
            return lstVariable
        else:
            if len(lstVariable) != 0:
                return lstVariable[0]
            else:
                return ""
    
    #function work with file excel
    @staticmethod
    def generateColnRow(inputData):
        dataBeforeAdd = Constants.EMPTY_STRING 
        dataValid = Constants.EMPTY_STRING
        for i in reversed(inputData):
            try:
                dataBeforeAdd = i + dataBeforeAdd 
                val = int(dataBeforeAdd)
                dataValid = dataBeforeAdd
            except Exception as e:
                break
        startRow = dataValid
        startColumn = inputData.replace(dataValid,Constants.EMPTY_STRING)
        return (startRow, startColumn)
    
    @staticmethod
    def getNumberFromCharacter(char):
        res = 0
        index = -1
        ls = Constants.LIST_ALPHABET_TABLE
        for c in reversed(char):
            index += 1
            for i in ls:
                for key,val in i.items():
                    if str(c) == str(key):
                        res = res + val
                        if index > 0:
                            res += 26
        return res           
    @staticmethod
    def optimizingData(sRow,sCol):
        return (int(sRow) - 1, CheckHelper.getNumberFromCharacter(sCol))
    @staticmethod
    def generateDataByIput(data):
        sRow, sCol = CheckHelper.generateColnRow(data)
        sRowOK, sColOK = CheckHelper.optimizingData(sRow, sCol)
        return (sColOK, sRowOK)
    
    
    
    
    
    


class ClassHelper:
    @staticmethod
    def import_class_from_string( moduleName, className):
        from importlib import import_module
        mod = import_module(moduleName+Constants.DOT+className)
        klass = getattr(mod, className)
        return klass
        
    @staticmethod
    def get_action_class( actionName):
        cwd = os.getcwd()
        acFolder = Constants.FD_ACTION
        #acFolder = os.path.join(cwd,'FAST_Python', 'Actions')
        for (root, dirs, filenames) in walk(acFolder):
            for f in filenames:
                if f.endswith(Constants.EXT_XML):
                    xmlFilePath =  os.path.join(acFolder, f)
                    #get the tree
                    DOMTree = xml.dom.minidom.parse(xmlFilePath)
                    actions = DOMTree.documentElement.getElementsByTagName(Constants.TAG_ACTION)
                    
                    for action in actions:
                        lang = action.getElementsByTagName(Constants.TAG_LANGUAGE)[0]
                        en = lang.getElementsByTagName(Constants.TAG_EN)[0]
                        name = en.getElementsByTagName(Constants.TAG_NAME)[0]
                        acName = name.childNodes[0].data
                        if acName == actionName:
                            defDefinition = action.getElementsByTagName(Constants.TAG_DEFINITION)[0]
                            defClass = defDefinition.getElementsByTagName(Constants.TAG_CLASS)[0]
                            className = defClass.childNodes[0].data
                            
        return className


class LocatorHelper:
    #trả về 1 list attributes từ các file json ở folder layouts
    @staticmethod
    def get_locator(executeFilePath, screenName,executeItem, control, resourceFolder):
        indexSplit = executeFilePath.split(Constants.SLASH)
        jsonPath = indexSplit[-1]
        tsName = executeItem.testSuiteName
        indexUserName = jsonPath.split(Constants.UNDERSCORE)
        userName = indexUserName[1]
        resultFilePath = resourceFolder + Constants.SLASH + tsName + Constants.SLASH + Constants.LAYOUTS + Constants.SLASH + screenName + Constants.EXT_JSON
        locator = Constants.EMPTY_STRING
        locator_value = Constants.EMPTY_STRING
        tsEntityJson = json.load(codecs.open(resultFilePath, 'r', 'utf-8-sig'))
        # with open(resultFilePath, 'r') as f:
        #     json_data = json.load(f)
        lstControls = tsEntityJson[Constants.TAG_CONTROLS]
        listCondition = {}
        for controls in lstControls:
            ctName = controls[Constants.TAG_NAME]
            if ctName == control:
                lstConditions = controls[Constants.TAG_ATTRIBUTES]
                for condition in lstConditions:
                    locator = condition[Constants.TAG_NAME]
                    locator_value = condition[Constants.TAG_VALUE]
                    listCondition[locator] = locator_value
        return listCondition
    #lấy ra các element
    @staticmethod
    def get_element_by_locator(driver, listCondition, index):
        elements = []
        if index == None:
            i = 0
        else:
            i = int(index)
        if Constants.VALUE_ID in listCondition.keys():
            if listCondition[Constants.VALUE_ID] != Constants.BLANK:
                elements = driver.find_elements_by_id(listCondition[Constants.VALUE_ID])
                if len(elements) > 0 :
                    if i == -1:
                        return elements
                    if len(elements) > i:
                        return [elements[i]]
                    return elements
        if Constants.VALUE_NAME in listCondition.keys():
            if listCondition[Constants.VALUE_NAME] != Constants.BLANK:
                elements = driver.find_elements_by_name(listCondition[Constants.VALUE_NAME])
                if len(elements) > 0 :
                    if i == -1:
                        return elements
                    if len(elements) > i:
                        return [elements[i]]
                    return elements
        if Constants.VALUE_CLASS in listCondition.keys():
            if listCondition[Constants.VALUE_CLASS] != Constants.BLANK:
                valueStrip = listCondition[Constants.VALUE_CLASS].strip()
                value = valueStrip.replace(" ", ".")
                value = "." + value
                elementClass = driver.find_elements_by_css_selector(value)
                
                elementText = []
                elementTagName = []
                if Constants.VALUE_TEXT in listCondition.keys():
                    if listCondition[Constants.VALUE_TEXT] != Constants.BLANK:
                        elementText = driver.find_elements_by_xpath("//*[contains(text(), '" + listCondition[Constants.VALUE_TEXT] + "')]")
                if Constants.VALUE_TAGNAME in listCondition.keys():
                    valueText = listCondition[Constants.VALUE_TAGNAME]
                    
                    if valueText != Constants.BLANK:
                        if valueText == Constants.ATTR_TEXT or valueText == Constants.ATTR_PASSWORD or valueText == Constants.ATTR_RADIO or valueText == Constants.ATTR_CHECKBOX:
                            valueText = Constants.ATTR_INPUT
                        
                        elementTagName = driver.find_elements_by_tag_name(valueText)
                
                if len(elementClass) > 0 and len(elementText) > 0 and len(elementTagName):
                    elementDict = set(elementClass) & set(elementText) & set(elementTagName)
                elif len(elementClass) > 0 and len(elementText) > 0:
                    elementDict = set(elementClass) & set(elementText)
                elif len(elementClass) > 0 and len(elementTagName) > 0:
                    elementDict = set(elementClass) & set(elementTagName)
                elif len(elementClass) > 0:
                    elementDict = set(elementClass)
                elements = list(elementDict)
                
                if len(elements) > 0 :
                    if i == -1:
                        return elements
                    if len(elements) > i:
                        return [elements[i]]
                    return elements
        if Constants.VALUE_HREF in listCondition.keys():
            if listCondition[Constants.VALUE_HREF] != Constants.BLANK:
                localAdd = Constants.A_SQUARR_BRACKETS_LEFT_HREF + listCondition[Constants.VALUE_HREF] + Constants.HREF_SQUARR_BRACKETS_RIGHT
                elementHref = driver.find_elements_by_css_selector(localAdd)
                elementText = []
                if Constants.VALUE_TEXT in listCondition.keys():
                    if listCondition[Constants.VALUE_TEXT] != Constants.BLANK:
                        elementText = driver.find_elements_by_xpath("//*[contains(text(), '" + listCondition[Constants.VALUE_TEXT] + "')]")
                
                if len(elementHref) > 0 and len(elementText) > 0:
                    elementDict = set(elementHref) & set(elementText)
                elif len(elementHref) > 0:
                    elementDict = set(elementHref)
                elements = list(elementDict)
                if len(elements) > 0 :
                    if i == -1:
                        return elements
                    if len(elements) > i:
                        return [elements[i]]
                    return elements
        
        if Constants.VALUE_TEXT in listCondition.keys():
            if listCondition[Constants.VALUE_TEXT] != Constants.BLANK:
                elementText = driver.find_elements_by_xpath("//*[contains(text(), '" + listCondition[Constants.VALUE_TEXT] + "')]")
                elementTagName = []
                if Constants.VALUE_TAGNAME in listCondition.keys():
                    valueText = listCondition[Constants.VALUE_TAGNAME]
                    if valueText == Constants.ATTR_TEXT or Constants.ATTR_PASSWORD or valueText == Constants.ATTR_RADIO or valueText == Constants.ATTR_CHECKBOX:
                        valueText = Constants.ATTR_INPUT
                    elementTagName = driver.find_elements_by_tag_name(listCondition[Constants.VALUE_TAGNAME])
                
                if len(elementText) > 0 and len(elementTagName):
                    elementDict = set(elementText) & set(elementTagName)
                elif len(elementText) > 0:
                    elementDict = set(elementText)
                elements = list(elementDict)
                if len(elements) > 0 :
                    if i == -1:
                        return elements
                    if len(elements) > i:
                        return [elements[i]]
                    return elements
        if Constants.VALUE_CSS in listCondition.keys():
            if listCondition[Constants.VALUE_CSS] != Constants.BLANK:
                elements = driver.find_elements_by_css_selector(listCondition[Constants.VALUE_CSS])
                if len(elements) > 0 :
                    if i == -1:
                        return elements
                    if len(elements) > i:
                        return [elements[i]]
                    return elements
        if Constants.VALUE_XPATH in listCondition.keys():
            if listCondition[Constants.VALUE_XPATH] != Constants.BLANK:
                elements = driver.find_elements_by_xpath(listCondition[Constants.VALUE_XPATH])
                if len(elements) > 0 :
                    if i == -1:
                        return elements
                    if len(elements) > i:
                        return [elements[i]]
                    return elements
        return elements
    #get value from variable 
    @staticmethod
    def get_value_from_variable(param, dictVariable):
        exp = "\$\w+"
        result = re.finditer(exp, param)
        if (result):
            resultls = (list(result))
            i = len(resultls) -1 
            if (i == 0) and (len(param) ==  len(resultls[i].group())):
                return dictVariable[param[1:len(param)]]
            while i >= 0:
                value = resultls[i]
                i = i - 1
                theValue = value.group()
                param = param[0: value.span()[0]] + "dictVariable['" + theValue  + "']" + param[value.span()[1]: len(param)]
        else:
            print("un match")
        exp2 ="\$\{.*\}"
        result2 = re.finditer('\$\{(.*?)\}', param)
        eresult = ''
        lastIndex = 0
        for key,value in dictVariable.items():
            locals()['%s' %key] = value
        for item in result2:
            value = item.group()
            newvalue = eval(value[2:len(value)-1])
            if item.span()[0] == 0 and item.span()[1] == len(param):
                eresult = newvalue
            else:
                eresult = eresult + param[lastIndex : item.span()[0]] + str(newvalue)
            lastIndex = item.span()[1]
        
        if (lastIndex < len(param) ):
            eresult = eresult + param[lastIndex:len(param)]
        if type(eresult) is str:
            if eresult != Constants.BLANK and eresult[0] == "#":
                listSplit = re.split('[\(\)\']', eresult)
                for i in listSplit:
                    if "#" in i:
                        if Constants.RANDOM in i:
                            testChange = 'BottestLib.BottestLib.random_number'
                        elif Constants.TEXT in i:
                            testChange = 'BottestLib.BottestLib.random_text'
                        elif Constants.DATE in i:
                            testChange = 'BottestLib.BottestLib.random_date'
                        elif Constants.NOW in i:
                            testChange = 'BottestLib.BottestLib.random_now'
                        elif Constants.HIRAGANA in i:
                            testChange = 'BottestLib.BottestLib.random_hiragana'
                        elif Constants.KATAKANA in i:
                            testChange = 'BottestLib.BottestLib.radom_katakana'
                        elif Constants.CHARFULLSIZE in i:
                            testChange = 'BottestLib.BottestLib.random_char_full_size'
                        eresult = eresult.replace(i, testChange)
                        try:
                            eresult = eval(eresult)
                        except Exception as e:
                            print(e)
        return eresult
    @staticmethod
    def set_value_from_variable( variable, expression, dictVariable):
        if type(expression) is str:
            value = LocatorHelper.get_value_from_variable(expression, dictVariable)
        else:
            value = expression
        dictVariable[variable] = value
        return value
    @staticmethod
    def get_element_by_locator2( driver, executeFilePath, screenName, executeItem, control, resourceFolder, listVariable, index):
        if screenName == Constants.DYNAMIC:
            
            element = LocatorHelper.get_value_from_variable(control, listVariable)
            
            return element
        else:
            listCondition = LocatorHelper.get_locator(executeFilePath,screenName,executeItem,control,resourceFolder)
        if type(listCondition) is str:
            listCondition = eval(listCondition)
        index = LocatorHelper.get_value_from_variable(str(index), listVariable)
        element = LocatorHelper.get_element_by_locator(driver, listCondition, index)
        return element
    @staticmethod
    def take_screenshot_of_specific_element(driver, element, imagePath, browserName):
        if browserName.strip().lower() == "safari":
            device_pixel_ratio = driver.execute_script('return window.devicePixelRatio')
            el = driver.find_element_by_tag_name('body')
            el.screenshot(imagePath)
            location = element.location
            size = element.size
            im = Image.open(imagePath)
            draw = ImageDraw.Draw(im)
            draw.rectangle(((location['x']*device_pixel_ratio, location['y']*device_pixel_ratio), (location['x']*device_pixel_ratio + size['width']*device_pixel_ratio, location['y']*device_pixel_ratio + size['height']*device_pixel_ratio)), outline='red')
        else:
            el = driver.find_element_by_tag_name('body')
            el.screenshot(imagePath)
            location = element.location
            size = element.size
            im = Image.open(imagePath)
            draw = ImageDraw.Draw(im)
            draw.rectangle(((location['x'], location['y']), (location['x'] + size['width'], location['y'] + size['height'])), outline='red', width=3)
        im.save(imagePath)
    @staticmethod
    def getValueFromParamByKey(listParam, key):
        for i in listParam:
            if i.name == key:
                return i.value
        return None
    
    @staticmethod
    def getValueFromParam(listParam, actionClass):
        
        x=()
        listActionstr = ActionList.list_Actions()
        try:
            listActions = ast.literal_eval(listActionstr)
        except Exception as e:
            print(e)
        for i in listActions:
            if i.get("data").get("test_class") == actionClass:
                actionParam = i.get("params")
                if len(actionParam) == 1:
                    return LocatorHelper.getValueFromParamByKey(listParam, actionParam[0].get("name"))
                for param in actionParam:
                        x= x + (LocatorHelper.getValueFromParamByKey(listParam, param.get("name")),)
                return x